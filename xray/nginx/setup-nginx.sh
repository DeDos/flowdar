#!/bin/bash
#Henry Zhu July 2018

SCRIPT_NAME=`basename "$0"`

#change these parameters
###################################
#where nginx should be installed
INSTALL_PREFIX=/home/henry/nginx-prefix

#where clang is built
CLANG_BUILD_DIR=/home/henry/xray_dev/llvm-build-base
###################################

#setup
CLANG_CC=$CLANG_BUILD_DIR/bin/clang++
XRAY_FLAGS="-fxray-instrument -fxray-instruction-threshold=1"

#download
NGINX_SRC_URL='https://nginx.org/download/nginx-1.12.1.tar.gz'
NGINX_DIR='nginx-1.12.1'
NGINX_TAR_GZ="${NGINX_DIR}.tar.gz"

#patch
NGINX_ORIGINAL='nginx.c'
NGINX_FDR='nginx-fdr.c'
NGINX_PROCESS_ORIGINAL='ngx_process_cycle.c'
NGINX_PROCESS_FDR='ngx_process_cycle_fdr.c'
XRAY_ORIGINAL='xray'
XRAY_FDR='xray-fdr'

#run
XRAY_BASIC_OPTIONS="patch_premain=true xray_mode=xray-basic verbosity=1"
XRAY_FDR_OPTIONS="patch_premain=true xray_mode=xray-fdr verbosity=1"

set -x

function print_usage()
{
	echo "Usage: ./${SCRIPT_NAME} [SETUP/RUN] [BASIC/FDR]/DOWNLOAD/FDR/RESTORE]"
}

if [ "$#" -eq 1 ]; then
	if [ "$1" == "SETUP" ]; then
    CC_OPTS="$XRAY_FLAGS -xc"
    (cd $NGINX_DIR; ./configure --with-cc="${CLANG_CC}" --with-cc-opt="${CC_OPTS}" --prefix="${INSTALL_PREFIX}" --without-http_gzip_module --with-ld-opt="${XRAY_FLAGS}")
		(cd $NGINX_DIR; make)
		(cd $NGINX_DIR; make install)
		sed -i "s/80;/8000;/" $INSTALL_PREFIX/conf/nginx.conf #use port 8000
    cp convert-xray-format-to-flowdar-format.sh $INSTALL_PREFIX/sbin/ #copy the convert tool over
	elif [ "$1" == "RUN" ]; then
		echo "Defaulting to basic mode"
		(cd ${INSTALL_PREFIX}/sbin; export XRAY_OPTIONS=$XRAY_BASIC_OPTIONS; ./nginx -g 'daemon off;')
	elif [ "$1" == "DOWNLOAD" ]; then
		wget $NGINX_SRC_URL
		tar -zxvf $NGINX_TAR_GZ
		rm -f $NGINX_TAR_GZ
	elif [ "$1" == "FDR" ]; then
		cp $NGINX_FDR $NGINX_DIR/src/core/$NGINX_ORIGINAL
		cp $NGINX_PROCESS_FDR $NGINX_DIR/src/os/unix/$NGINX_PROCESS_ORIGINAL

		cp $XRAY_FDR/* $CLANG_BUILD_DIR/lib/clang/7.0.0/include/$XRAY_ORIGINAL

		echo "${NGINX_DIR} is now set to fdr mode"
	elif [ "$1" == "RESTORE" ]; then
		cp $NGINX_ORIGINAL $NGINX_DIR/src/core/
		cp $NGINX_PROCESS_ORIGINAL $NGINX_DIR/src/os/unix/

		cp -rf $XRAY_ORIGINAL $CLANG_BUILD_DIR/lib/clang/7.0.0/include/

		echo "${NGINX_DIR} is now set to basic mode"
	else
		print_usage
	fi
elif [ "$#" -eq 2 ]; then
	if [ "$1" == "RUN" ]; then
		if [ "$2" == "BASIC" ]; then
			(cd ${INSTALL_PREFIX}/sbin; export XRAY_OPTIONS=$XRAY_BASIC_OPTIONS; ./nginx -g 'daemon off;')
		elif [ "$2" == "FDR" ]; then
			(cd ${INSTALL_PREFIX}/sbin; export XRAY_OPTIONS=$XRAY_FDR_OPTIONS; ./nginx -g 'daemon off;')
		else
			echo "Usage: ./${SCRIPT_NAME} [RUN] [BASIC/FDR]"
		fi
	else
		print_usage
	fi
else
	print_usage
fi
