#Henry Zhu July 2018

# Step 1: Build a clang using the system compiler that can build with XRay instrumentation.
git clone https://github.com/llvm-project/llvm-project-20170507 llvm-project

mkdir llvm-build
cd llvm-build
cmake -GNinja \
  -DLLVM_ENABLE_PROJECTS="clang;compiler-rt" \
  -DCMAKE_BUILD_TYPE=Release ../llvm-project/llvm
ninja clang

