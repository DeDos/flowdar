# Henry Zhu July 2018

This directory contains script files for building clang with xray, setting nginx, running nginx with xray and converting xray to flowdar format.

You should change the variables to expected path for setup-nginx.sh and convert-xray-to-flowdar-format.sh

build-clang-xray.sh - builds clang with xray enabled. (Build in current directory)

==============

setup-nginx.sh - downloads, compiles, runs nginx with xray enabled

Make sure you change the path variables in setup-nginx.sh to your paths

Usage: ./setup-nginx.sh [SETUP/RUN/DOWNLOAD/FDR/RESTORE]

DOWNLOAD - downloads nginx source 1.12.1 into current working directory

SETUP - compile nginx source with slight modifications with xray enabled

RUN - runs nginx with daemon off and xray with basic mode turned on in the INSTALL_PREFIX directory

FDR - copies nginx-fdr.c and ngx-process-fdr.c from this directory into nginx source directory and replaces xray interface header with c ones

RESTORE - restores the c files to its originals in nginx source and xray interface headers

convert-xray-to-flowdar-format.sh - converts xray to flowdar by running llvm-xray and parsing the output

It takes in xray-log.\*

MAKE SURE THAT THE FILE NAME DOES NOT HAVE ANY WHITESPACE CHARACTERS OR ELSE IT WILL NOT WORK.

The generated trace will be printed to stdout. You will have to pipe the output into a file (such as trace_resolved.txt for trace-explorer)

==============

Steps:

./setup-nginx.sh DOWNLOAD
./setup-nginx.sh FDR (if you want fdr mode to be enabled. Default is basic mode)
./setup-nginx.sh SETUP
./setup-nginx.sh RUN (FDR)

run wget localhost:8000 a couple of times
You can kill the nginx process by CTRL-C or kill -QUIT $(pidof nginx)
The logs are generated in nginx build directory/sbin
There may be multiple xray logs generated (select the one that has the string "worker" in the filename and rename to a file without whitespaces)
In the same directory there is convert-xray-format-to-flowdar-format.sh. You should run this with ./convert-xray-format-to-flowdar-format.sh YOUR_XRAY_LOG_FILE > trace_resolved.txt
Move the trace_resolved.txt into the trace explorer directory and run the trace explorer tool.

=============

NOTES ABOUT CURRENT VERSION OF XRAY (CLANG 7.0)

XRay does not log any arguments/return values besides the first argument

