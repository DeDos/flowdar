## Instructions for building varnish with xray and generating traces

copy mgt_child.c into /bin/varnishd/mgt/mgt_child.c

Run the following command with your path to clang

CC=/home/henry/xray_dev/llvm-build-base/bin/clang CFLAGS=-fxray-instrument -fxray-instruction-threshold=1 LDFLAGS=-fxray-instrument -fxray-instruction-threshold=1  ./configure --prefix=/home/henry/varnish_build

run nginx on port 8080 (modify nginx configuration)

./varnishd -a :8001 -b :8080 -d (runs varnish cli on 8001 and varnish ontop of nginx on 8080)

## Generating outputs from wget vs under sloworis

type start (in varnish cli)

wget localhost:8001

type quit in varnish cli

type start (in varnish cli)

perl sloworis.pl -dns 127.0.0.1 -port 8001 -num 1 -timeout 5 (run perl)

type quit in varnish cli

## Comparing outputs from wget vs under sloworis

cd varnish_build/var/varnish/Optiplex-9010 (traces generated here, it might not be Optiplex-9010, depends on name on system)

rename the two generated xray logs (xray-log.varnish.*) to something easier to read like slow and wget

modify find_xray_traces_bottlenecks.sh INSTR_MAP_PATH to point at ./varnishd

./find_xray_traces_bottlenecks.sh ~/xray_dev/xray-work/bin/llvm-xray wget slow

Go to trace_tools/ folder for more details on find_xray_traces_bottlenecks.sh

