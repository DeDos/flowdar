# Comparing two xray logs and finding bottlenecks

./find_xray_traces_bottlenecks.sh [llvm-xray path] [first xray log file] [second xray log file]

The order is not important. The way this tool works is by determining which file is smaller and then, finding functions that both contain.

The tool is measured in seconds.

### Example of output

```
Name           TraceTime1  TraceTime2  Difference
Lck_CondWait   13.97107    7.740974    6.2301
http1_req      4.350135    0.006544    4.34359
HTTP1_Session  4.350134    0.006544    4.34359
HTC_RxStuff    4.350101    0.005679    4.34442
stuff1         4.349180    0.005678    4.3435
VTCP_read      4.349180    0.005678    4.3435
poll_split     4.349179    0.005677    4.3435
VTIM_sleep     0.708095    7.391500    6.
```

TraceTime1 is the first log file time in seconds and TraceTime2 is the second log file time in seconds. Difference is the difference between those two times.

## Changing variables

### FILTER=max
[      min,       med,       90p,       99p,       max]       sum

Change this variable to filter to the above values, so for example FILTER=min takes the quickest times from functions, default is max (splits from llvm-xray convert output)

### THRESHOLD=0.001

Change this variable to filter via the difference of time between two functions

For example, if function has a time of 0.001 (normal) vs 0.002 (under attack), then the function would be added to the output

But if the function has a time of 0.001 (normal) vs 0.0015 (under attack), then the function would not be added to the output

### Path to varnishd

INSTR_MAP_PATH=/home/henry/varnish_build/sbin/varnishd

# XRay -> Flowdar format

convert-xray-format-to-flowdar-format.sh [xray log file]

Modify variables with paths to point to the correct paths in convert-xray-format-to-flowdar-format.sh


