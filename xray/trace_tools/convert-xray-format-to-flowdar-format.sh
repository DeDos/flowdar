#!/bin/bash
#
#Copyright (c) 2019, University of Pennsylvania
#All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the <organization> nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
#DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#
#Converts the xray format to flowdar format
#Henry Zhu July 2018

if [[ $# -ne 1 ]];
then
	echo "Usage ./convert_xray_format_to_trace_format.sh [xray-log...]"
	echo "Please do not have backslashs \\ in your argument"
	exit
fi

LLVM_XRAY="/home/henry/xray_dev/llvm-build-base/bin/llvm-xray"
LLVM_XRAY_FLAGS="convert -f yaml -symbolize -instr_map=./nginx"
XRAY_TRACE_BINARY="$1"
LLVM_XRAY_TRACE="llvm_xray_trace"

$LLVM_XRAY $LLVM_XRAY_FLAGS $XRAY_TRACE_BINARY > $LLVM_XRAY_TRACE

#parse through the xray yaml output and grab the necessary mapping (we get an output of "function: main kind: function-enter ..."
FUNCTION_VARIABLES_DICTIONARY=$(awk -F "," '{
                                for(i = 1; i <= NF; i++)
                                {
                                    if(index($i, "function:"))
                                    {
                                        the_function=$i;
                                    }
                                    if(index($i, "kind:"))
                                    {
                                        kind=$i;
                                    }
                                    if(index($i, "thread:"))
                                    {
                                        thread=$i;
                                    }
                                    if(index($i, "tsc:"))
                                    {
                                        ts=$i;
                                    }
                                    if(index($i, "args:"))
                                    {
                                        args=$i;
                                    }
                                }
                                printf "%s %s %s %s %s\n", the_function, kind, thread, ts, args
			        #make sure variables are not cached
			        the_function = "";
				kind = "";
				thread = "";
				ts = "";
				args = "";
                                }' $LLVM_XRAY_TRACE)

#grab the values mapped to each variable, so for example if we have "function: main", this would grab "main"
#the format is "function: [function name]" ..., so hardcode each dictionary element
FUNCTION_VARIABLES=$(echo "$FUNCTION_VARIABLES_DICTIONARY" | awk -F " " '{ printf "%s %s %s %s %s\n", $2, $4, $6, $8, $12 }')

#parse through the variables and convert to flowdar format
echo "$FUNCTION_VARIABLES" | awk -F " " '
					BEGIN{
						stack[0]="main";
						function_type="INVALID";
					}
					function format_to_trace(function_type, caller, callee, ts, tid, arg1)
					{
						#print call/return

						trace_string = "";
						if(function_type == "call")
							trace_string = trace_string ">";
						else if(function_type == "return")
							trace_string = trace_string "<";
						else
							return "";

						trace_string = trace_string " ";

						#caller name
						if(caller == "")
							trace_string = trace_string "*INVALID*";
						else
							trace_string = trace_string caller;

						#caller linenum
						trace_string = trace_string ":0 ";

						#callee name
						if(callee == "")
							trace_string = trace_string "*INVALID*";
						else
							trace_string = trace_string callee;

						#callee linenum
						trace_string = trace_string ":0 ";

						#pid
						trace_string = trace_string "0 ";

						#ts (second)
						trace_string = trace_string "0 ";

						#ts (usecond)
						trace_string = trace_string ts;
						trace_string = trace_string " ";

						#tid
						trace_string = trace_string tid;
						trace_string = trace_string " ";

						if(function_type == "call")
						{
							#arg1
							trace_string = trace_string sprintf("0x%x", int(arg1));
							trace_string = trace_string " ";

							#rest of args
							trace_string = trace_string "0x0 ";
							trace_string = trace_string "0x0 ";
							trace_string = trace_string "0x0 ";
							trace_string = trace_string "0x0 ";
							trace_string = trace_string "0x0 ";
						}
						else
						{
							#return value
							trace_string = trace_string "0x0 ";
						}

						#print stack
						trace_string = trace_string sprintf("0x%0256x", 0);
						return trace_string;
					}
					function arr_len(arr)
					{
						k = 0;
						for(i in arr)
						{
							k++;
						}
						return k;
					}
					{
						caller = stack[arr_len(stack) - 1];
						callee = $1;
						function_type = $2;
						tid = $3;
						ts = $4;
						arg1 = $5;
						if(function_type == "function-enter" || function_type == "function-enter-arg")
						{
							function_type = "call";
							stack[arr_len(stack)] = callee;
						}
						else if(function_type == "function-exit" || function_type == "function-tail-exit")
						{
							function_type = "return";
							delete stack[arr_len(stack) - 1]; #pop from stack
							caller = stack[arr_len(stack) - 1]; #take from top of stack
						}
						else
						{
							function_type = "";
					    	}
						formatted_string = format_to_trace(function_type, caller, callee, ts, tid, arg1);
					        if(formatted_string != "" && function_type != "")
						{
							printf("%s\n", formatted_string);
						}
                                        }'
