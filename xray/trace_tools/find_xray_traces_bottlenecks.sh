#!/bin/bash
#
#Copyright (c) 2019, University of Pennsylvania
#All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the <organization> nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
#DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Henry Zhu, UPenn

LLVM_XRAY=$1
TRACE_ONE=$2
TRACE_TWO=$3

#threshold between time threshold
FILTER=max
THRESHOLD=0.001
INSTR_MAP_PATH=/home/henry/varnish_build/sbin/varnishd

TRACE_ONE_ACCOUNT="${TRACE_ONE}.account"
TRACE_TWO_ACCOUNT="${TRACE_TWO}.account"

TRACE_ONE_TIME_AND_FUNCTION="${TRACE_ONE}.time_and_function"
TRACE_TWO_TIME_AND_FUNCTION="${TRACE_TWO}.time_and_function"

TRACE_TIME_AND_FUNCTION="time_and_function"

RESULT="results"

#set -x

###############################################################################
### Use llvm xray to read the traces and sort by function id
###############################################################################

$LLVM_XRAY account -format=text -instr_map=$INSTR_MAP_PATH -sort=$FILTER -sortorder=dsc -keep-going $TRACE_ONE > $TRACE_ONE_ACCOUNT 2> /dev/null
$LLVM_XRAY account -format=text -instr_map=$INSTR_MAP_PATH -sort=$FILTER -sortorder=dsc -keep-going $TRACE_TWO > $TRACE_TWO_ACCOUNT 2> /dev/null

#awk script prints the function id, function name and time
AWK_GATHER_TIME_AND_FUNCTION_SCRIPT='
            function find_index_of(str)
            {
              for(i = 1; i <= NF; i++)
              {
                found_index = index($i, str);
                if(found_index != 0)
                {
                  return i;
                }
              }
              return -1;
            }
            BEGIN { FILTER_COLUMN=-1; }
            {
              if (FILTER_COLUMN != -1)
              {
                ID=$1;
                TIME=substr($FILTER_COLUMN, 1, 8);
                FUNCTION=$11
                if (ID && TIME && FUNCTION)
                {
                  print ID, FUNCTION, TIME;
                }
              }
              else
              {
                FILTER_COLUMN=find_index_of(FILTER);
              }
            }'

#time and function extracted
awk -vFILTER="$FILTER" "$AWK_GATHER_TIME_AND_FUNCTION_SCRIPT" $TRACE_ONE_ACCOUNT > $TRACE_ONE_TIME_AND_FUNCTION
awk -vFILTER="$FILTER" "$AWK_GATHER_TIME_AND_FUNCTION_SCRIPT" $TRACE_TWO_ACCOUNT > $TRACE_TWO_TIME_AND_FUNCTION

###############################################################################
### Filter out functions that are not called by the trace
###############################################################################

# Get the number of lines and find which trace contains the less number of different functions that are called
TRACE_ONE_NUM_LINES=$(wc -l $TRACE_ONE_TIME_AND_FUNCTION | awk '{print $1}')
TRACE_TWO_NUM_LINES=$(wc -l $TRACE_TWO_TIME_AND_FUNCTION | awk '{print $1}')

if [ "$TRACE_ONE_NUM_LINES" -lt "$TRACE_TWO_NUM_LINES" ]; then
  TRACE_LOWER_LINES=$TRACE_ONE_TIME_AND_FUNCTION
  TRACE_HIGHER_LINES=$TRACE_TWO_TIME_AND_FUNCTION
else
  TRACE_LOWER_LINES=$TRACE_TWO_TIME_AND_FUNCTION
  TRACE_HIGHER_LINES=$TRACE_ONE_TIME_AND_FUNCTION
fi

awk 'FNR==NR{a[$1]=$1;b[$1]=$3;next} ($1 in a) {print $2, $3, b[$1]}' $TRACE_HIGHER_LINES $TRACE_LOWER_LINES > $TRACE_TIME_AND_FUNCTION

###############################################################################
### Filter out functions that are not called by the trace
###############################################################################

AWK_FIND_TIME_SCRIPT='
            BEGIN { print "FunctionName TraceTime1 TraceTime2 Difference" }
            function abs(x) { return x > 0.0 ? x : -x }
            {
              FUNCTION_NAME=$1;
              TIME_ONE=$2;
              TIME_TWO=$3;
              TIME_DIFF=abs(TIME_ONE - TIME_TWO);
              if(TIME_DIFF > THRESHOLD)
              {
                print FUNCTION_NAME, TIME_ONE, TIME_TWO, TIME_DIFF
              }

            }'

awk -vTHRESHOLD="$THRESHOLD" "$AWK_FIND_TIME_SCRIPT" $TRACE_TIME_AND_FUNCTION | column -t


