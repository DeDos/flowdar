![Flowdar](flowdar.png)

Flowdar is an [extensively documented](documentation) **toolset** for
understanding how computer programs work.

In particular, Flowdar helps understand the **flow** of **control** and
**data**, and understand **resource usage** by the application.

Flowdar works by generating a trace while the program is running, and then analysing
the trace off-line. You can configure how much detail you want the trace to
contain -- the less information you need, the less overhead is required to
produce that information.

Find out more by reading our [paper on Flowdar](https://www.seas.upenn.edu/~nsultana/files/flowdar_cnsm19.pdf).

## Components
Flowdar consists of tools for:
* [generating](generation) traces
* processing traces -- to extract individual client's subtrace, for both [thread-based](results/sep_thread.py) and [event-based](trace_processing) examples.
* [visualising](presentation) traces
* [navigating](trace_explorer) traces
* [converting](xray) traces to interoperate with [XRay](https://www.llvm.org/docs/XRay.html), another tracing system.

## Examples
We have gathered an example set of [workloads](workloads) and [results](results).

## License
[3-clause BSD](LICENSE)

## Version
0.1

[CHANGELOG](CHANGELOG.md)

## Enquiries/support
Contact [Nik Sultana](https://www.seas.upenn.edu/~nsultana/)

## Acknowledgements
* Built by an awesome team of [contributors](CONTRIBUTORS)
* Built during the [DeDOS project](https://dedos.cis.upenn.edu/) at the [University of Pennsylvania](https://www.seas.upenn.edu/)
