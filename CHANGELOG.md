## 0.1 -- April 2019
* First public release.
* Includes full tools for generating, processing, visualisating, nagivating traces, and converting XRay traces to Flowdar format.
* Includes documentation and examples of using the tools.
