/*
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


Define tracing hooks.
Nik Sultana, UPenn, May 2017
Zihao Jin, UPenn, August 2017

Found this post very instructive: https://balau82.wordpress.com/2010/10/06/trace-and-profile-function-calls-with-gcc/

NOTE:
* only works with GCC
* isn't thread-aware
* isn't portable (e.g., expects 64-bit memory addresses)

*/

#include "common.h"
#include <sys/syscall.h>
#include <execinfo.h>
#include <unistd.h>

static int trace_fd;

// Only a stub here.
// We could implement a per-process output buffer inside tracer_hooks.c,
// as long as we could distinguish between calls to buf_write() from different threads,
// and handles them properly so that they won't overwrite each other's output buffer.
static inline void buf_write(const void *buf, size_t n) {
    write(trace_fd, buf, n);
}

void
__attribute__ ((constructor))
trace_begin (void)
{
  assert(8 == sizeof(long));
  assert(sizeof(long) == sizeof(void *));

  char * filename = getenv("TRACE_FILE");
  if (NULL == filename) {
    filename = QUOTE(TRACE_FILE);
  }

  struct stat filename_stat;
  int result = stat(filename, &filename_stat);
#if VERBOSE
  if (!(0 == result)) {
    fprintf(stderr, "Couldn't stat trace file: %s\n", strerror(errno));
  }
#endif // VERBOSE
  assert(0 == result);
  assert(S_ISFIFO(filename_stat.st_mode));

  trace_fd = open(filename, O_WRONLY);
#if VERBOSE
  if (!(-1 != trace_fd)) {
    fprintf(stderr, "Couldn't open trace file: %s\n", strerror(errno));
  }
#endif // VERBOSE
  assert (-1 != trace_fd);
}

void
__attribute__ ((destructor))
trace_end (void)
{
}

inline void
__cyg_profile_func_enter (void * callee, void * caller)
{
  struct log_entry entry;

#ifdef CALLSTACK
    asm("movq %%rdi, %0" : "=r" (entry.rdi));
    asm("movq %%rsi, %0" : "=r" (entry.rsi));
    asm("movq %%rdx, %0" : "=r" (entry.rdx));
    asm("movq %%rcx, %0" : "=r" (entry.rcx));
    asm("movq %%r8, %0" : "=r" (entry.r8));
    asm("movq %%r9, %0" : "=r" (entry.r9));
    // The argument callee is passed via %rax (instead of %rsi) after patching.
    asm("movq %%rax, %0" : "=r" (entry.callee));
#endif // CALLSTACK

  entry.direction = FUNCTION_CALL;
  // Should not assign caller and callee here, as rdi and rsi register are preserved, thus not used for argument passing.
#ifndef CALLSTACK
  entry.caller = caller;
  entry.callee = callee;
#endif // CALLSTACK
  entry.pid = getpid();
  // The macro __NR_gettid is found here: /usr/include/asm/unistd_64.h
  // On Linux x86_64, __NR_gettid = 186
  entry.tid = (long)syscall(__NR_gettid);

#if CHECKED
    int result =
#endif // CHECKED
    gettimeofday(&entry.timestamp, NULL);
#if CHECKED
    assert(0 == result);
#endif // CHECKED

#ifdef CALLSTACK
    memset(entry.callstack, 0, CALLSTACK_SIZE);

    void *backtrace_data[3];
    backtrace(backtrace_data, 3);

    entry.caller = backtrace_data[2];
    if (__builtin_frame_address(0) && __builtin_frame_address(1)) {
        memcpy(entry.callstack, __builtin_frame_address(1) - (CALLSTACK_SIZE
#ifdef BACKTRACE
            >> 2
#endif // BACKTRACE
        ) + SAVED_RBP_AND_RETURN_ADDR_SIZE, CALLSTACK_SIZE
#ifdef BACKTRACE
            >> 2
#endif // BACKTRACE
        );
    }

#ifdef BACKTRACE
        char **backtrace_text = backtrace_symbols(backtrace_data, 3);
        int i = 0;
        for (i = 0; i < 3; ++i) {
            snprintf(entry.callstack + (CALLSTACK_SIZE >> 2) + BACKTRACE_SIZE * i, BACKTRACE_SIZE, "%s\n", backtrace_text[i]);
        }
#endif // BACKTRACE
#endif // CALLSTACK

#if CHECKED
    result =
#endif // CHECKED
    buf_write(&entry, sizeof(struct log_entry));
#if CHECKED
    assert(sizeof(struct log_entry) == result);
#endif // CHECKED
}

// Much of the code in __cyg_profile_func_enter() is repeated here
// to explicitly inline these two functions (__cyg_profile_func_enter() and __cyg_profile_func_exit())
// Otherwise, it would be even harder to preserve the arguments, the return values and the call stacks
// when an additional layer of function call occurs.
inline void
__cyg_profile_func_exit (void * callee, void * caller)
{
  struct log_entry entry;

#ifdef CALLSTACK
  asm("movq %%rax, %0" : "=r" (entry.rax));
#endif // CALLSTACK

  entry.direction = FUNCTION_RETURN;
  entry.caller = caller;
  entry.callee = callee;
  entry.pid = getpid();
  entry.tid = (long)syscall(__NR_gettid);

#if CHECKED
    int result =
#endif // CHECKED
    gettimeofday(&entry.timestamp, NULL);
#if CHECKED
    assert(0 == result);
#endif // CHECKED

#ifdef CALLSTACK
    memset(entry.callstack, 0, CALLSTACK_SIZE);

    void *backtrace_data[3];
    backtrace(backtrace_data, 3);

    entry.caller = backtrace_data[2];
    if (__builtin_frame_address(0) && __builtin_frame_address(1)) {
        memcpy(entry.callstack, __builtin_frame_address(1) - (CALLSTACK_SIZE
#ifdef BACKTRACE
            >> 2
#endif // BACKTRACE
        ) + SAVED_RBP_AND_RETURN_ADDR_SIZE, CALLSTACK_SIZE
#ifdef BACKTRACE
            >> 2
#endif // BACKTRACE
        );
    }

#ifdef BACKTRACE
        char **backtrace_text = backtrace_symbols(backtrace_data, 3);
        int i = 0;
        for (i = 0; i < 3; ++i) {
            snprintf(entry.callstack + (CALLSTACK_SIZE >> 2) + BACKTRACE_SIZE * i, BACKTRACE_SIZE, "%s\n", backtrace_text[i]);
        }
#endif // BACKTRACE
#endif // CALLSTACK

#if CHECKED
    result =
#endif // CHECKED
    buf_write(&entry, sizeof(struct log_entry));
#if CHECKED
    assert(sizeof(struct log_entry) == result);
#endif // CHECKED
#ifdef CALLSTACK
    asm("movq %0, %%rax" :: "r"(entry.rax));
#endif // CALLSTACK
}
