#!/bin/bash

#Copyright (c) 2019, University of Pennsylvania
#All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the <organization> nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
#DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


# We exchange the callq instruction and the instruction immediately after that,
# only if the callq is followed by a mov, which is usually a "mov to rax".
# By exchanging these two instructions can we get the correct return value (rax register).

# Other instructions that could follow a "callq to __cyg_profile_func_exit" include: nop, cmp, jmp.
# As we observed, the presence of those instructions indicates that the return value (rax register)
# is already determined by the time we call __cyg_profile_func_exit.
# (That is only an empirical conclusion, though.)

objdump -d $1 | grep --no-group-separator -A 1 "callq.*<__cyg_profile_func_exit.*>" | awk '{
    BINARY_OFFSET = 4194304; # The beginning address of a binary on x86-64 Linux platform, the value usually should not be changed unless ported to another platform.
    numfields = split($0, fields, "\t");
    if (numfields == 3) {
        sub(/^[ \t\r\n]+/, "", fields[1]);
        sub(/^[ \t\r\n]+/, "", fields[3]);
        numsubfields = split(fields[3], subfields, " ");
        if (subfields[1] == "callq") {
            if (substr(subfields[3], 1, 24) == "<__cyg_profile_func_exit") {
                gsub("[ :\t\r\n]", "", fields[1]);
                cmd = "echo \"ibase=16; "toupper(fields[1])"\" | bc";
                cmd | getline result;
                close(cmd);
                printf("%x\n", result - BINARY_OFFSET);
            }
            else {
                print("0")
            }
        }
        else if (subfields[1] == "mov") {
            print(split(fields[2], nextinstfields, " "));
        }
        else if (subfields[1] == "nop") {
            print("0")
        }
        else if (subfields[1] == "cmp") {
            print("0")
        }
        else if (subfields[1] == "jmp") {
            print("0")
        }
	else if (subfields[1] == "leaveq") {
	    print("0")
	}
        else {
            print("0")
# NOTE The following line can be enabled when debugging, in case the program
#      being analysed does not conform  to what we have seen so far.
#            print("<"subfields[1]">")
        }
    } else {
      # FIXME not sure if this printing to stderr is portable.
      print "patch_exit.sh: numfields != 3" > "/dev/stderr"
      exit 1
    }
}' | sed 'N;s/\n/ /'
# The sed command here seems to merge every two lines of awk output into one line.
