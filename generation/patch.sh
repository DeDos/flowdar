#!/bin/bash

#Copyright (c) 2019, University of Pennsylvania
#All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the <organization> nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
#DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


echo "Patching function entry"
# patch_enter.sh extracts the calls to __cyg_profile_func_enter
./patch_enter.sh $1 > add.cfg

echo "Patching function exit"
# patch_exit.sh extracts the calls to __cyg_profile_func_exit
./patch_exit.sh $1 > xchg.cfg

# Use add util to remove the "mov to rsi" instruction before __cyg_profile_func_enter,
# and rewrite "mov to rdi" instruction into "mov to rax" before __cyg_profile_func_enter,
# which avoids overwriting the first two parameters of the function being traced.

# As rax register is always overwritten after the call to __cyg_profile_func_enter,
# before any possible read of rax register,
# it is safe to overwrite rax here.
./add $1 $1"_p1" add.cfg

# Use xchg util to exchange the call to __cyg_profile_func_exit and the instruction immediately after that.
# As sometimes in gcc generated code, rax is determined after the call to __cyg_profile_func_exit,
# and the value of rax register should be extracted by __cyg_profile_func_exit,
# exchanging two instructions would reach our goal.
./xchg $1"_p1" $1"_p2" xchg.cfg

mv $1"_p2" $1
rm $1"_p1"
chmod +x $1
