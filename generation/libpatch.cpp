/*
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


This util contains a hardcoded patch for two patterns in typical dynamic libraries (.so),
to avoid rdi and rsi register being overwritten,
and to have callee passed via rax register for __cyg_profile_func_enter().
See comments below and patch_lib.sh for more information.
Zihao Jin, UPenn, August 2017
*/

#include <iostream>
#include <fstream>
#include <cstring>
using namespace std;

int main(int argc, char **argv) {
    if (argc != 4) {
        cout << "Usage: " << argv[0] << " <input file> <output file> <script file>" << endl;
        return 0;
    }
    ifstream fin(argv[3]);

    char *buffer;
    long size;
    ifstream fin_binary(argv[1], ios::in|ios::binary|ios::ate);
    size = fin_binary.tellg();
    fin_binary.seekg(0, ios::beg);
    buffer = new char[size];
    fin_binary.read(buffer, size);
    fin_binary.close();

    cout << "input file size = " << size << endl;

    int offset;
    int data;
    string line;

    int count = 0;
    int sizes[3];
    while (getline(fin, line)) {
        if (count % 4 != 3) {
            sscanf(line.c_str(), "%d", sizes + count % 4);
        }
        else {
            if (sizes[0] == 4 && sizes[1] == 3 && sizes[2] == 7) {
                // mov, mov, lea
                /*
                   40d6a:	48 8b 45 08          	mov    0x8(%rbp),%rax
                   40d6e:	48 89 c6             	mov    %rax,%rsi
                   40d71:	48 8d 3d ce ff ff ff 	lea    -0x32(%rip),%rdi        # 40d46 <getpwnam_safe>
                   40d78:	e8 33 db fc ff       	callq  e8b0 <__cyg_profile_func_enter@plt>

                   rewritten into:

                   40d6a:	90 90 90 90 90          nop; nop; nop; nop; nop;
                   40d6e:	90 90 90             	nop; nop; nop;
                   40d71:	48 8d 05 ce ff ff ff 	lea    -0x32(%rip),%rax        # 40d46 <getpwnam_safe>
                   40d78:	e8 33 db fc ff       	callq  e8b0 <__cyg_profile_func_enter@plt>
                 */
                sscanf(line.c_str(), "%x", &offset);
                *(unsigned char *)(buffer + offset - 14) = 0x90;
                *(unsigned char *)(buffer + offset - 13) = 0x90;
                *(unsigned char *)(buffer + offset - 12) = 0x90;
                *(unsigned char *)(buffer + offset - 11) = 0x90;
                *(unsigned char *)(buffer + offset - 10) = 0x90;
                *(unsigned char *)(buffer + offset - 9) = 0x90;
                *(unsigned char *)(buffer + offset - 8) = 0x90;
                *(unsigned char *)(buffer + offset - 5) = 0x05;
                printf("patched (mov, mov, lea) at 0x%x\n", offset);
            }
            else if (sizes[0] == 3 && sizes[1] == 7 && sizes[2] == 3) {
                // mov, mov, mov
                /*
                    40e24:	48 89 c6             	mov    %rax,%rsi
                    40e27:	48 8b 05 82 bc 20 00 	mov    0x20bc82(%rip),%rax        # 24cab0 <_DYNAMIC+0xe40>
                    40e2e:	48 89 c7             	mov    %rax,%rdi
                    40e31:	e8 7a da fc ff       	callq  e8b0 <__cyg_profile_func_enter@plt>

                    rewritten into:

                    40e24:	90 90 90             	nop; nop; nop;
                    40e27:	48 8b 05 82 bc 20 00 	mov    0x20bc82(%rip),%rax        # 24cab0 <_DYNAMIC+0xe40>
                    40e2e:	90 90 90            	nop; nop; nop;
                    40e31:	e8 7a da fc ff       	callq  e8b0 <__cyg_profile_func_enter@plt>
                 */
                sscanf(line.c_str(), "%x", &offset);
                *(unsigned char *)(buffer + offset - 13) = 0x90;
                *(unsigned char *)(buffer + offset - 12) = 0x90;
                *(unsigned char *)(buffer + offset - 11) = 0x90;
                *(unsigned char *)(buffer + offset - 3) = 0x90;
                *(unsigned char *)(buffer + offset - 2) = 0x90;
                *(unsigned char *)(buffer + offset - 1) = 0x90;
                printf("patched (mov, mov, mov) at 0x%x\n", offset);
            }
            else {
                printf("Warning: unrecognized pattern at line 0x%x\n", offset);
            }
        }
        ++count;
    }

    fin.close();

    ofstream fout(argv[2]);
    fout.write(buffer, size);
    fout.close();
    return 0;
}
