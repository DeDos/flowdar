/*
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


Test program for our tracer.
Nik Sultana, UPenn, July 2017
Zihao Jin, UPenn, August 2017
*/

#include <stdio.h>

void
fun1_void_void(void)
{
  printf("1");
}

int
fun2_int_void(void)
{
  printf("2");
  return 5;
}

int
fun3_int_int(int x)
{
  printf("3");
  return x;
}

int
fun4_int_int(int x)
{
  printf("4");
  return x + 3;
}

int
fun5_int_int(int x)
{
  int y = 7;
  printf("5");
  return x - y;
}

int
fun6_int_int(int x)
{
  int y = x - 7;
  printf("6");
  return x - y;
}

int
fun7_int_intint(int x, int y)
{
  int z = x + y;
  int y0 = x - y;
  int x0 = z - y;
  printf("7");
  return x0 - y0;
}

void
fun8_void_void()
{
  printf("8");
}

void
main()
{
  int x;
  int y;
  fun1_void_void();
  x = fun2_int_void();
  y = fun3_int_int(19);
  x = fun4_int_int(x);
  y = fun5_int_int(y);
  x = fun6_int_int(x);
  y = fun7_int_intint(x, y);
  void (*fptr)() = fun8_void_void;
  fptr();
  printf("\n");
  printf("main:x=%d\n", x);
  printf("main:y=%d\n", y);
}
