/*
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


Buffers and forwards trace.
Nik Sultana, UPenn, June 2016
Zihao Jin, UPenn, August 2017
*/

#include "common.h"

#include <errno.h>
#include <stdbool.h>
#include <stdint.h>

int
main (int argc, char ** argv)
{
  char * input_filename = NULL;
  char * output_filename = NULL;
  uint16_t buff_size = sizeof(struct log_entry);
  bool verbose = false;
  bool persistent = false;

  int flag;
  char * final_pos;
  while ((flag = getopt(argc, argv, "b:i:o:vp")) != -1) {
    switch (flag) {
    case 'b':
      assert(0 == errno);
      long pre_result = strtol(optarg, &final_pos, 10);
      assert(pre_result >= 0 && pre_result <= UINT16_MAX && 0 == errno && '\0' == *final_pos);
      buff_size = (uint16_t)pre_result;
      assert(buff_size >= sizeof(struct log_entry));
      // FIXME not checking if buff_size <= PIPE_BUF
      break;
    case 'i':
      input_filename = optarg;
      break;
    case 'o':
      output_filename = optarg;
      break;
    case 'v':
      verbose = true;
      break;
    case 'p':
      persistent = true;
      break;
    default:
      abort();
    }
  }

  if (verbose) {
    fprintf(stderr, "input_filename = %s\n", input_filename);
    fprintf(stderr, "output_filename = %s\n", output_filename);
    fprintf(stderr, "buff_size = %d\n", buff_size);
    fprintf(stderr, "sizeof(struct log_entry) = %lu\n", sizeof(struct log_entry));
  }

  // Establish trace input filename
  if (NULL == input_filename) {
    input_filename = getenv("TRACE_FILE");
    if (NULL == input_filename) {
      input_filename = QUOTE(TRACE_FILE);
    }

    assert(NULL != input_filename);
    if (verbose) {
      fprintf(stderr, "input_filename = %s\n", input_filename);
    }
  }

  // Check trace input
  struct stat filename_stat;
  int result = stat(input_filename, &filename_stat);
#if VERBOSE
  if (!(0 == result)) {
    fprintf(stderr, "Couldn't stat trace file: %s\n", strerror(errno));
  }
#endif // VERBOSE
  assert(0 == result);
  assert(S_ISFIFO(filename_stat.st_mode));

  // Check trace output
  result = stat(output_filename, &filename_stat);
#if VERBOSE
  if (!(0 == result)) {
    fprintf(stderr, "Couldn't stat _output_ trace file: %s\n", strerror(errno));
  }
#endif // VERBOSE
  assert(0 == result);
  assert(S_ISFIFO(filename_stat.st_mode));

  int outfile_fd = open(output_filename, O_WRONLY);
#if VERBOSE
  if (!(-1 != outfile_fd)) {
    fprintf(stderr, "Couldn't open _output_ trace file: %s\n", strerror(errno));
  }
#endif // VERBOSE
  assert(-1 != outfile_fd);

  int trace_fd = open(input_filename, O_RDONLY);
#if VERBOSE
  if (!(-1 != trace_fd)) {
    fprintf(stderr, "Couldn't open trace file: %s\n", strerror(errno));
  }
#endif // VERBOSE
  assert (-1 != trace_fd);

  char buf[buff_size];
  char *buf_ptr = buf;
  int bytes_read;
  int bytes_written;
  while (true) {
    bytes_read = read(trace_fd, buf_ptr, sizeof(struct log_entry));
    if (bytes_read <= 0) {
      if (persistent) {
        continue;
      }
      else {
        break;
      }
    }

    buf_ptr += bytes_read;
    if (buf_ptr + sizeof(struct log_entry) > buf + buff_size) {
      bytes_written = write(outfile_fd, buf, buf_ptr - buf);
#if CHECKED
        assert(bytes_written == bytes_read);
#endif // CHECKED
      buf_ptr = buf;
    }
  }

  close(trace_fd);
  close(outfile_fd);
}
