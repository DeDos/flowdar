/*
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


Experiments with in-memory tracing.
Nik Sultana, UPenn, June 2016
Zihao Jin, UPenn, August 2017
*/

#ifndef __CHOPCHOP_COMMON_H_
#define  __CHOPCHOP_COMMON_H_

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/time.h>
#include <stdint.h>

#if !defined TRACE_FILE
#error "Must set TRACE_FILE"
#endif

#define _QUOTE(S) #S
#define QUOTE(S) _QUOTE(S)

#define FUNCTION_CALL '>'
#define FUNCTION_RETURN '<'

#ifdef CALLSTACK
    #ifdef BACKTRACE
        #define CALLSTACK_SIZE 512
        #define BACKTRACE_SIZE 96
    #else
        #define CALLSTACK_SIZE 128
    #endif // BACKTRACE
    #define SAVED_RBP_AND_RETURN_ADDR_SIZE 16
#endif // CALLSTACK

struct log_entry {
    struct timeval timestamp;
    void *caller;
    void *callee;
#ifdef CALLSTACK
    // Added data fields:
    uint64_t rdi;
    uint64_t rsi;
    uint64_t rdx;
    uint64_t rcx;
    uint64_t r8;
    uint64_t r9;
    uint64_t rax;
#endif // CALLSTACK
    pid_t pid;
    long tid;
    char direction;
#ifdef CALLSTACK
    // Added data fields:
    unsigned char callstack[CALLSTACK_SIZE];
#endif // CALLSTACK
};

#endif // __CHOPCHOP_COMMON_H_
