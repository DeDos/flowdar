/*
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


Process in-memory traces -- such as reading and converting them into ASCII.
Nik Sultana, UPenn, June 2017
Zihao Jin, UPenn, August 2017
*/

#include <stdbool.h>

#include "common.h"

#ifdef CALLSTACK
  // Printing 512 bytes on the call stack requires 1553 bytes here,
  // and other information requires no more than 100 bytes currently, so 1800 should be enough.
  #define STR_BUF_SIZE 1800
#else
  #define STR_BUF_SIZE 300
#endif // CALLSTACK

#define ACTIVITY_DOT_INTERVAL 20

void convert_trace (char * input_filename, char * output_filename);
void log_trace (char * input_filename, char * output_filename);
void discard_trace (char * input_filename);
void check_input_tracechannel (char * input_filename);
void bin_to_ascii (char * buf, char * str_buf);
int open_output_fd (char * output_filename);

bool verbose = false;
bool persistent = false;

int
main (int argc, char ** argv)
{
  char * input_filename = NULL;
  char * output_filename = NULL;
  enum mode {LOG = 0, CONVERT, DISCARD};
  enum mode op_mode = LOG;

  int flag;
  while ((flag = getopt(argc, argv, "cdi:o:pv")) != -1) {
    switch (flag) {
    case 'c':
      op_mode = CONVERT;
      break;
    case 'd':
      op_mode = DISCARD;
      break;
    case 'i':
      input_filename = optarg;
      break;
    case 'o':
      output_filename = optarg;
      break;
    case 'p':
      persistent = true;
      break;
    case 'v':
      verbose = true;
      break;
    default:
      abort();
    }
  }

  if (verbose) {
    fprintf(stderr, "input_filename = %s\n", input_filename);
    fprintf(stderr, "output_filename = %s\n", output_filename);
    fprintf(stderr, "op_mode = %d\n", op_mode);
    fprintf(stderr, "persistent = %d\n", persistent);
    fprintf(stderr, "sizeof(struct log_entry) = %lu\n", sizeof(struct log_entry));
  }

  switch (op_mode) {
    case LOG:
      log_trace(input_filename, output_filename);
      break;
    case CONVERT:
      convert_trace(input_filename, output_filename);
      break;
    case DISCARD:
      discard_trace(input_filename);
      break;
    default:
      abort();
  }
}

#ifdef CALLSTACK
inline void mem_dump(unsigned char *mem, char *str) {
    char *ptr = str;
    int i = 0;
    for (i = 0; i < (CALLSTACK_SIZE
#ifdef BACKTRACE
        >> 2
#endif // BACKTRACE
    ); i += 1) {
        sprintf(ptr, "%02x", mem[i]);
        ptr += 2;
    }
#ifdef BACKTRACE
        sprintf(ptr, "\n");
        ptr += 1;
        memcpy(ptr, mem + (CALLSTACK_SIZE >> 2), (CALLSTACK_SIZE >> 2) + (CALLSTACK_SIZE >> 1));
        for (i = 0; i < (CALLSTACK_SIZE >> 2) + (CALLSTACK_SIZE >> 1) - BACKTRACE_SIZE; ++i) {
            if (i % BACKTRACE_SIZE == (BACKTRACE_SIZE - 1)) {
                ptr[i] = '\n';
            }
            else if (ptr[i] == 0 || ptr[i] == '\n') {
                ptr[i] = ' ';
            }
        }
#endif // BACKTRACE
}
#endif // CALLSTACK

inline void
bin_to_ascii(char * buf, char * str_buf)
{
  struct log_entry *entry = (struct log_entry *)buf;
  assert(FUNCTION_CALL == entry->direction || FUNCTION_RETURN == entry->direction);
#ifdef CALLSTACK
  char buffer[(CALLSTACK_SIZE >> 5) + CALLSTACK_SIZE * 3 + 1];
  memset(buffer, 0, sizeof(buffer));
  mem_dump(entry->callstack, buffer);
  if (FUNCTION_CALL == entry->direction) {
      snprintf(str_buf, STR_BUF_SIZE, "%c %p %p %d %lu %lu %lu 0x%lx 0x%lx 0x%lx 0x%lx 0x%lx 0x%lx %s\n",
        entry->direction, entry->caller, entry->callee, entry->pid,
        entry->timestamp.tv_sec, entry->timestamp.tv_usec, entry->tid, entry->rdi, entry->rsi, entry->rdx, entry->rcx, entry->r8, entry->r9, buffer);
  }
  else if (FUNCTION_RETURN == entry->direction) {
    snprintf(str_buf, STR_BUF_SIZE, "%c %p %p %d %lu %lu %lu 0x%lx %s\n",
        entry->direction, entry->caller, entry->callee, entry->pid,
        entry->timestamp.tv_sec, entry->timestamp.tv_usec, entry->tid, entry->rax, buffer);
  }
#else
  snprintf(str_buf, STR_BUF_SIZE, "%c %p %p %d %lu %lu %d\n",
           entry->direction, entry->caller, entry->callee, entry->pid,
           entry->timestamp.tv_sec, entry->timestamp.tv_usec, entry->tid);
#endif // CALLSTACK

}

void
convert_trace (char * input_filename, char * output_filename)
{
  assert(!persistent); // Persistence wouldn't make sense here.

  // FIXME check that input_filename exists, and that output_filename doesn't exist.
  int trace_fd = open(input_filename, O_RDONLY);
  assert(-1 != trace_fd);

  int outfile_fd = open_output_fd(output_filename);

  char buf[sizeof(struct log_entry)];
  char str_buf[STR_BUF_SIZE];

  int bytes_read;
  int bytes_written;
  while (bytes_read = read(trace_fd, buf, sizeof(struct log_entry))) {
    if (bytes_read <= 0) {
      break;
    }
    bin_to_ascii(buf, str_buf);
    bytes_written = write(outfile_fd, str_buf, strlen(str_buf));
#if CHECKED
    assert(bytes_written == bytes_read);
#endif // CHECKED
  }

  close(outfile_fd);
}

void
check_input_tracechannel (char * input_filename)
{
  // Establish trace input filename
  if (NULL == input_filename) {
    input_filename = getenv("TRACE_FILE");
    if (NULL == input_filename) {
      input_filename = QUOTE(TRACE_FILE);
    }

    assert(NULL != input_filename);
    if (verbose) {
      fprintf(stderr, "input_filename = %s\n", input_filename);
    }
  }

  // Check trace input
  struct stat filename_stat;
  int result = stat(input_filename, &filename_stat);
#if VERBOSE
  if (!(0 == result)) {
    fprintf(stderr, "Couldn't stat trace file: %s\n", strerror(errno));
  }
#endif // VERBOSE
  assert(0 == result);
  assert(S_ISFIFO(filename_stat.st_mode));
}

int
open_output_fd (char * output_filename)
{
  int outfile_fd = STDOUT_FILENO;
  if (NULL != output_filename) {
    // Check trace output
    struct stat filename_stat;
    int result = stat(output_filename, &filename_stat);
#if VERBOSE
    if (!(0 == result)) {
      fprintf(stderr, "Couldn't stat _output_ trace file: %s\n", strerror(errno));
    }
#endif // VERBOSE
    outfile_fd = open(output_filename, O_WRONLY | O_CREAT, S_IRUSR | S_IRGRP | S_IROTH);
    assert(-1 != outfile_fd);
  }
  return outfile_fd;
}

void
log_trace (char * input_filename, char * output_filename)
{
  check_input_tracechannel(input_filename);

  int outfile_fd = open_output_fd(output_filename);

  int trace_fd = open(input_filename, O_RDONLY);
#if VERBOSE
  if (!(-1 != trace_fd)) {
    fprintf(stderr, "Couldn't open (input) trace file: %s\n", strerror(errno));
  }
#endif // VERBOSE
  assert (-1 != trace_fd);

  char buf[sizeof(struct log_entry)];
  char str_buf[STR_BUF_SIZE];
  int bytes_read;
  int bytes_written;

  while (true) {
    bytes_read = read(trace_fd, buf, sizeof(struct log_entry));
    if (bytes_read <= 0) {
      if (persistent)
        continue;
      else
        break;
    } else {
      if (STDOUT_FILENO == outfile_fd) {
        bin_to_ascii(buf, str_buf);
        bytes_written = write(outfile_fd, str_buf, strlen(str_buf));
      } else {
        bytes_written = write(outfile_fd, buf, sizeof(struct log_entry));
      }
#if CHECKED
      assert(bytes_written == bytes_read);
#endif // CHECKED
    }
  }

  close(trace_fd);
  close(outfile_fd);
}

void
discard_trace (char * input_filename)
{
  check_input_tracechannel(input_filename);

  setbuf(stdout, NULL);

  int trace_fd = open(input_filename, O_RDONLY);
#if VERBOSE
  if (!(-1 != trace_fd)) {
    fprintf(stderr, "Couldn't open trace file: %s\n", strerror(errno));
  }
#endif // VERBOSE
  assert (-1 != trace_fd);

  char buf[sizeof(struct log_entry)];
  int bytes_read;
  int countdown = 0;
  while (true) {
    bytes_read = read(trace_fd, buf, sizeof(struct log_entry));
    if (bytes_read <= 0) {
      if (persistent)
        continue;
      else
        break;
    } else {
      if (0 >= countdown) {
        printf(".");
        countdown = ACTIVITY_DOT_INTERVAL;
      } else {
        countdown--;
      }
    }
  }

  close(trace_fd);
  printf("\n");
}
