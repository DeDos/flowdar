Step-by-Step Setup Guide
--
The following steps are tested on Ubuntu Server 16.04.3.

NOTE: Privilege-related issues are not covered in the following instructions. Please use `sudo` when necessary, depending on your environment.

1. Download Apache httpd 2.4.26 source code, unpack it.
2. Download APR 1.6.2 source code, unpack it.
3. Download APR-util 1.6.0 source code, unpack it.
4. Download PCRE 8.40, unpack it.
5. Download OpenSSL 1.0.2l, unpack it.
6. Install the required packages with `apt-get install libtool libexpat1-dev g++ make libdata-compare-perl libssl-dev bc python`.
7. Create a directory called "apache_build_env" and move httpd 2.4.26, APR 1.6.2, APR-util 1.6.0, PCRE 8.40, and OpenSSL 1.0.2l to it.
8. Run ./configure, make, and sudo make install in PCRE 8.40.
9. Run ./config, make, and sudo make install in OpenSSL 1.0.2l.
10. Download r.sh and chopchop, and put them in the same directory as apache_build_env.
11. Change the paths in r.sh according to previous steps.
12. Make r.sh executable by running `chmod +x r.sh`.
13. Create a fifo file named "apache.local.trace" under your $OUTPUT_PATH (which is specified in your r.sh) In other words, `cd` into your $OUTPUT_PATH and `mkfifo apache.local.trace`.
14. Open chopchop/dyngraph/generation/tracer_hooks.s with a text editor (e.g. gedit), change the path "/home/jinzihao/apache_build_env/outputs/apache.local.trace", so that it points to your fifo file.
15. Build tracer with: `./r.sh tb -DCALLSTACK`.
16. Open a new terminal window and run tracer with: `./r.sh tr`.
17. Build APR with `./r.sh aprb` to attach the tracer to APR and APR-util.
18. Build Apache httpd with: `./r.sh ab` to attach the tracer to Apache.
19. Copy the httpd.conf from chopchop/dyngraph/results/1a/ to /usr/local/apache2/conf/. Modify the paths inside httpd.conf according to your environment. Alternatively, you can modify the existing httpd.conf according to "Diff from default conf (of httpd.conf)" section in https://gitlab.com/DeDos/chopchop/tree/master/dyngraph/workloads/apache2.
20. Run Apache httpd with `./r.sh ar`, try to open http://127.0.0.1 in a browser and make sure Apache gives you an "It works" message.
21. Kill Apache with `sudo pkill httpd`.
22. Build DWARF utils with `cd chopchop/dyngraph/generation/dwarf_utils; make`.
23. Kill the `./r.sh tr`, and restart `./r tr`.
24. Edit run.sh and resolve.sh in chopchop/dyngraph/results/1a/, change the paths (APACHE_BUILD_PATH, TRACER_PATH, etc.) according to your configurations.
25. Run an experiment (an attack or a normal task) with `cd chopchop/dyngraph/results/1a; ./run.sh`.
26. Kill the `./r.sh tr` to stop tracing.
27. Post-process the results of an experiment with cd chopchop/dyngraph/results/1a; ./resolve.sh.
28. See results in chopchop/dyngraph/results/1a/thread_separated, and make sure this folder is not empty.
29. `cd` into chopchop/dyngraph/generation, and run `./validate <trace_file>`. It is normal to see one or two lines of output. Please report if you see a large number of validator outputs.



Building
--
You must provide a default `TRACE_FILE` value,
```
$ TRACE_FILE=/path/to/trace/file ./build.sh "-DVERBOSE"
TRACE_FILE=/path/to/trace/file
```
This may be overridden when building/running other programs:
```
$ TRACE_FILE=local.program.trace make
```
(An example of this can be seen for Apache.)
I needed to use this feature the dbclust1 server, because of odd permission
configs: a root user (i.e., the user that loads ``httpd`) can't write to
other users' home directories. This made me set the default TRACE_FILE value
to a location where root can write to. But for some reason while building one of
the files was loaded, and the file in root's directory was attempted to be
opened (this time as me), leading to the same error we'd get below:
```
$ ./httpd
lt-httpd: tracer_hooks.c:37: trace_begin: Assertion `((void *)0) != trace_file' failed.
Aborted (core dumped)
```
Thus one can override the TRACE_FILE target at build/runtime of the program
being traced.

Enable function argument, return value and call stack tracing by #defining CALLSTACK:
```
$ TRACE_FILE=/path/to/trace/file ./build.sh -DCALLSTACK
```

Running
--
For Linux (e.g. Ubuntu 16.04)

Trace with trace_buff:
```
$ mkfifo /path/to/trace/file
$ mkfifo /path/to/buffered/file
$ ./trace_buff -i /path/to/trace/file -o /path/to/buffered/file -p
$ ./trace_reader -i /path/to/buffered/file -p
$ /program/to/be/traced
```

Trace without trace_buff:
```
$ mkfifo /path/to/trace/file
$ ./trace_reader -i /path/to/trace/file -p
$ /program/to/be/traced
```

Validate trace file:
```
./validate /path/to/trace/file
```
