; Copyright (c) 2019, University of Pennsylvania
; All rights reserved.
;
; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions are met:
;     * Redistributions of source code must retain the above copyright
;       notice, this list of conditions and the following disclaimer.
;     * Redistributions in binary form must reproduce the above copyright
;       notice, this list of conditions and the following disclaimer in the
;       documentation and/or other materials provided with the distribution.
;     * Neither the name of the <organization> nor the
;       names of its contributors may be used to endorse or promote products
;       derived from this software without specific prior written permission.
;
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
; ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
; DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
; DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
; ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	.file	"tracer_hooks.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"/home/jinzihao/apache_build_env/outputs/apache.local.trace"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"TRACE_FILE"
.LC2:
	.string	"tracer_hooks.c"
.LC3:
	.string	"0 == result"
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"S_ISFIFO(filename_stat.st_mode)"
	.section	.rodata.str1.1
.LC5:
	.string	"-1 != trace_fd"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB6:
	.section	.text.startup,"ax",@progbits
.LHOTB6:
	.p2align 4,,15
	.globl	trace_begin
	.type	trace_begin, @function
trace_begin:
.LFB79:
	.cfi_startproc
	pushq	%rbx
	.cfi_def_cfa_offset 16
	.cfi_offset 3, -16
	leaq	.LC1(%rip), %rdi
	leaq	.LC0(%rip), %rbx
	subq	$160, %rsp
	.cfi_def_cfa_offset 176
	movq	%fs:40, %rsi
	movq	%rsi, 152(%rsp)
	xorl	%esi, %esi
	call	getenv@PLT
	testq	%rax, %rax
	movq	%rsp, %rdx
	movl	$1, %edi
	cmovne	%rax, %rbx
	movq	%rbx, %rsi
	call	__xstat@PLT
	testl	%eax, %eax
	jne	.L9
	movl	24(%rsp), %edx
	andl	$61440, %edx
	cmpl	$4096, %edx
	jne	.L10
	movl	$1, %esi
	movq	%rbx, %rdi
	call	open@PLT
	cmpl	$-1, %eax
	movl	%eax, trace_fd(%rip)
	je	.L11
	movq	152(%rsp), %rsi
	xorq	%fs:40, %rsi
	jne	.L12
	addq	$160, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 16
	popq	%rbx
	.cfi_def_cfa_offset 8
	ret
.L9:
	.cfi_restore_state
	leaq	__PRETTY_FUNCTION__.4594(%rip), %rcx
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$41, %edx
	call	__assert_fail@PLT
.L12:
	call	__stack_chk_fail@PLT
.L11:
	leaq	__PRETTY_FUNCTION__.4594(%rip), %rcx
	leaq	.LC2(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$50, %edx
	call	__assert_fail@PLT
.L10:
	leaq	__PRETTY_FUNCTION__.4594(%rip), %rcx
	leaq	.LC2(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$42, %edx
	call	__assert_fail@PLT
	.cfi_endproc
.LFE79:
	.size	trace_begin, .-trace_begin
	.section	.text.unlikely
.LCOLDE6:
	.section	.text.startup
.LHOTE6:
	.section	.init_array,"aw"
	.align 8
	.quad	trace_begin
	.section	.text.unlikely
.LCOLDB7:
	.section	.text.exit,"ax",@progbits
.LHOTB7:
	.p2align 4,,15
	.globl	trace_end
	.type	trace_end, @function
trace_end:
.LFB80:
	.cfi_startproc
	rep ret
	.cfi_endproc
.LFE80:
	.size	trace_end, .-trace_end
	.section	.text.unlikely
.LCOLDE7:
	.section	.text.exit
.LHOTE7:
	.section	.fini_array,"aw"
	.align 8
	.quad	trace_end
	.section	.text.unlikely
.LCOLDB8:
	.text
.LHOTB8:
	.p2align 4,,15
	.globl	__cyg_profile_func_enter
	.type	__cyg_profile_func_enter, @function
__cyg_profile_func_enter:
.LFB81:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 3, -24
	movb	$62, -168(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -24(%rbp)
	movq	%rax, -248(%rbp)
	movq	%rdi, -240(%rbp)
	movq	%rsi, -232(%rbp)
	movq	%rdx, -224(%rbp)
	movq	%rcx, -216(%rbp)
	movq	%r8, -208(%rbp)
	movq	%r9, -200(%rbp)
	movq	$39, %rax
	syscall
	movl	%eax, -184(%rbp)
	xorl	%eax, %eax
	movq	$186, %rax
	syscall
	xorl	%esi, %esi
	leaq	-272(%rbp), %rbx
	movq	%rbx, %rdi
	movq	%rax, -176(%rbp)
	call	gettimeofday@PLT
	leaq	105(%rbx), %rdi
	movl	$128, %edx
	xorl	%esi, %esi
	call	memset@PLT
	leaq	-304(%rbp), %rdi
	movl	$3, %esi
	call	backtrace@PLT
	movq	-288(%rbp), %rdx
	movq	%rdx, -256(%rbp)
	movq	0(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L16
	movq	-112(%rdx), %rsi
	movq	%rsi, -167(%rbp)
	movq	-104(%rdx), %rsi
	movq	%rsi, -159(%rbp)
	movq	-96(%rdx), %rsi
	movq	%rsi, -151(%rbp)
	movq	-88(%rdx), %rsi
	movq	%rsi, -143(%rbp)
	movq	-80(%rdx), %rsi
	movq	%rsi, -135(%rbp)
	movq	-72(%rdx), %rsi
	movq	%rsi, -127(%rbp)
	movq	-64(%rdx), %rsi
	movq	%rsi, -119(%rbp)
	movq	-56(%rdx), %rsi
	movq	%rsi, -111(%rbp)
	movq	-48(%rdx), %rsi
	movq	%rsi, -103(%rbp)
	movq	-40(%rdx), %rsi
	movq	%rsi, -95(%rbp)
	movq	-32(%rdx), %rsi
	movq	%rsi, -87(%rbp)
	movq	-24(%rdx), %rsi
	movq	%rsi, -79(%rbp)
	movq	-16(%rdx), %rsi
	movq	%rsi, -71(%rbp)
	movq	-8(%rdx), %rsi
	movq	%rsi, -63(%rbp)
	movq	(%rdx), %rsi
	movq	%rsi, -55(%rbp)
	movq	8(%rdx), %rdx
	movq	%rdx, -47(%rbp)
.L16:
	movl	trace_fd(%rip), %edi
	movq	%rbx, %rsi
	movl	$240, %edx
	call	write@PLT
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L23
	addq	$296, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L23:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE81:
	.size	__cyg_profile_func_enter, .-__cyg_profile_func_enter
	.section	.text.unlikely
.LCOLDE8:
	.text
.LHOTE8:
	.section	.text.unlikely
.LCOLDB9:
	.text
.LHOTB9:
	.p2align 4,,15
	.globl	__cyg_profile_func_exit
	.type	__cyg_profile_func_exit, @function
__cyg_profile_func_exit:
.LFB82:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 3, -24
	movq	%rax, -192(%rbp)
	movq	%rsi, -256(%rbp)
	movq	%rdi, -248(%rbp)
	movq	%fs:40, %rbx
	movq	%rbx, -24(%rbp)
	movb	$60, -168(%rbp)
	movq	$39, %rax
	syscall
	movl	%eax, -184(%rbp)
	xorl	%eax, %eax
	movq	$186, %rax
	syscall
	xorl	%esi, %esi
	leaq	-272(%rbp), %rbx
	movq	%rbx, %rdi
	movq	%rax, -176(%rbp)
	call	gettimeofday@PLT
	leaq	105(%rbx), %rdi
	movl	$128, %edx
	xorl	%esi, %esi
	call	memset@PLT
	leaq	-304(%rbp), %rdi
	movl	$3, %esi
	call	backtrace@PLT
	movq	-288(%rbp), %rdx
	movq	%rdx, -256(%rbp)
	movq	0(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L26
	movq	-112(%rdx), %rsi
	movq	%rsi, -167(%rbp)
	movq	-104(%rdx), %rsi
	movq	%rsi, -159(%rbp)
	movq	-96(%rdx), %rsi
	movq	%rsi, -151(%rbp)
	movq	-88(%rdx), %rsi
	movq	%rsi, -143(%rbp)
	movq	-80(%rdx), %rsi
	movq	%rsi, -135(%rbp)
	movq	-72(%rdx), %rsi
	movq	%rsi, -127(%rbp)
	movq	-64(%rdx), %rsi
	movq	%rsi, -119(%rbp)
	movq	-56(%rdx), %rsi
	movq	%rsi, -111(%rbp)
	movq	-48(%rdx), %rsi
	movq	%rsi, -103(%rbp)
	movq	-40(%rdx), %rsi
	movq	%rsi, -95(%rbp)
	movq	-32(%rdx), %rsi
	movq	%rsi, -87(%rbp)
	movq	-24(%rdx), %rsi
	movq	%rsi, -79(%rbp)
	movq	-16(%rdx), %rsi
	movq	%rsi, -71(%rbp)
	movq	-8(%rdx), %rsi
	movq	%rsi, -63(%rbp)
	movq	(%rdx), %rsi
	movq	%rsi, -55(%rbp)
	movq	8(%rdx), %rdx
	movq	%rdx, -47(%rbp)
.L26:
	movl	trace_fd(%rip), %edi
	movl	$240, %edx
	movq	%rbx, %rsi
	call	write@PLT
	movq	-192(%rbp), %rax
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L33
	addq	$296, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L33:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE82:
	.size	__cyg_profile_func_exit, .-__cyg_profile_func_exit
	.section	.text.unlikely
.LCOLDE9:
	.text
.LHOTE9:
	.section	.rodata
	.align 8
	.type	__PRETTY_FUNCTION__.4594, @object
	.size	__PRETTY_FUNCTION__.4594, 12
__PRETTY_FUNCTION__.4594:
	.string	"trace_begin"
	.local	trace_fd
	.comm	trace_fd,4,4
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.4) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
