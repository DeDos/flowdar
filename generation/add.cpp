/*
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


This util contains a hardcoded patch for a pattern in typical executables,
to avoid rdi and rsi register being overwritten,
and to have callee passed via rax register for __cyg_profile_func_enter().
See comments below and patch.sh for more information.
Zihao Jin, UPenn, August 2017
*/

#include <iostream>
#include <fstream>
#include <cstring>
using namespace std;

int main(int argc, char **argv) {
    if (argc != 4) {
        cout << "Usage: " << argv[0] << " <input file> <output file> <script file>" << endl;
        return 0;
    }
    ifstream fin(argv[3]);

    char *buffer;
    long size;
    ifstream fin_binary(argv[1], ios::in|ios::binary|ios::ate);
    size = fin_binary.tellg();
    fin_binary.seekg(0, ios::beg);
    buffer = new char[size];
    fin_binary.read(buffer, size);
    fin_binary.close();

    cout << "input file size = " << size << endl;

    int offset;
    string line;

    while (getline(fin, line)) {
        /*
            494fa6:	48 8b 45 08          	mov    0x8(%rbp),%rax
            494faa:	48 89 c6             	mov    %rax,%rsi
            494fad:	bf 9a 4f 49 00       	mov    $0x494f9a,%edi
            494fb2:	e8 49 5f f9 ff       	callq  42af00 <__cyg_profile_func_enter>

            rewritten into:

            494fa6:	b8 9a 4f 49 00          mov    $0x494f9a,%eax
            494fab:	e8 50 5f f9 ff          callq  42af00 <__cyg_profile_func_enter>
            494fb0:	48 8b 45 08             mov    0x8(%rbp),%rax
            494fb4:	90 90 90               	nop; nop; nop;
         */
        sscanf(line.c_str(), "%x", &offset);
        *(unsigned int *)(buffer + offset + 1) += 7;
        __uint32_t inst1 = *(__uint32_t *)(buffer + offset - 12);
        __uint32_t inst3 = *(__uint32_t *)(buffer + offset - 4);
        *(unsigned char *)(buffer + offset - 12) = 0xb8;
        *(unsigned int *)(buffer + offset - 11) = inst3;
        memmove(buffer + offset - 7, buffer + offset, 5);
        *(unsigned int *)(buffer + offset - 2) = inst1;
        memset(buffer + offset + 2, 0x90, 3);
        printf("patched at 0x%x: ", offset);
        for (int i = 0; i < 17; ++i) {
            printf("%02x ", *(unsigned char *)(buffer + offset - 12 + i));
        }
        printf("\n");
    }

    fin.close();

    ofstream fout(argv[2]);
    fout.write(buffer, size);
    fout.close();
    return 0;
}
