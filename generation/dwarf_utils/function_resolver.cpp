/*
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <cstring>
using namespace std;

int main(int argc, char **argv) {
    if (argc < 3 || argc % 2 == 0) {
        // Function maps and offsets must be placed in an ascending order

        // Correct example (0 < 7f3aca0a8000 < 7f3aca508000):
        // ./function_resolver httpd_function_map.txt 0 aprutil_function_map.txt 7f3aca0a8000 apr_function_map.txt 7f3aca508000
        // Wrong example (7f3aca0a8000 < 7f3aca508000, but 7f3aca508000 > 0):
        // ./function_resolver aprutil_function_map.txt 7f3aca0a8000 apr_function_map.txt 7f3aca508000 httpd_function_map.txt 0

        // So why not sort the data here?
        // Because most of the time input data are sorted by nature, so it's better not to waste time re-sorting that.
        cout << "Usage: " << argv[0] << " <function map 1> <offset 1> ... <function map n> <offset n>" << endl;
        return 0;
    }

    vector<string> function_name;
    vector<unsigned long> function_address;

    for (int i = 1; i < argc; i += 2) {
        ifstream fin_fmap(argv[i]);
        string line;
        unsigned long offset = stoul(argv[i + 1], 0, 16);

        while (getline(fin_fmap, line)) {
            unsigned long address;
            string name;
            istringstream sin(line);
            sin >> hex >> address >> name;
            function_name.push_back(name);
            function_address.push_back(address + offset);
        }
        fin_fmap.close();
    }

    unsigned long address;
    while (cin >> hex >> address) {
        vector<unsigned long>::iterator inferedAddress = upper_bound(function_address.begin(), function_address.end(), address) - 1;
        cout << function_name[inferedAddress - function_address.begin()] << " " << hex << address - *inferedAddress << endl;
    }

    return 0;
}
