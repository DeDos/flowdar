#!/bin/bash
#
#Copyright (c) 2019, University of Pennsylvania
#All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the <organization> nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
#DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

objdump --dwarf=info $1 | awk '
BEGIN {
    MAX_STATE_ID = 9;
}

{
    num_of_fields = split($1, fields, ">");
    if (num_of_fields == 3) {
        if ($NF == "(DW_TAG_subprogram)") { // Functions
            state[0] = 1;
        }
        else if ($NF == "(DW_TAG_formal_parameter)") { // Function parameter
            state[1] = 1;
        }
        else if ($NF == "(DW_TAG_variable)") { // Local variable
            state[2] = 1;
        }
        else if ($NF == "(DW_TAG_base_type)") { // Basic data type
            datatype_addr = substr(fields[2], 2, length(fields[2]) - 1);
            datatype_type[datatype_addr] = 0;
            state[3] = 1;
        }
        else if ($NF == "(DW_TAG_pointer_type)") { // Pointer type
            datatype_addr = substr(fields[2], 2, length(fields[2]) - 1);
            datatype_type[datatype_addr] = 1;
            state[4] = 1;
        }
        else if ($NF == "(DW_TAG_structure_type)") { // struct type
            datatype_addr = substr(fields[2], 2, length(fields[2]) - 1);
            datatype_type[datatype_addr] = 2;
            state[5] = 1;
        }
        else if ($NF == "(DW_TAG_union_type)") { // union type
            datatype_addr = substr(fields[2], 2, length(fields[2]) - 1);
            datatype_type[datatype_addr] = 3;
            state[6] = 1;
        }
        else if ($NF == "(DW_TAG_typedef)") { // typedef type
            datatype_addr = substr(fields[2], 2, length(fields[2]) - 1);
            datatype_type[datatype_addr] = 4;
            state[7] = 1;
        }
        else if ($NF == "(DW_TAG_member)") { // member type
            datatype_addr = substr(fields[2], 2, length(fields[2]) - 1);
            datatype_type[datatype_addr] = 5;
            state[8] = 1;
        }
        else if ($NF == "(DW_TAG_enumeration_type)") { // enum type
            datatype_addr = substr(fields[2], 2, length(fields[2]) - 1);
            datatype_type[datatype_addr] = 6;
            state[9] = 1;
        }
    }
    else if ($2 == "DW_AT_declaration") {
        for (i = 3; i <= 8; ++i) {
            if (state[i] > 0) {
                delete datatype_type[datatype_addr];
            }
        }
        for (i = 0; i <= MAX_STATE_ID; ++i) {
            state[i] = 0;
        }
    }
    else if ($2 == "DW_AT_name") {
        if (state[0] == 1) {
            state[0] += 1;
            function_name = $NF;
#            print("F "$NF);
        }
        if (state[1] == 1) {
            state[1] += 1;
            parameter_name = $NF;
        }
        if (state[2] == 1) {
            state[2] += 1;
            variable_name = $NF;
        }
    }
    else if ($2 == "DW_AT_byte_size") {
        if (state[3] == 1) {
            datatype_size[datatype_addr] = $4;
            state[3] = 0;
        }
        if (state[4] == 1) {
            datatype_size[datatype_addr] = $4;
            state[4] += 1;
        }
        if (state[5] == 1) {
            datatype_size[datatype_addr] = $4;
            state[5] = 0;
        }
        if (state[6] == 1) {
            datatype_size[datatype_addr] = $4;
            state[6] = 0;
        }
        if (state[9] == 1) {
            datatype_size[datatype_addr] = $4;
            state[9] = 0;
        }
    }
    else if ($2 == "DW_AT_type") {
        if (state[1] == 2) {
            state[1] += 1;
#            parameter_size = datatype_size[substr($4, 4, length($4) - 4)];
            parameter_type = substr($4, 4, length($4) - 4);
        }
        if (state[2] == 2) {
            state[2] += 1;
#            variable_size = datatype_size[substr($4, 4, length($4) - 4)];
            variable_type = substr($4, 4, length($4) - 4);
        }
        if (state[4] == 2) {
            pointer_type[datatype_addr] = substr($4, 4, length($4) - 4);
            state[4] = 0;
        }
        if (state[7] == 1) {
            typedef_type[datatype_addr] = substr($4, 4, length($4) - 4);
            state[7] = 0;
        }
        if (state[8] == 1) {
            member_type[datatype_addr] = substr($4, 4, length($4) - 4);
            state[8] = 0;
        }
    }
    else if ($2 == "DW_AT_location") {
        if ($(NF - 1) == "(DW_OP_fbreg:") {
            if (state[1] == 3) {
                state[1] = 0;
#                print("P "function_name" "parameter_name" "( - substr($NF, 1, length($NF) - 1))" "parameter_type);
                variables["P "function_name" "parameter_name" "( - substr($NF, 1, length($NF) - 1))] = parameter_type;
            }
            if (state[2] == 3) {
                state[2] = 0;
#                print("V "function_name" "variable_name" "( - substr($NF, 1, length($NF) - 1))" "variable_type);
                variables["V "function_name" "variable_name" "( - substr($NF, 1, length($NF) - 1))] = variable_type;
            }
        }
#        else {
#            print("O -");
#        }
    }
}

END {
    for (elem in datatype_type) {
        if (datatype_type[elem] == 0 ||
            datatype_type[elem] == 1 ||
            datatype_type[elem] == 2 ||
            datatype_type[elem] == 3) {
#            Basic, pointer union or struct, we can directly determine its size.
#            print(elem" "datatype_size[elem]);
            calculated_size[elem] = datatype_size[elem];
        }
        else if (datatype_type[elem] == 4) {
#            Typedef, we need to recursively follow the typedefs to reach a basic, pointer, union or a struct
            original_elem = elem;
            type = datatype_type[elem];
            while (type == 4) {
                elem = typedef_type[elem];
                type = datatype_type[elem];
            }
            if (elem in datatype_size) {
#                print(original_elem" "datatype_size[elem]);
                calculated_size[original_elem] = datatype_size[elem];
            }
        }
        else if (datatype_type[elem] == 5) {
#            Member, we could determine the size and offset.
#            It is only a stub here.
        }
    }

    for (elem in variables) {
        print(elem" "calculated_size[variables[elem]]);
    }
}
'
