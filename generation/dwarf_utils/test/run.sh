#!/bin/bash
#
#Copyright (c) 2019, University of Pennsylvania
#All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the <organization> nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
#DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

echo "<<<< 1. test_prog >>>>"
../../trace_reader -i ~/apache_build_env/outputs/apache.local.trace > trace.txt &
./test_prog
pkill trace_reader
../function_map.sh test_prog > test_prog_function_map.txt
../stack_map.sh test_prog > test_prog_stack_map.txt
echo "#!/bin/bash" > filter.sh
chmod +x filter.sh
cat test_prog_stack_map.txt | awk '{print($1" "$2" "$3)}' | ../variable_locator test_prog_function_map.txt test_prog_stack_map.txt >> filter.sh
./filter.sh | awk '{print($NF);}' > filtered.txt
diff filtered.txt correct_test_prog.txt
if [ $? -eq 0 ] ; then
    echo "<<<< test passed >>>>"
else
    echo "<<<< test failed >>>>"
fi
echo ""
echo "<<<< 2. apache (this takes 30 ~ 60 secs)>>>>"
echo "#!/bin/bash" > filter.sh
cat httpd_variables.txt | ../variable_locator httpd_function_map.txt httpd_stack_map.txt >> filter.sh
rm trace.txt
ln -s httpd_trace.txt trace.txt
./filter.sh | awk '{print($NF);}' > filtered.txt
rm trace.txt
diff filtered.txt correct_httpd.txt
if [ $? -eq 0 ] ; then
    echo "<<<< test passed >>>>"
else
    echo "<<<< test failed >>>>"
fi
