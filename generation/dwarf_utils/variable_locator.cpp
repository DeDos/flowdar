/*
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <cstring>
using namespace std;

static const unsigned int CALLSTACK_SIZE = 128;

struct stack_var {
    char type; // P for parameter, V for local variable
    string function;
    string name;
    int offset;
    int size;
};

int main(int argc, char **argv) {
    if (argc < 3) {
        cout << "Usage: " << argv[0] << " <function map> <stack map> [--dump] [--offset=addr]" << endl;
        return 0;
    }

    string fmap_filename;
    string smap_filename;
    int arg_count = 0;
    bool is_dump = false;
    unsigned long offset = 0;

    for (int i = 0; i < argc; ++i) {
        // We don't really need to worry about buffer overflow here -
        // at least we have 2 characters in an argv element.
        if (argv[i][0] == '-' && argv[i][1] == '-') {
            if (!strcmp(argv[i] + 2, "dump")) {
                is_dump = true;
            }
            else {
                char buffer[8];
                buffer[7] = 0;
                memcpy(buffer, argv[i] + 2, 7);
                if (!strcmp(buffer, "offset=")) {
                    offset = stoul(argv[i] + 9, 0, 16);
                }
            }
        }
        else {
            if (arg_count == 1) {
                fmap_filename = argv[i];
            }
            else if (arg_count == 2) {
                smap_filename = argv[i];
            }
            ++arg_count;
        }
    }

    ifstream fin_fmap(fmap_filename);
    string line;
    map<string, unsigned long> function_map;
    while (getline(fin_fmap, line)) {
        unsigned long address;
        string function_name;
        istringstream sin(line);
        sin >> hex >> address >> function_name;
        function_map[function_name] = address;
    }
    fin_fmap.close();

    ifstream fin_smap(smap_filename);
    vector<stack_var> stack_map;
    while (getline(fin_smap, line)) {
        stack_var variable;
        istringstream sin(line);
        sin >> variable.type >> variable.function >> variable.name >> variable.offset >> variable.size;
        stack_map.push_back(variable);
    }
    fin_smap.close();

    if (is_dump) {
        for (vector<stack_var>::iterator it = stack_map.begin(); it != stack_map.end(); ++it) {
            cout << it->type << " " << it->function << " " << hex << function_map[it->function] + offset
                 << dec << " " << it->name << " " << it->offset << " " << it->size << endl;
        }
    }
    else {
        stack_var variable;
        while (cin >> variable.type >> variable.function >> variable.name) {
            // Current we use a linear search here, which has an O(n) time complexity.
            // We could make it O(log(n)) with a map<T, set<vector<stack_var>::iterator> > for each data field of type T.
            // However, given a relatively small (5939 for httpd) size of data, that might do harm on the performance.
            for (vector<stack_var>::iterator it = stack_map.begin(); it != stack_map.end(); ++it) {
                if ((variable.type == '*' || it->type == variable.type) &&
                    (!variable.function.compare("*") || !(it->function.compare(variable.function))) &&
                    (!variable.name.compare("*") || !(it->name.compare(variable.name)))) {
                    cout << "awk '{if ($1 == \"<\" && $3 == \"0x"
                         << hex << function_map[it->function] + offset << "\") {print \"" << it->type << " " << it->function << " " << it->name << " \"$4\" \"$7\" \"$5\" \"$6\" \"substr($9, "
                         << dec << ((CALLSTACK_SIZE - it->offset) * 2 + 1) << ", "
                         << it->size * 2 << ")}}' trace.txt" << endl;
                }
            }
        }
    }

    return 0;
}
