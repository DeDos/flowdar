/*
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <cstring>
using namespace std;

int main(int argc, char **argv) {
    if (argc != 2) {
        cout << "Usage: " << argv[0] << " <resolved function list>" << endl;
        return 0;
    }

    ifstream fin(argv[1]);
    string line;
    int count = 0;
    vector<pair<string, string> > function_names;
    pair<string, string> current_record;
    while (getline(fin, line)) {
        if (++count % 2) {
            current_record.first = line.substr(0, line.find_first_of(" @")) + ":" + line.substr(line.find_last_of(" ") + 1);
        }
        else {
            current_record.second = line.substr(0, line.find_first_of(" @")) + ":" + line.substr(line.find_last_of(" ") + 1);
            function_names.push_back(current_record);
        }
    }

    count = 0;
    while (getline(cin, line)) {
        int third_space_in_trace_line = line.find_first_of(" ", line.find_first_of(" ", 2) + 1);
        cout << line.substr(0, 1) << " " << function_names[count].first << " " << function_names[count].second << line.substr(third_space_in_trace_line) << endl;
        ++count;
    }

    return 0;
}
