/*
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>
using namespace std;

int main(int argc, char **argv) {
    if (argc < 2) {
        cout << "Usage: " << argv[0] << " <code map> [offset]" << endl;
        return 0;
    }
    uint64_t y;
    uint64_t x;

    ifstream fin(argv[1]);
    string line;
    int separatorPostions[2];
    map<unsigned long, pair<string, int> > lineNumber;
    vector<unsigned long> validAddress;
    while (getline(fin, line)) {
        int count = 0;
        for (string::iterator it = line.begin(); it != line.end(); ++it) {
            if (*it == '*') {
                if (count > 2) {
                    cout << "Warning: line {" << line << "} has more than 3 fields." << endl;
                }
                separatorPostions[count++] = it - line.begin();
            }
        }
        validAddress.push_back(stol(line.substr(separatorPostions[1] + 3), 0, 16));
        lineNumber[validAddress.back()] =
                pair<string, int>(line.substr(0, separatorPostions[0]),
                                  stoi(line.substr(separatorPostions[0] + 1, separatorPostions[1] - separatorPostions[0] - 1)));
    }
    unsigned long offset = 0;
    if (argc > 2) {
        offset = stol(argv[2], 0, 16);
    }
    unsigned long address;
    while (cin >> hex >> address) {
        vector<unsigned long>::iterator inferedAddress = upper_bound(validAddress.begin(), validAddress.end(), address - offset) - 1;
        cout << lineNumber[*inferedAddress].first << " " << lineNumber[*inferedAddress].second << endl;
    }
    fin.close();
    return 0;
}
