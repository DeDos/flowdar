/*
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


A validator for trace files, checking for call-return mismatches.
See /dyngraph/documentation/NOTES_ON_VALIDATOR.md for more information.
Zihao Jin, UPenn, August 2017
*/

#include <iostream>
#include <fstream>
#include <map>
#include <stack>
#include <vector>

using namespace std;

pair<string, unsigned long> parse_function_string(const string& function_string) {
    int colon_position = function_string.find_first_of(":");
    pair<string, unsigned long> result;
    result.first = function_string.substr(0, colon_position);
    result.second = stoul(function_string.substr(colon_position + 1), 0, 16);
    return result;
};

int main(int argc, char **argv) {
    if (argc != 2) {
        cout << "Usage: " << argv[0] << " <trace file>" << endl;
        return 0;
    }

    ifstream fin(argv[1]);
    string line;
    vector<pair<string, string> > callstack;
    vector<int> call_id;
    vector<int> return_id;
    vector<pair<string, string> > return_without_calls;


    int count = 0;
    while (getline(fin, line)) {
        // The vector is used to store the position of spaces (' ') in a line.
        // As STL doesn't come with a split() function for strings,
        // I need to implement a simple version here.
        vector<int> spacePosition;

        for (int i = 0; i < line.size(); ++i) {
            if (line[i] == ' ') {
                spacePosition.push_back(i);
            }
        }

        pair<string, string> call(line.substr(spacePosition[0] + 1, spacePosition[1] - spacePosition[0]),
                                  line.substr(spacePosition[1] + 1, spacePosition[2] - spacePosition[1]));

        if (line[0] == '>') {
            callstack.push_back(call);
            call_id.push_back(count);
        }
        else if (line[0] == '<') {
            if (callstack.size() && !callstack.back().first.compare(call.first)
                && !callstack.back().second.compare(call.second)) {
                callstack.pop_back();
                call_id.pop_back();
            }
            else {
                return_without_calls.push_back(call);
                return_id.push_back(count);
            }
        }
        else {
            cout << "Warning: unrecognized trace {" << line << "}" << endl;
        }
        ++count;
    }

    bool flag = false;
    for (int i = 1; i < return_id.size(); ++i) {
        pair<string, unsigned long> func1 = parse_function_string(return_without_calls[i].second);
        pair<string, unsigned long> func2 = parse_function_string(return_without_calls[i - 1].first);

        if (return_id[i] <= return_id[i - 1] || func1.first.compare(func2.first)) {
            flag = true;
        }
    }

    if (call_id.size() && return_id.size()) {
        pair<string, unsigned long> func1 = parse_function_string(callstack.front().first);
        pair<string, unsigned long> func2 = parse_function_string(return_without_calls.back().first);

        if (call_id.front() <= return_id.back() || func1.first.compare(func2.first)) {
            flag = true;
        }
    }

    for (int i = 1; i < call_id.size(); ++i) {
        pair<string, unsigned long> func1 = parse_function_string(callstack[i].first);
        pair<string, unsigned long> func2 = parse_function_string(callstack[i - 1].second);

        if (call_id[i] <= call_id[i - 1] || func1.first.compare(func2.first)) {
            flag = true;
        }
    }

    if (flag) {
        for (int i = 0; i < return_id.size(); ++i) {
            cout << "< " << return_id[i] << " " << return_without_calls[i].first << " " << return_without_calls[i].second << endl;
        }
        for (int i = 0; i < call_id.size(); ++i) {
            cout << "> " << call_id[i] << " " << callstack[i].first << " " << callstack[i].second << endl;
        }
    }

    return 0;
}
