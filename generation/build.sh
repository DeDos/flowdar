#!/bin/bash
#
#Copyright (c) 2019, University of Pennsylvania
#All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the <organization> nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
#DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#
# Tracing hooks, to be linked with program to be traced.
# Nik Sultana, UPenn, May 2017

# TRACE_FILE should be a fifo file (Create with mkfifo command).
if [ -z "${TRACE_FILE}" ]
then
  echo "Must define TRACE_FILE"
  exit 1
else
  echo "TRACE_FILE=${TRACE_FILE}"
fi

if [ -z "$1" ]
then
  EXTRA_FLAGS=
else
# To add more than one EXTRA_FLAGS, add it here as below,
# otherwise this script would only recognize the first flag.

#  EXTRA_FLAGS="-DBACKTRACE $1"
  EXTRA_FLAGS="$1"
fi

if [ -z "${CC}" ]
then
  CC=gcc
fi

if [ -z "${CXX}" ]
then
  CXX=g++
fi

if [ -z "${AS}" ]
then
  AS=as
fi

echo "Building utils"
${CXX} add.cpp -o add
${CXX} mov.cpp -o mov
${CXX} xchg.cpp -o xchg

echo "Building tracer"
# Here we use assembly code to avoid gcc's optimization that may break the following patch.

if [[ $1 == *'-DCALLSTACK'* ]]; then
  ${AS} tracer_hooks.s -o tracer_hooks.o
else
  ${CC} -O3 -c -fPIC -o tracer_hooks.o -DTRACE_FILE=${TRACE_FILE} ${EXTRA_FLAGS} tracer_hooks.c
fi

# Uncomment the following section to compile tracer_hooks.o from tracer_hooks.c, and patch it with mov.cfg.

## Using -ffixed-rax and -ffixed-rcx option here is to avoid rcx (4th argument) and rax (return value) register being overwritten before we dump them.
## The -ffixed-rax and -ffixed-rcx option should be modified if the machine code gcc generates for the tracer changed.
# ${CC} -O3 -c -ffixed-rax -ffixed-rcx -fPIC -o tracer_hooks.orig.o -DTRACE_FILE=${TRACE_FILE} ${EXTRA_FLAGS} tracer_hooks.c

# echo "Patching tracer"
## How to patch tracer_hooks.o depends on the machine code gcc generated.
## The machine code gcc generates might change across different builds.
## Check with `objdump -d tracer_hooks.o` to see if mov.cfg matches current machine code gcc generated.

## The mov.cfg provided here works with gcc 5.4.0, x86_64.
## The purpose of the patch here is to avoid rdx register being overwritten,
## by dumping the value of rdx register as early as possible,
## which allows gcc to safely use rdx as an ordinary register afterwards.

## See the comments in mov.cpp about the mov util.

# ./mov tracer_hooks.orig.o tracer_hooks.o mov.cfg
# rm tracer_hooks.orig.o

echo "Building trace reader"
${CC} -O3 -DTRACE_FILE=${TRACE_FILE} ${EXTRA_FLAGS} trace_reader.c -o trace_reader

echo "Building tracer test"
# NOTE using "--static" and "-O0" can yield a better binary for analysis.
${CC} -g -fno-function-cse -finstrument-functions tracer_test.c tracer_hooks.o -o tracer_test

if [[ $1 == *'-DCALLSTACK'* ]]; then
  echo "Patching tracer test"
  # See comments in patch.sh for more information.
  ./patch.sh tracer_test
fi

echo "Building trace buff"
${CC} -O3 -DTRACE_FILE=${TRACE_FILE} ${EXTRA_FLAGS} trace_buff.c -o trace_buff

echo "Building validator"
${CXX} -std=c++11 validate.cpp -o validate
