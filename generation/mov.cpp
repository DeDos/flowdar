/*
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


Unlike add, xchg & libpatch, this util is *not* a hardcoded patch for a specific code pattern,
instead, it does byte-for-byte patch on <input file> with the information from <script file>,
and produces <output file>.

Example <script file>:
43 d2
52 20
7f fa
83 10
The first column is the offset in binary file, the second column is the new value for that byte.

Zihao Jin, UPenn, August 2017
*/

#include <iostream>
#include <fstream>
#include <cstring>
using namespace std;

int main(int argc, char **argv) {
    if (argc != 4) {
        cout << "Usage: " << argv[0] << " <input file> <output file> <script file>" << endl;
        return 0;
    }
    ifstream fin(argv[3]);

    char *buffer;
    long size;
    ifstream fin_binary(argv[1], ios::in|ios::binary|ios::ate);
    size = fin_binary.tellg();
    fin_binary.seekg(0, ios::beg);
    buffer = new char[size];
    fin_binary.read(buffer, size);
    fin_binary.close();

    cout << "input file size = " << size << endl;

    int offset;
    int data;
    string line;

    while (getline(fin, line)) {
        sscanf(line.c_str(), "%x %x", &offset, &data);
        *(unsigned char *)(buffer + offset) = data;
        printf("patched at 0x%x\n", offset);
    }

    fin.close();

    ofstream fout(argv[2]);
    fout.write(buffer, size);
    fout.close();
    return 0;
}
