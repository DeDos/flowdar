#!/bin/bash
#
#Copyright (c) 2019, University of Pennsylvania
#All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the <organization> nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
#DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

CXX=g++
export APACHE_BUILD_PATH="/usr/local/apache2/"
export SRC_PATH="/home/pardis/Desktop/apache_build_env/"
export APR_BUILD_PATH="/usr/local/apr/"
export TRACER_PATH="/home/pardis/Desktop/chopchop/dyngraph/generation/"
export OUTPUT_PATH="/home/pardis/Desktop/apache_build_env/outputs/"
export TRACE_FILENAME="apache.local.trace"
export TRACE_FILE=$OUTPUT_PATH$TRACE_FILENAME

set +x

# "tracer build"
if [ $1 == 'tb' ]; then
	cd $TRACER_PATH
	./build.sh $2
# "trace read"
elif [ $1 == 'tr' ]; then
	cd $TRACER_PATH
#	sudo pkill trace_buff
# With buffer
#	./trace_buff -i $OUTPUT_PATH$TRACE_FILENAME -o $OUTPUT_PATH$TRACE_FILENAME.buffered -p -b 32768 &
#	./trace_reader -i $OUTPUT_PATH$TRACE_FILENAME.buffered -p
# Without buffer
#	./trace_reader -i $OUTPUT_PATH$TRACE_FILENAME -p
# Output to file instead of stdout
#	rm -f $OUTPUT_PATH"trace.txt"
#	./trace_reader -i $OUTPUT_PATH$TRACE_FILENAME -p > $OUTPUT_PATH"trace.txt"
# Just store the trace from the fifo to a normal file, doesn't do the conversion
	rm -f $OUTPUT_PATH"trace.bin"
	./trace_reader -i $OUTPUT_PATH$TRACE_FILENAME -o $OUTPUT_PATH"trace.bin" -p
# "trace convert"
elif [ $1 == 'tc' ]; then
	cd $TRACER_PATH
	rm -f $OUTPUT_PATH"trace.txt"
	./trace_reader -i $OUTPUT_PATH"trace.bin" -o $OUTPUT_PATH"trace.txt" -c
# "tracer test"
elif [ $1 == 'tt' ]; then
	cd $TRACER_PATH
	./tracer_test
# "apache build"
elif [ $1 == 'ab' ]; then
	cd $SRC_PATH"httpd-2.4.26"
	make clean
	./configure --enable-ssl --with-ssl=$SRC_PATH/openssl-1.02l --with-mpm=worker --prefix=$APACHE_BUILD_PATH CFLAGS='-g -finstrument-functions -I $TRACER_PATH -DOPENSSL_NO_SSL2 -fno-inline -fno-optimize-sibling-calls -fno-omit-frame-pointer' LDFLAGS="$TRACER_PATH/tracer_hooks.o" --with-apr=$SRC_PATH/apr-1.6.2/ --with-apr-util=$SRC_PATH/apr-util-1.6.0/ --with-pcre=$SRC_PATH/pcre-8.40/pcre-config --prefix=$APACHE_BUILD_PATH && make && make install
	cd $TRACER_PATH
	./patch.sh $APACHE_BUILD_PATH"/bin/httpd"
	for file in `ls $APACHE_BUILD_PATH"/modules/"*.so`
	do
		./patch_lib.sh $file
	done
# "apache run"
elif [ $1 == 'ar' ]; then
	cd $APACHE_BUILD_PATH"bin"
	sudo pkill httpd
	sudo ./apachectl start
	cd $HOMEDIR
# "apr build"
elif [ $1 == 'aprb' ]; then
	cd $SRC_PATH"apr-1.6.2"
	CFLAGS=' -g -finstrument-functions -I '$TRACER_PATH' -fno-inline -fno-optimize-sibling-calls -fno-omit-frame-pointer -DCALLSTACK ' LDFLAGS=' '$TRACER_PATH'tracer_hooks.o ' ./configure --prefix=$APR_BUILD_PATH
	make clean
	make -j 8
	sudo make install -j 8
	cd $SRC_PATH"apr-util-1.6.0"
	CFLAGS=' -g -finstrument-functions -I '$TRACER_PATH' -fno-inline -fno-optimize-sibling-calls -fno-omit-frame-pointer -DCALLSTACK ' LDFLAGS=' '$TRACER_PATH'tracer_hooks.o ' ./configure --with-apr=$SRC_PATH"apr-1.6.2" --prefix=$APR_BUILD_PATH
	make clean
	make -j 8
	sudo make install -j 8
	cd $TRACER_PATH
	${CXX} xchg.cpp -o xchg
	${CXX} libpatch.cpp -o libpatch
	sudo ./patch_lib.sh $APR_BUILD_PATH"lib/libapr-1.so.0.6.2"
	sudo ./patch_lib.sh $APR_BUILD_PATH"lib/libaprutil-1.so.0.6.0"
# "benchmark"
elif [ $1 == 'b' ]; then
	# Use non-traced ab!
	ab -n 100 -c 100 -B 127.0.0.1 http://127.0.0.1/
fi

