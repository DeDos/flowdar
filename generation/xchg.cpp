/*
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


This util contains a hardcoded patch for __cyg_profile_func_exit(),
to make sure __cyg_profile_func_exit() is called after return value (rax register) is determined.
See comments below and patch_exit.sh for more information.
Zihao Jin, UPenn, August 2017
*/

#include <iostream>
#include <fstream>
#include <cstring>
using namespace std;

static const unsigned int CALLQ_INSTRUCTION_SIZE = 5;

int main(int argc, char **argv) {
    if (argc != 4) {
        cout << "Usage: " << argv[0] << " <input file> <output file> <script file>" << endl;
        return 0;
    }
    ifstream fin(argv[3]);

    char *buffer;
    long size;
    ifstream fin_binary(argv[1], ios::in|ios::binary|ios::ate);
    size = fin_binary.tellg();
    fin_binary.seekg(0, ios::beg);
    buffer = new char[size];
    fin_binary.read(buffer, size);
    fin_binary.close();

    cout << "input file size = " << size << endl;

    int offset;
    int next_instruction_size;
    string line;

    while (getline(fin, line)) {
        sscanf(line.c_str(), "%x %x", &offset, &next_instruction_size);
        if (next_instruction_size != 0) {
            // In x86-64 assembly, the callq instruction takes a relative address as the target rather than an absolute one,
            // and it is relative to the address of the instruction *immediately after* callq (not callq itself).
            // So if we exchange the callq instruction with the instruction immediately after that,
            // we must update the relative address by subtracting the size of the following instruction.
            *(unsigned int *)(buffer + offset + 1) -= next_instruction_size;
            char next_instruction_buffer[next_instruction_size];

            /*
                47862a:	e8 71 2a fb ff       	callq  42b0a0 <__cyg_profile_func_exit>
                47862f:	89 d8                	mov    %ebx,%eax

                rewritten into:

                47862a:	89 d8       	        mov    %ebx,%eax
                47862c:	e8 6f 2a fb ff          callq  42b0a0 <__cyg_profile_func_exit>
             */
            memcpy(next_instruction_buffer, buffer + offset + CALLQ_INSTRUCTION_SIZE, next_instruction_size);
            memmove(buffer + offset + next_instruction_size, buffer + offset, CALLQ_INSTRUCTION_SIZE);
            memcpy(buffer + offset, next_instruction_buffer, next_instruction_size);
            printf("patched at 0x%x: ", offset);
            for (int i = 0; i < CALLQ_INSTRUCTION_SIZE + next_instruction_size; ++i) {
                printf("%02x ", *(unsigned char *)(buffer + offset + i));
            }
            printf("\n");
        }
    }

    fin.close();

    ofstream fout(argv[2]);
    fout.write(buffer, size);
    fout.close();
    return 0;
}
