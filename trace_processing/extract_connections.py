#!/usr/bin/env python
#
#Copyright (c) 2019, University of Pennsylvania
#All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the <organization> nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
#DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#
# Extract connections from a monolithic Nginx trace file
# Pardis Pashakhanloo, UPenn, January 2018

import os
import argparse
from collections import OrderedDict

parser = argparse.ArgumentParser(description='Extract connections')

parser.add_argument('-i',
                    help='Input trace file name', required=True)
parser.add_argument('-o',
                    help='Directory to put output files', required=True)
parser.add_argument('--print_log',
                    help='Print runtime information', action='store_true', required=False)

args = parser.parse_args()

RET_VAL = 7
ARG1 = 7
DIR, CALLER, CALLEE = 0, 1, 2

print_log = args.print_log
input_filename = args.i
output_directory = args.o

output_prefix = output_directory + '/' + input_filename + '_'

if not os.path.exists(output_directory):
    os.makedirs(output_directory)

f_input = open(input_filename, 'r')

conn_ids = []
req_ids = []

curr_conn = 0

line_numbers = 0

# First pass: Find connection identifiers and their corresponding request number
# In this pass, using the return value of ngx_get_connection(), the connection id
# is obtained. Next, using the return value of ngx_http_create_request(), the
# corresponding request id is obtained.
if print_log:
    print('Find connection identifiers and their corresponding request number')
i = 0
while 1:
    line = f_input.readline()

    if not line:
        break
    line_numbers = line_numbers + 1

    # record connection id's
    if line.startswith('< ngx_event_accept ngx_get_connection'):
        message = line.split(" ")
        conn_ids.append(message[RET_VAL])

    # record request id's
    # ngx_http_create_request() glues a request id to the corresponding connection id
    elif line.startswith('> ngx_http_wait_request_handler ngx_http_create_request'):
        message = line.split(" ")
        curr_conn = message[ARG1]
    elif line.startswith('< ngx_http_wait_request_handler ngx_http_create_request'):
        message = line.split(" ")
        req_ids.insert(conn_ids.index(curr_conn), message[RET_VAL])

ids_lst = []
for i in range(0, len(conn_ids)):
    ids_lst.append((conn_ids[i], req_ids[i]))

ids = OrderedDict(ids_lst)

if print_log:
    print('End of the first pass.')


if print_log:
    print('Second pass: Extract connections wrt connection ID')
# Second pass: Extract connections
# File is scanned again from the beginning, and in each line (i.e. message), we
# look for a request id or a connection id in a call message (i.e. starting with >).
# Then, the whole block (i.e. until reaching the corresponding return message)
# is copied to a separate file. This is done for every pair of (connection_id, request_id).
f_input.seek(0)
finished = [False] * (line_numbers+1)
f_outputs = dict()  # {conn_id: file_handler}

i = 0
line_counter = 0
while 1:
    line = f_input.readline()
    line_counter = line_counter + 1

    if not line:
        break

    # init connection --> create a separate file
    # the beginning of the first block is hard-coded.
    if line.startswith('> ngx_epoll_process_events ngx_event_accept'):
        # files are named after the connection ID
        f_output = open(output_prefix + str(ids_lst[i][0]), 'w+')
        if print_log:
            print('FROM CONNECTION ID...')
        while not line.startswith('< ngx_epoll_process_events ngx_event_accept'):
            f_output.write(line)
            finished[line_counter] = True
            line = f_input.readline()
            line_counter = line_counter + 1
            if not line:
                break
        f_output.write(line)
        finished[line_counter] = True
        i = i + 1
         # After writing each block to its corresponding file, the file is closed
         # because, if we keep all files open, since the number of connections can
         # be very large, we may encounter "too many open files" error message from the operating system
         # because the number of open files in a system is limited and varies across operation systems.
        f_output.close()
        

    # other connection-related blocks
    else:
        found_conn_id = 0
        found_req_id = 0
        message = line.split(" ")
        for conn_id in conn_ids:
            if (conn_id in message) and (not finished[line_counter]) and (message[DIR] == '>'):
                found_conn_id = conn_id
                break
        if not found_conn_id == 0:  # a conn_id present in an unfinished call message
            f_output = open(output_prefix + str(found_conn_id), 'a')
            if print_log:
                print('FROM CONNECTION ID...')
            while not line.startswith('<' + ' ' + message[CALLER] + ' ' + message[CALLEE]):
                f_output.write(line)
                finished[line_counter] = True
                line = f_input.readline()
                line_counter = line_counter + 1
                if not line:
                    break
            f_output.write(line)
            finished[line_counter] = True
            f_output.close()
        else:
            for req_id in req_ids:
                if (req_id in message) and (not finished[line_counter]) and (message[DIR] == '>'):
                    found_req_id = req_id
                    break
            if not found_req_id == 0:
                index = req_ids.index(found_req_id)
                f_output = open(output_prefix + str(conn_ids[index]), 'a')
                if print_log:
                    print('FROM REQUEST ID...')
                while not line.startswith('<' + ' ' + message[CALLER] + ' ' + message[CALLEE]):
                    f_output.write(line)
                    finished[line_counter] = True
                    line = f_input.readline()
                    line_counter = line_counter + 1
                    if not line:
                        break
                f_output.write(line)
                finished[line_counter] = True
                f_output.close()


if print_log:
    print('End of the second pass.')

if print_log:
    print('Total number of connections: ' + str(len(conn_ids)))

f_input.close()
