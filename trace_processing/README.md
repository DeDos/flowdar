**extract_connections.py**

This script can be used to split an Nginx trace file into individual trace files each containing one connection. The resulting trace files are compatible with visualization and analysis toolchain.
The correctness of this script has been tested with traces from Nginx 1.12.2.

Note: Before running this script, offsets should be removed using `process_function_naming.py`.

Usage:

    python3 extract_connections.py -i <input_trace> -o <output_directory> [--print_log]
    
    input_trace: a trace file in the format described in `documentation/TRACE_FILE.md`
    output_directory: a directory to save the output files
    print_log: print debugging information to the console

Running this script results in creating `n` trace files where `n` is the number of connections that are processed in `input_trace`.

Example:
Let `_25397_25397` be the raw trace file produced by the tracer tool.
1. Remove offsets:
    python3 process_function_naming.py -i _25397_25397 -o r_25397_25397 --remove_offset
2. Extract connections:
    python3 extract_connections.py -i r_25397_25397 -o connections

Individual connections will be in the folder "connections".

