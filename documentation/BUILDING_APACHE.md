Important environment variables:
```bash
export HOMEDIR=/home/jinzihao
export CHOPCHOP="$HOMEDIR/chophop"
export CXX=g++
export APACHE_BUILD_ENV=/home/jinzihao/apache_build_env
export LOCAL_TRACE_FILE=/home/jinzihao/apache_build_env/outputs/apache.local.trace
export GENERATION_SRC_DIR=/home/jinzihao/chopchop/dyngraph/generation
export APR_SRC=/home/jinzihao/apache_build_env/apr-1.6.2
export APR_BIN=/home/jinzihao/apache_build_env/apr_bin
export PREFIX=/home/jinzihao/apache_build_env/bin
export SSL_BIN=/home/jinzihao/apache_build_env/openssl_bin/
export APR_UTIL_SRC=/home/jinzihao/apache_build_env/apr-util-1.6.0/
export PCRE_SRC=/home/jinzihao/apache_build_env/pcre-8.40
```

The following commands are used to build Apache httpd:
```bash
# can set EXTRA_CFLAGS to -DCALLSTACK
export EXTRA_CFLAGS=
sudo rm -rf $APACHE_BUILD_ENV/bin/*
make clean
CC=gcc TRACE_FILE=$LOCAL_TRACE_FILE \
CFLAGS=' -g -finstrument-functions -I $GENERATION_SRC_DIR \
  -DOPENSSL_NO_SSL2 -fno-inline -fno-optimize-sibling-calls -fno-omit-frame-pointer '$EXTRA_CFLAGS \
  LDFLAGS=" $GENERATION_SRC_DIR/tracer_hooks.o -L${APR_BIN} -lapr-1 " \
./configure --enable-debugger-mode --enable-ssl --with-mpm=worker \
  --with-ssl=$SSL_BIN \
  --with-apr=$APR_SRC \
  --with-apr-util=$APR_UTIL_SRC \
  --with-pcre=$PCRE_SRC/pcre-config \
  --prefix=$PREFIX && \
TRACE_FILE=$LOCAL_TRACE_FILE make -j8
make install -j8
```

The following commands are used to build APR & APR-util:
```bash
CC=gcc TRACE_FILE=$LOCAL_TRACE_FILE \
CFLAGS=' -g -finstrument-functions -I $GENERATION_SRC_DIR \
  -fno-inline -fno-optimize-sibling-calls -fno-omit-frame-pointer -DCALLSTACK ' \
  LDFLAGS=" $GENERATION_SRC_DIR/tracer_hooks.o " ./configure \
make clean
make -j 8
sudo make install -j 8

CC=gcc TRACE_FILE=$LOCAL_TRACE_FILE \
  CFLAGS=' -g -finstrument-functions -I $GENERATION_SRC_DIR \
  -fno-inline -fno-optimize-sibling-calls -fno-omit-frame-pointer -DCALLSTACK ' \
  LDFLAGS=" $GENERATION_SRC_DIR/tracer_hooks.o " \
./configure --with-apr=$APR_SRC
make clean
make -j 8
sudo make install -j 8

cd $CHOPCHOP/dyngraph/generation
${CXX} xchg.cpp -o xchg
${CXX} libpatch.cpp -o libpatch
# FIXME update the paths below -- use --prefix path 
sudo ./patch_lib.sh /usr/local/apr/lib/libapr-1.so.0.6.2
sudo ./patch_lib.sh /usr/local/apr/lib/libaprutil-1.so.0.6.0
cd -
```
