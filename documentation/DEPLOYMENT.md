Deployment
==
Use the script in [../generation/README.md](../generation/README.md), first
modify the paths at its beginning, then `tb -DCALLSTACK`, then `tr` (in a
separate terminal), then `aprb`, then `ab`, and ideally Apache is built. Some
steps are not included in the script in README.md, which include:

* Before building the tracer, in `tracer_hooks.s`, modify .LC0: .string to the
  path of the trace file (a fifo file).

* Before building Apache, first build and install [pcre](http://www.pcre.org/)
  with `./configure; make; sudo make install` This will install pcre in a
  default location, which you might want to alter by using the `--prefix`
  parameter to the configure script.

* If Apache complains that some dynamic libraries could not be located, add
  `LD_LIBRARY_PATH=/usr/local/lib/:/usr/local/apr/lib/` (assuming that these
  are the paths where pcre, apr and apr-util are installed) at the very beginning
  of `bin/apachectl` located in Apache build path (which need not be your
  default system installation path).

* Before using `resolve.sh` in `results` folder, make sure Python is installed.
