
# Temporal Analysis Output

Temporal analysis is applied on the output of structural analysis and generates a file which consists of six space-separated fields. Lines of this files are sorted with regarding time difference in a decreasing manner.

The general format is as below:

`Direction Caller Callee TimeDiff TimeGood TimeBad LineGood LineBad Origin`

## Explanation of each field

field | type | meaning
-|-|-
Direction | {'><'} | call-return 
Caller | string | name of the caller function
Callee | string | name of the function being called
TimeDiff | decimal | difference between time duration for a function call in good and bad traces (microseconds)
TimeGood | decimal | time duration for a function call in good trace (microseconds)
TimeBad | decimal | time duration for a function call in bad trace (microseconds)
LineGood | decimal | line number where this messages appeared in the good trace
LineBad | decimal | line number where this messages appeared in the bad trace
Origin | string | name of the corresponding merged file that this message comes from

## Example
```
>< worker_thread:31a process_socket:0 22565655 12230 22577885 63 2011 1__10416_10422_2__9895_9904
```
