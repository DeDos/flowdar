Possible "Brittleness" of Patching
==
* patch_enter.sh depends on a specific assembly code pattern, works on Ubuntu 16.04, GCC 5.4, x86-64, check before porting to other platforms.
* patch_exit.sh depends on a specific assembly code pattern, works on Ubuntu 16.04, GCC 5.4, x86-64, check before porting to other platforms.
* gcc may omit inserting the call to `__cyg_profile_func_exit` for some functions. In Apache 2.4.26, the following functions lacks a call to `__cyg_profile_func_exit`:
```
destroy_and_exit_process
abort_on_oom
ap_abort_on_oom
ap_log_assert
yy_fatal_error
clean_child_exit
just_die
child_main
```

One could spot missing `__cyg_profile_func_exit`s with:
```
objdump -d httpd | grep -E '(<[^@]*>:|<__cyg_profile_func_exit>|<__cyg_profile_func_enter>)'
```

The above script filters out all the `__cyg_profile_func_enter()`s and `__cyg_profile_func_exit()`s, and normally the output should be:

```
func1
__cyg_profile_func_enter()
__cyg_profile_func_exit()
func2
__cyg_profile_func_enter()
__cyg_profile_func_exit()
func3
__cyg_profile_func_enter()
__cyg_profile_func_exit()
......
```

One enter and one exit for func1, another pair for func2, still another pair for func3...

And an abnormal case could be:

```
func1
__cyg_profile_func_enter()
__cyg_profile_func_exit()
func2
__cyg_profile_func_enter()
func3
__cyg_profile_func_enter()
__cyg_profile_func_exit()
......
```

One enter and one exit for func1, one enter and... wait... no exit for func2?! 
