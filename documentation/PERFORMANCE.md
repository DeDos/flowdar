How is performance measured?
==

```
./ab -n 100 -c 100 -B 192.168.1.4 http://192.168.1.3/
```

Results
==
### without tracer
```
This is ApacheBench, Version 2.3 <$Revision: 1757674 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking 192.168.1.3 (be patient).....done


Server Software:        Apache/2.4.25
Server Hostname:        192.168.1.3
Server Port:            80

Document Path:          /
Document Length:        45 bytes

Concurrency Level:      50
Time taken for tests:   0.014 seconds
Complete requests:      100
Failed requests:        0
Total transferred:      28900 bytes
HTML transferred:       4500 bytes
Requests per second:    7209.29 [#/sec] (mean)
Time per request:       6.935 [ms] (mean)
Time per request:       0.139 [ms] (mean, across all concurrent requests)
Transfer rate:          2034.65 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        1    1   0.4      1       2
Processing:     2    4   1.7      4       8
Waiting:        2    4   1.7      4       8
Total:          3    6   2.0      5      10

Percentage of the requests served within a certain time (ms)
  50%      5
  66%      7
  75%      7
  80%      8
  90%      8
  95%      9
  98%     10
  99%     10
 100%     10 (longest request)
```

### with tracer & -DCALLSTACK

```
This is ApacheBench, Version 2.3 <$Revision: 1757674 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking 192.168.1.3 (be patient).....done


Server Software:        Apache/2.4.25
Server Hostname:        192.168.1.3
Server Port:            80

Document Path:          /
Document Length:        45 bytes

Concurrency Level:      50
Time taken for tests:   18.476 seconds
Complete requests:      100
Failed requests:        0
Total transferred:      28900 bytes
HTML transferred:       4500 bytes
Requests per second:    5.41 [#/sec] (mean)
Time per request:       9238.140 [ms] (mean)
Time per request:       184.763 [ms] (mean, across all concurrent requests)
Transfer rate:          1.53 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    1   0.6      1       2
Processing:  2345 8617 1981.6   8499   13014
Waiting:     1476 7559 1718.8   7592   11331
Total:       2346 8618 1981.7   8501   13015

Percentage of the requests served within a certain time (ms)
  50%   8501
  66%   9380
  75%  10063
  80%  10384
  90%  11549
  95%  12064
  98%  12508
  99%  13015
 100%  13015 (longest request)
```

### with tracer (w/o -DCALLSTACK)
```
This is ApacheBench, Version 2.3 <$Revision: 1757674 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking 192.168.1.3 (be patient).....done


Server Software:        Apache/2.4.25
Server Hostname:        192.168.1.3
Server Port:            80

Document Path:          /
Document Length:        45 bytes

Concurrency Level:      100
Time taken for tests:   4.868 seconds
Complete requests:      100
Failed requests:        0
Total transferred:      28900 bytes
HTML transferred:       4500 bytes
Requests per second:    20.54 [#/sec] (mean)
Time per request:       4868.188 [ms] (mean)
Time per request:       48.682 [ms] (mean, across all concurrent requests)
Transfer rate:          5.80 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        2    3   0.5      3       4
Processing:   884 3637 1116.0   4019    4855
Waiting:      491 3298 1303.4   3646    4852
Total:        888 3640 1115.6   4022    4857

Percentage of the requests served within a certain time (ms)
  50%   4022
  66%   4483
  75%   4725
  80%   4742
  90%   4813
  95%   4845
  98%   4856
  99%   4857
 100%   4857 (longest request)
```

5. 4 + buffer built into tracer_hook
This is ApacheBench, Version 2.3 <$Revision: 1757674 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking 192.168.1.3 (be patient).....done


Server Software:        Apache/2.4.25
Server Hostname:        192.168.1.3
Server Port:            80

Document Path:          /
Document Length:        45 bytes

Concurrency Level:      100
Time taken for tests:   4.560 seconds
Complete requests:      100
Failed requests:        0
Total transferred:      28900 bytes
HTML transferred:       4500 bytes
Requests per second:    21.93 [#/sec] (mean)
Time per request:       4560.352 [ms] (mean)
Time per request:       45.604 [ms] (mean, across all concurrent requests)
Transfer rate:          6.19 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        2    4   0.8      4       6
Processing:   967 3274 1082.9   3460    4552
Waiting:      681 2996 1190.4   3076    4550
Total:        972 3278 1082.3   3464    4555

Percentage of the requests served within a certain time (ms)
  50%   3464
  66%   4183
  75%   4366
  80%   4393
  90%   4515
  95%   4538
  98%   4549
  99%   4555
 100%   4555 (longest request)
```
6. 4 + trace_buff
```
Server Software:        Apache/2.4.25
Server Hostname:        192.168.1.3
Server Port:            80

Document Path:          /
Document Length:        45 bytes

Concurrency Level:      100
Time taken for tests:   1.623 seconds
Complete requests:      100
Failed requests:        0
Total transferred:      27900 bytes
HTML transferred:       4500 bytes
Requests per second:    61.60 [#/sec] (mean)
Time per request:       1623.375 [ms] (mean)
Time per request:       16.234 [ms] (mean, across all concurrent requests)
Transfer rate:          16.78 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        2    3   0.6      3       4
Processing:    17 1331 369.9   1482    1612
Waiting:       17 1188 446.4   1390    1610
Total:         21 1334 369.5   1485    1614

Percentage of the requests served within a certain time (ms)
  50%   1485
  66%   1573
  75%   1579
  80%   1584
  90%   1602
  95%   1612
  98%   1614
  99%   1614
 100%   1614 (longest request)
```
7. Commit c91a20f606734b4fa795a375c653066b2b19a2ef (with trace_buff, output trace to console)
```
This is ApacheBench, Version 2.3 <$Revision: 1706008 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking 127.0.0.1 (be patient).....done


Server Software:        Apache/2.4.26
Server Hostname:        127.0.0.1
Server Port:            80

Document Path:          /
Document Length:        45 bytes

Concurrency Level:      100
Time taken for tests:   14.145 seconds
Complete requests:      100
Failed requests:        0
Total transferred:      28900 bytes
HTML transferred:       4500 bytes
Requests per second:    7.07 [#/sec] (mean)
Time per request:       14144.870 [ms] (mean)
Time per request:       141.449 [ms] (mean, across all concurrent requests)
Transfer rate:          2.00 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        1    1   0.1      1       1
Processing:  5070 11636 2423.5  12468   14143
Waiting:     3753 10686 2759.7  11535   14130
Total:       5071 11637 2423.5  12469   14144

Percentage of the requests served within a certain time (ms)
  50%  12469
  66%  13403
  75%  13756
  80%  13822
  90%  13987
  95%  14065
  98%  14138
  99%  14144
 100%  14144 (longest request)
```
8. Commit c91a20f606734b4fa795a375c653066b2b19a2ef (with trace_buff, write trace to file)
```
This is ApacheBench, Version 2.3 <$Revision: 1706008 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking 127.0.0.1 (be patient).....done


Server Software:        Apache/2.4.26
Server Hostname:        127.0.0.1
Server Port:            80

Document Path:          /
Document Length:        45 bytes

Concurrency Level:      100
Time taken for tests:   5.007 seconds
Complete requests:      100
Failed requests:        0
Total transferred:      28900 bytes
HTML transferred:       4500 bytes
Requests per second:    19.97 [#/sec] (mean)
Time per request:       5006.518 [ms] (mean)
Time per request:       50.065 [ms] (mean, across all concurrent requests)
Transfer rate:          5.64 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        1    1   0.0      1       1
Processing:  2277 4016 722.4   4103    5004
Waiting:     2034 3662 884.3   3761    4980
Total:       2279 4017 722.4   4104    5005

Percentage of the requests served within a certain time (ms)
  50%   4104
  66%   4417
  75%   4777
  80%   4851
  90%   4948
  95%   4995
  98%   5002
  99%   5005
 100%   5005 (longest request)
```
TODO
==
* Repeat the measurement more times.
* Analyze the measurements with a statistical approach (e.g. hypothesis testing).
* Make a conclusion - (increased? decreased? no significant change?)
* Response to Slowloris/ReDOS
* curl/wget download large files

