Definition
==
Each line of tracer output consists of data fields separated by space (" "). There are two types of trace: call and return, identified by its direction field. A call-trace has 14 fields, while a return-trace has 9 fields.

The data fields are defined below:

field | type
-|-
Direction | {'<', '>'}
Caller | hex
Callee | hex
PID | decimal
Timestamp (s) | decimal
Timestamp (us) | decimal
Thread ID\*** | decimal
arg1* | hex
arg2* | hex
arg3* | hex
arg4* | hex
arg5* | hex
arg6* | hex
return value** | hex
stack | (2-digit hex) * 128, low addr -> high addr

\* Present in call-trace only

\** Present in return-trace only

\*** Not implemented yet


Example
==
Call
--
```
> 0x400ac0 0x400a76 9495 1500411370 919960 10 0x1 0x7ffe99252838 0x7ffe99252838 0x0 0x401190 0x7f1b837cbab0 000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000714c15d74cea930000000000000000800940000000000030282599fe7f0000000000000000000030272599fe7f0000840a40000000000050272599fe7f0000a00c400000000000
```

Return
--
```
< 0x400ca5 0x400aa9 9495 1500411370 920045 12 0x5 0000000000000000000000000000000000000000000000000000000000000000000000000000000000714c15d74cea930500000000000000800940000000000030282599fe7f0000000000000000000030272599fe7f0000e50a40000000000030272599fe7f0000000000000000000050272599fe7f0000a50c400000000000
```
