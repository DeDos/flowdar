Call stack simulator
--
The algorithm begins with an empty stack.

When the validator encounters a call (">"), push the caller and callee onto the simulated call stack.

When the validator encounters a return ("<"), check if the record on the stack top has the same caller and callee address. If the return matches the call on the stack top, pop stack. Otherwise, report an error.

After reading all the trace lines, if the stack is not empty, report an error.

Call-return mismatch
--
Call stack simulator works well with a single-process, single-thread program's trace. When multiple processes/threads are involved, the following "patch" should be applied to the call stack simulator approach:

The following algorithm checks whether a call-return mismatch is valid:
```
The numbers in the following diagram denote call stack level, an increment in the number denotes a function call, while a decrement denotes a function return.
 
p1: 0 1 2 1 2 3 4 fork()! 4 5 4 3 2 exit()!
                    |
p2: - - - - - - -   --->  4 3 2 3 2 3 4 5 4 3 2 1 2 1 0

In p1, the 0->1 at the very beginning, and the *second* occurrence of 1->2 are call-without-returns (Because there is no 2->1 & 1->0 at the end.). There is no return-without-calls in p1.
In p2, the 4->3 & 3->2 at the very beginning, and the *first* occurrence of 2->1, plus the 1->0 at the very end are return-without-calls. There is no call-without-returns in p2.

If we add another fork() and another exit() to the above example, it could become:

p1: 0 1 2 1 2 3 4 fork()! 4 5 4 3 2 exit()!
                    |
p2: - - - - - - -   --->  4 3 2 fork()! 2 3 2 3 4 5 exit()!
                                  |
p3: - - - - - - - - - - - - - -   --->  2 3 2 1 0

p1 is not changed here.
In p2, the 4->3 & 3->2 at the very beginning are return-without-calls, and the 2->3, 3->4, 4->5 at the end are call-without-returns.
In p3, the 2->1 & 1->0 at the end are return-without-calls.

Considering these two examples, there is a way to tell whether a call-without-return or a return-without-call is valid or not:
Record all the call-without-returns and return-without-calls, and check if they form a V-shaped chain - going down to the bottom, then going all the way up.
Valid examples from p1, p2 and p3 above:
p1: 0 1 2 (Down from 0 to 0, then up from 0 to 2)
p2: 4 3 2 3 4 5 (Down from 4 to 2, then up from 2 to 5)
p3: 2 1 0 (Down from 2 to 0, then up from 0 to 0)

More valid examples:
5 4 3 2 3 4 (Down from 5 to 2, then up from 2 to 4)
3 4 5 (Down from 3 to 3, then up from 3 to 5)
4 3 2 1 (Down from 4 to 1, then up from 1 to 1)
```
