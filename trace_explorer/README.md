# This directory contains the code for trace visualization

# note
This tool assumes that the trace matches the source. If the source does not match the trace, the trace may have entries that are invalid (null or states that the file could not be found) because clang parses through all the files and applies the #defines in the headers. Currently, setup/setup.sh assumes that nginx_version="nginx-1.12.1" and will fetch that version of nginx. If you wish to change the version, type in different value such as nginx_version="nginx-1.13.1"

# files
extra/ - source that will be put into clang's source tree
nginx_source_and_traces/ - trace files included here (used for the program)
setup/ - building source and compiling program
tools/ - fetching nginx c/h files passing them into program

## Setup
Setup clang (using 7.0.0)

# 1 setup from scratch

Go to setup directory and run "./setup YOUR_CLANG_LOCATION FROM_SCRATCH"

Replace YOUR_CLANG_LOCATION with the directory you wish to install clang source tree

This will fetch and build the entire clang source tree and build this tool in "YOUR_CLANG_LOCATION"

NOTE: This will build RELEASE and use the gold linker instead of ldd
WARNING: This will take a long time to build as the clang source tree takes a while to link (On 4GB system ~ 15 minutes)
WARNING2: The linking may also fail if your computer does not have > 4GB. A one time link job will require around ~8GB. To continue to link, run ninja in ~/clang-llvm/build and follow the rest of the commands listed in setup.sh

What setup does:
fetch llvm source
fetch clang source
move the program's source files into clang
fetch ninja
fetch nginx source
compile llvm
compile ninja
configure nginx
runs the program

# 2 setup from already having clang

follow setup tutorial in (setup directories and building clang part) MAKE SURE THIS MATCHES:
https://clang.llvm.org/docs/LibASTMatchersTutorial.html

Go to setup directory and run setup.sh 

For example, "./setup.sh ~/clang-llvm"

The project will put the contents of stack-tracer source into the clang directory and run ninja in the build directory

# requirements
curses
cmake

# cleaning
If you ran `./setup YOUR_CLANG_LOCATION FROM_SCRATCH` then simply delete `YOUR_CLANG_LOCATION` to remove files obtained to set up and run the tool.

# running
The commandline arguments take in only c files -- 

there is a tool (nginx_src_fetch_run.sh) in $YOUR_CLANG_DIR/build directory that fetches all the c files and passes them as command line arguments to stack-tracer

Usage:

"./ngnix_src_fetch_run.sh" = will fetch all C files in ngnix/ and use "trace_resolved.txt" as the trace_file
"./ngnix_src_fetch_run.sh C_DIRECTORY" = will fetch all C files in C_DIRECTORY and use "trace_resolved.txt" as the trace_file
"./ngnix_src_fetch_run.sh C_DIRECTORY TRACEFILE" = will fetch all C files in C_DIRECTORY and use "TRACEFILE" as the trace_file

# controls

up - go up the trace by one line

down - go down the trace by one line

left - go up the trace by 10 line

right - go down the trace by 10 line

'v' - open up editor to view source code (set EDITOR environment variable to your editor, for example, export EDITOR=vim)

' ' - switch different views (abstract view vs detailed view)

'g' - go to a particular line number in the trace

'q' - quit program
