#!/bin/bash
#
#Copyright (c) 2019, University of Pennsylvania
#All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the <organization> nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
#DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


SRC_DIR=$(cd ..; pwd)
clang_path="$1"
clang_src_path=$clang_path/llvm/tools/clang/tools
clang_build_path=$clang_path/build
program_name="trace-viewer"
nginx_version="nginx-1.12.1"
nginx_targz="${nginx_version}.tar.gz"
nginx_url="https://nginx.org/download/${nginx_targz}"

finish_clang_setup () {
	rm -rf $clang_src_path/extra
	ln -s $SRC_DIR/extra $clang_src_path/extra
	(cd $clang_build_path; ./ninja $program_name)
	rm -rf $clang_build_path/nginx_src_fetch_run.sh
	ln -s $SRC_DIR/tools/nginx_src_fetch_run.sh $clang_build_path/nginx_src_fetch_run.sh
	ln -s $SRC_DIR/nginx_source_and_traces/trace_resolved.txt $clang_build_path/trace_resolved.txt
}

compile_and_run_clang() {
	echo "Running $program_name...\n"
	echo "Please wait as this may take a while.\n"
	(cd $clang_build_path; ninja $program_name && ./nginx_src_fetch_run.sh)
}

#check for clang's path
if [ "$#" -eq 1 ]; then
	finish_clang_setup
elif [ "$#" -eq 2 ]; then
	if [ "$2" = "FROM_SCRATCH" ]; then
		(mkdir $clang_path && cd $clang_path; \
		git clone http://llvm.org/git/llvm.git; \
		cd llvm/tools; \
		git clone http://llvm.org/git/clang.git; \
		cd clang/tools; \
		git clone http://llvm.org/git/clang-tools-extra.git extra; \
		cd $clang_path; \
		git clone https://github.com/martine/ninja.git; \
		mkdir build; \
		cd ninja; \
		git checkout release; \
		./configure.py --bootstrap; \
		cp -f ninja ../build; \
		cd $clang_path; \
		cd build; \
		cmake -G Ninja ../llvm -DLLVM_USE_LINKER=gold -DCMAKE_BUILD_TYPE=Release; \
		#download nginx source \
		wget $nginx_url; \
		tar xzvf $nginx_targz; \
		rm -f $nginx_targz; \
		mv $nginx_version nginx; \
		cd nginx; \
		./configure; \
		)
		finish_clang_setup
		compile_and_run_clang
	else
		echo "Please specify ./setup.sh CLANG_DIR FROM_SCRATCH"
	fi
else
	echo "Please specify path to clang as an argument such as ./setup.sh CLANG_DIR [FROM_SCRATCH]"
fi
