/**
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


 * @file Tracer.cpp
 * @brief This class implements the tracer
 *
 * @author Henry Zhu
 * @date 5/17/2018
 */
#include "tracer.h"

using namespace NginxTracer;

/**
* @brief Returns the singleton tracer
*
* @return singleton tracer
*/
const Tracer& Tracer::get_tracer()
{
	static Tracer tracer;
	return tracer;
}

/**
* @brief Reads lines from a given file and parses through each line of the file into a vector of trace elements
* @param filename string of the trace file
*
* @return Vector of trace elements
*/
const std::vector<TraceElement> Tracer::parse_lines(const std::string& filename)
{
	std::vector<TraceElement> elements;
	std::ifstream file(filename);
	std::string line;
	while(std::getline(file, line))
	{
		elements.push_back(TraceElement(line));
	}
	return elements;
}

