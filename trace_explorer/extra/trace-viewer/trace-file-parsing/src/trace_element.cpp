/**
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


 * @file TraceElement.cpp
 * @brief This file implments the member functions for the TraceElement class
 *
 * @author Henry Zhu
 * @date 5/17/2018
 */

#include "trace_element.h"

using namespace NginxTracer;

//max number of tokens in a line of the trace file
constexpr static int MAX_CALL_FUNCTION_ARGUMENTS = 14;
constexpr static int MAX_RETURN_FUNCTION_ARGUMENTS = 9;

//size of 64bit in characters
constexpr size_t bit64_length_in_characters = sizeof(uint64_t) * 2;

/**
 * @brief Parses a string of stack memory and converts it to a vector of uint64_t
 * 
 * @param stack_memory is the string of characters representing memory
 * @param stack_dump is the vector of uint64_t that is filled when stack_memory is converted to uint64_t's
 */

inline void ParseStackDump(const std::string& stack_memory, std::vector<uint64_t>& stack_dump)
{
	//number of elements
	auto elements = stack_memory.length() / bit64_length_in_characters;

	//increase size before hand
	stack_dump.resize(elements);

	for (size_t i = 0; i < elements; i++)
	{
		//substring and then convert to type
		std::stringstream converter(stack_memory.substr(i * bit64_length_in_characters, bit64_length_in_characters));

		converter >> std::hex >> stack_dump.at(i);
	}
}

/**
 * @brief Constructor for trace_element that takes in a line from the trace file and constructs a class by parsing the line
 * 
 * @param trace_contents is the line parsed from the trace file
 */

TraceElement::TraceElement(const std::string& trace_contents)
{
	//lambda function to split by delimiter
	auto SplitByDelimiter = [](const std::string& str, std::string&& delimiter) {
		std::vector<std::string> results;
		size_t start = 0;
		size_t end   = 0;
		while ((end = str.find(delimiter, start)) != std::string::npos)
		{
			//push back the split string
			results.push_back(str.substr(start, end - start));

			start = end + delimiter.length();
		}
		//push last result back
		results.push_back(str.substr(start));

		return results;
	};

	//check if caller/callee has weird stuff (INVALID name)
	auto CheckIfCallerOrCalleeHasWeird = [this](const std::string& caller, const std::string callee) {
		if (caller.find("INVALID") != std::string::npos)
		{
			this->is_invalid = true;
		}
		else if (callee.find("INVALID") != std::string::npos)
		{
			this->is_invalid = true;
		}
	};

	//get the parameters by line
	std::vector<std::string> trace_parameters = SplitByDelimiter(trace_contents, " ");

	//move trace contents
	this->trace_contents = trace_contents;

	//check if the line is representing a call function
	if (trace_parameters[0] == ">")
	{
		//check length
		if (trace_parameters.size() > MAX_CALL_FUNCTION_ARGUMENTS)
		{
			throw "Trace has above max number of call arguments";
		}
		
		this->is_invalid             = false;
		this->type                 = FunctionType::Call;
		auto caller_with_line_number               = SplitByDelimiter(trace_parameters[1], ":");
		this->caller = caller_with_line_number[0];
		this->caller_line_num = caller_with_line_number[1];
		auto callee_with_line_number               = SplitByDelimiter(trace_parameters[2], ":");
		this->callee = callee_with_line_number[0];
		this->callee_line_num = callee_with_line_number[1];
		this->pid                  = std::move(trace_parameters[3]);
		this->timestamp_s          = std::move(trace_parameters[4]);
		this->timestamp_us         = std::move(trace_parameters[5]);
		this->thread_id            = std::move(trace_parameters[6]);
		this->arg1                 = std::move(trace_parameters[7]);
		this->arg2                 = std::move(trace_parameters[8]);
		this->arg3                 = std::move(trace_parameters[9]);
		this->arg4                 = std::move(trace_parameters[10]);
		this->arg5                 = std::move(trace_parameters[11]);
		this->arg6                 = std::move(trace_parameters[12]);
		std::string&& stack_memory = std::move(trace_parameters[13]);

		CheckIfCallerOrCalleeHasWeird(this->caller, this->callee);

		ParseStackDump(stack_memory, this->stack_dump);
	}
	//return
	else if (trace_parameters[0] == "<")
	{
		//check length
		if (trace_parameters.size() > MAX_RETURN_FUNCTION_ARGUMENTS)
		{
			throw "Trace has above max number of return arguments";
		}
		this->is_invalid             = false;
		this->type                 = FunctionType::Return;
		auto caller_with_line_number               = SplitByDelimiter(trace_parameters[1], ":");
		this->caller = caller_with_line_number[0];
		this->caller_line_num = caller_with_line_number[1];
		auto callee_with_line_number               = SplitByDelimiter(trace_parameters[2], ":");
		this->callee = callee_with_line_number[0];
		this->callee_line_num = callee_with_line_number[1];
		this->pid                  = std::move(trace_parameters[3]);
		this->timestamp_s          = std::move(trace_parameters[4]);
		this->timestamp_us         = std::move(trace_parameters[5]);
		this->thread_id            = std::move(trace_parameters[6]);
		this->return_value         = std::move(trace_parameters[7]);
		std::string&& stack_memory = std::move(trace_parameters[8]);

		CheckIfCallerOrCalleeHasWeird(this->caller, this->callee);

		ParseStackDump(stack_memory, this->stack_dump);
	}
	//bad
	else
	{
		this->type = FunctionType::Unknown;
		std::cout << "Warning: unrecognized trace {" << this->trace_contents << "}" << std::endl;
	}
}

/**
 * @brief Get connection id from the trace_element
 * 
 * @return string representing connection id
 */

const std::string TraceElement::get_connid() const
{
	//first element is the assumed to be connid
	return std::to_string(this->stack_dump.at(0));
}

/**
 * @brief Get the line trace that was parsed by this trace_element
 * 
 * @return string containing the line from the trace line
 */

const std::string& TraceElement::get_trace_contents() const 
{
	return this->trace_contents;
}

/**
 * @brief Check if the trace element is weird
 * 
 * @return bool that is true if the trace_element is weird
 */

bool TraceElement::get_is_invalid() const
{
	return this->is_invalid;
}

/**
 * @brief Returns the caller's name
 * 
 * @return const std::string& A reference to the name of the caller
 */

const std::string& TraceElement::get_caller() const
{
	return this->caller;
}

/**
 * @brief Returns the caller's line number
 * 
 * @return unsigned long The caller's line number
 */

unsigned long TraceElement::get_caller_linenum() const
{
	//change string line number (hex, so base 16) into the type long
	return std::stol(this->caller_line_num, nullptr, 16);
}

/**
 * @brief Returns the caller's name
 * 
 * @return const std::string& A reference to the name of the caller
 */

const std::string& TraceElement::get_callee() const
{
	return this->callee;
}

/**
 * @brief Returns the callee's line number
 * 
 * @return unsigned long The callee's line number
 */

unsigned long TraceElement::get_callee_linenum() const
{
	//change string line number (hex, so base 16) into the type long
	return std::stol(this->callee_line_num, nullptr, 16);
}

/**
 * @brief Returns the callee's line number
 * 
 * @return unsigned long The callee's line number
 */

FunctionType TraceElement::get_function_type() const
{
	return type;
}

/**
 * @brief Returns the return value
 * 
 * @return const std::string& Return value
 */

const std::string& TraceElement::get_return_value() const
{
	return return_value;
}

/**
 * @brief Returns the argument on the stack
 * 
 * @brief arg_no The argument number
 * @return 
 */

const std::string& TraceElement::get_arg(unsigned long arg_no) 
{
	switch(arg_no)
	{
		case 1: return arg1;
		case 2: return arg2;
		case 3: return arg3;
		case 4: return arg4;
		case 5: return arg5;
		default: break;
	}
	ASSERT((arg_no >= MIN_ARG_INDEX && arg_no <= MAX_ARG_INDEX), "arg_no should not be " << arg_no << ". Instead it should be between " << MIN_ARG_INDEX << " and " << MAX_ARG_INDEX);
	std::terminate();
}

