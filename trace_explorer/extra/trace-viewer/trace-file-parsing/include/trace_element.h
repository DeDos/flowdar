/**
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


 * @file TraceElement.h
 * @brief This class represents an element (line) in the trace file
 *
 * @author Henry Zhu
 * @date 5/17/2018
 */

#pragma once

#include <array>
#include <fstream>
#include <iostream>
#include <sstream>
#include <stdint.h>
#include <vector>

#include "debug_help.h"

namespace NginxTracer
{
	//These values represent the number of arguments in the trace. 1 would be the first argument to the call ... 5 would be the last argument to the call (5 being current limit in trace)
	constexpr size_t MIN_ARG_INDEX = 1;
	constexpr size_t MAX_ARG_INDEX = 5;

	enum class FunctionType
	{
		Call,
		Return,
		Unknown
	};

	/**
	 * @brief element representing each line in the trace file.
	 **/

	class TraceElement
	{
		public:
			explicit TraceElement() = default;
			explicit TraceElement(const std::string& trace_contents);

			const std::string get_connid() const;
			const std::string& get_trace_contents() const;
			bool get_is_invalid() const;

			const std::string& get_caller() const;
			unsigned long get_caller_linenum() const;

			const std::string& get_callee() const;
			unsigned long get_callee_linenum() const;

			FunctionType get_function_type() const;
			const std::string& get_return_value() const;
			const std::string& get_arg(unsigned long arg_no);
		private:
			bool is_invalid;
			FunctionType type;
			std::string trace_contents;
			std::string caller;
			std::string caller_line_num;
			std::string callee;
			std::string callee_line_num;
			std::string pid;
			std::string timestamp_s;
			std::string timestamp_us;
			std::string thread_id;
			std::string return_value;
			std::string arg1;
			std::string arg2;
			std::string arg3;
			std::string arg4;
			std::string arg5;
			std::string arg6;
			std::vector<uint64_t> stack_dump;
	};
} 

