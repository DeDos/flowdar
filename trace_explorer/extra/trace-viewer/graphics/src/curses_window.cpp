/**
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


 * @file CursesWindow.cpp
 * @brief Implementation of CursesWindow
 * @author Henry Zhu
 * @version 0.01
 * @date 2018-05-18
 */
#include "curses_window.h"

/**
 * @brief Constructs a window from just a screen
 *
 * @param screen Screen of the window
 * @param bordered Bordered window or not
 */
CursesWindow::CursesWindow(Screen<screen_type> screen, bool bordered) : Window(std::move(screen), bordered)
{
}


/**
 * @brief Constructs a window with a parent
 *
 * @param parent Parent of this window
 * @param screen Screen of the window
 * @param bordered Bordered window or not
 */
CursesWindow::CursesWindow(std::weak_ptr<Window> parent, Screen<screen_type> screen, bool bordered) : Window(parent, std::move(screen), bordered)
{
	//create window (with custom deleter since delwin needs to be called)
	window = custom_unique_ptr<WINDOW>(newwin(screen.height, screen.width, screen.y, screen.x), [](WINDOW* win) { delwin(win); });
	keypad(window.get(), TRUE);

	//calculate the percentage of parents
	if(auto p = parent.lock())
	{
		percent_x_of_parent = static_cast<double>(screen.x) / static_cast<double>(p->get_screen().x);
		percent_y_of_parent = static_cast<double>(screen.y) / static_cast<double>(p->get_screen().y);
		percent_width_of_parent = static_cast<double>(screen.width) / static_cast<double>(p->get_screen().width);
		percent_height_of_parent = static_cast<double>(screen.height) / static_cast<double>(p->get_screen().height);
	}
}

/**
 * @brief Constructs a window with a parent
 *
 * @param parent_window Parent window
 * @param x Top left of start
 * @param y Top left of start
 * @param width Width of screen
 * @param height Height of screen
 * @note not implemented
 */
CursesWindow::CursesWindow(std::weak_ptr<Window> parent_window, screen_type x, screen_type y, screen_type width, screen_type height) : CursesWindow(parent_window, Screen<screen_type>(x, y, width, height))
{

}

CursesWindow::~CursesWindow()
{

}

/**
 * @brief Adds a window under here
 *
 * @param window Window at add
 */
void CursesWindow::add_subwindow(const std::shared_ptr<Window> window)
{
	subwindows.push_back(window);
}

/**
 * @brief Gets the window by its index
 *
 * @param index The index
 *
 * @return Indexed subwindow
 */
const std::shared_ptr<Window> CursesWindow::get_subwindow(size_t index)
{
	return subwindows.at(index);
}


void CursesWindow::set_screen(Screen<screen_type> screen) 
{
	this->screen = screen;
}

/**
 * @brief Get screen dimensions
 *
 * @return screen
 */
const Screen<screen_type>& CursesWindow::get_screen() const
{
	return screen;
}

/**
 * @brief Clears the window
 */
void CursesWindow::clear() const
{
	wclear(window.get());
}

/**
 * @brief refreshes the window and its sub windows
 */
void CursesWindow::refresh() const
{
	wrefresh(window.get());

	if(bordered)
	{
		this->draw_border();
	}

	for(const auto& subwindow : subwindows)
	{
		subwindow->refresh();
	}
}

void CursesWindow::outrefresh() const
{
	wnoutrefresh(window.get());
}

/**
 * @brief Resize the window
 */
void CursesWindow::resize()
{
	auto EqualsDouble = [](double a, double b)
	{
		return std::abs(a - b) < 0.01;
	};

	//check if screen is not topmost
	if(!EqualsDouble(percent_width_of_parent, 0.0) && !EqualsDouble(percent_height_of_parent, 0.0))
	{
		//check if the parent ptr is still valid
		if(auto p = parent.lock())
		{
			//calculate new screen size (percentage) from parent
			screen.width = static_cast<double>(p->get_screen().width) * percent_width_of_parent;
			screen.height = static_cast<double>(p->get_screen().height) * percent_height_of_parent;
		
			//resize
			wresize(window.get(), screen.height, screen.width);
		}
	}
	
	//resize children
	for(const auto& subwindow : subwindows)
	{
		subwindow->resize();
	}
}

/**
 * @brief Move by percent
 */
void CursesWindow::move()
{
	auto EqualsDouble = [](double a, double b)
	{
		return std::abs(a - b) < 0.01;
	};

	//check if screen is not topmost
	if(!EqualsDouble(percent_width_of_parent, 0.0) && !EqualsDouble(percent_height_of_parent, 0.0))
	{
		//check if the parent ptr is still valid
		if(auto p = parent.lock())
		{
			//calculate new screen size (percentage) from parent
			screen.x = static_cast<double>(p->get_screen().x) * percent_x_of_parent;
			screen.y = static_cast<double>(p->get_screen().y) * percent_y_of_parent;
		
			//move
			mvwin(window.get(), screen.y, screen.x);
		}
	}
	
	//move children
	for(auto&& subwindow : subwindows)
	{
		subwindow->move();
	}
}

/**
 * @brief Draws a border
 */
void CursesWindow::draw_border() const
{
    	wborder(window.get(), '|', '|', '-', '-', '+', '+', '+', '+');
}

/**
 * @brief Draws text at specific x, y
 *
 * @param x X coord
 * @param y Y coord
 * @param text Text to print
 */
void CursesWindow::draw_text(screen_type x, screen_type y, const std::string& text)
{
	mvwprintw(window.get(), y, x, text.c_str());
}

/**
 * @brief Draws text at specific x, y
 *
 * @param x X coord
 * @param y Y coord
 * @param text Text to print
 */
void CursesWindow::draw_text(screen_type x, screen_type y, std::string text)
{
	mvwprintw(window.get(), y, x, text.c_str());
}

/**
 * @brief Get character from keyboard when interacting with window
 *
 * @return Gets a character
 */
int CursesWindow::getchar()
{
	return wgetch(window.get());
}

/**
 * @brief Sets the cursor to x and y
 *
 * @param x X
 * @param y Y
 */
void CursesWindow::set_cursor(long x, long y)
{
	wmove(window.get(), y, x);
}

/**
 * @brief Gets the cursor
 *
 * @return Pair containing x and y
 */
std::pair<long, long> CursesWindow::get_cursor()
{
	int x{};
	int y{};
	getyx(window.get(), y, x);
	return {x, y};
}

/**
 * @brief Sets an attribute on for the window
 *
 * @param attribute Attribute to set
 */
void CursesWindow::set_attribute_on(int attribute)
{
	wattron(window.get(), attribute);
}

/**
 * @brief Sets an attribute off for the window
 *
 * @param attribute Attribute to turn off
 */
void CursesWindow::set_attribute_off(int attribute)
{
	wattroff(window.get(), attribute);
}
