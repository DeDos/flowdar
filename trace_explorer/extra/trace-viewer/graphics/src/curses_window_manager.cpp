/**
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


 * @file CursesWindowManager.cpp
 * @brief Implementation of CursesWindowManager
 * @author Henry Zhu
 * @version 0.01
 * @date 2018-05-18
 */

#include "curses_window_manager.h"

/**
 * @brief Default constructor 
 */
CursesWindowManager::CursesWindowManager() : WindowManager()
{
}

CursesWindowManager::~CursesWindowManager()
{
	shutdown();
}

/**
 * @brief Initializs the window manager
 */
void CursesWindowManager::init()
{
	initscr();
	start_color();
	if(has_colors() == FALSE)
	{
		std::cout << "Terminal does not support color" << std::endl;
	}
	else
	{
		std::cout << "Terminal does support color" << std::endl;
	}
	cbreak();
	noecho();
	keypad(stdscr, TRUE);

	//timeout is used to block indefinitely for getting input from curses
	timeout(-1);
	
	Screen<screen_type> screen(0, 0, 0, 0);
	getmaxyx(stdscr, screen.height, screen.width);
	
	top_window = std::make_shared<CursesWindow>(std::move(screen));
}

/**
 * @brief Shut down the window manager
 */
void CursesWindowManager::shutdown()
{
	endwin();
}

/**
 * @brief Creates a window on topmost window
 *
 * @param screen Screen dimensions
 * @param bordered Bordered window?
 */
void CursesWindowManager::create_window(Screen<screen_type> screen, bool bordered)
{
	top_window->add_subwindow(std::make_shared<CursesWindow>(top_window, std::move(screen), bordered));
}

/**
 * @brief Get the top most window
 *
 * @return top window
 */
std::shared_ptr<Window> CursesWindowManager::get_top_window() const
{
	return top_window;
}

/**
 * @brief Fetches a subwindow at index
 *
 * @param index Index of subwindow
 *
 * @return Subwindow
 */
std::shared_ptr<Window> CursesWindowManager::get_window(unsigned int index) const
{
	return top_window->get_subwindow(index);
}

/**
 * @brief Get screen dimensions
 *
 * @return Screen dimensions
 */
const Screen<screen_type>& CursesWindowManager::get_screen() const
{
	return top_window->get_screen();
}

/**
 * @brief Refreshes topmost window and all subwindows
 */
void CursesWindowManager::refresh()
{
	/*
	screen_type new_x, new_y;

	//recalculate window size
	getmaxyx(stdscr, new_y, new_x);

	auto screen = top_window->get_screen();

	if(screen.height != static_cast<int>(new_y) && screen.width != static_cast<int>(new_x))
	{
		top_window->set_screen(Screen<screen_type>(0, 0, new_x, new_y));
	}
	*/
	top_window->refresh();
}


