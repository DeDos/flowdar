cmake_minimum_required(VERSION 3.5)

project(graphics)

find_package(Curses REQUIRED)
include_directories(${CURSES_INCLUDE_DIR})

file(GLOB_RECURSE headers include/*.h)
file(GLOB_RECURSE sources src/*.cpp)

add_library(${PROJECT_NAME}
	STATIC
	${sources}
	)

add_library(frontend::${PROJECT_NAME} ALIAS ${PROJECT_NAME})

target_include_directories(${PROJECT_NAME}
	PUBLIC
	include/
	)

target_link_libraries(${PROJECT_NAME}
	PRIVATE
	${CURSES_LIBRARIES}
	)

