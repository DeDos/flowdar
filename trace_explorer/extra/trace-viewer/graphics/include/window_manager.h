/**
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


 * @file WindowManager.h
 * @brief This class represents a window manager, which manages windows
 * @author Henry Zhu
 * @version 0.01
 * @date 2018-05-18
 */

#pragma once

#include <vector>
#include <memory>
#include <iostream>
#include <fstream>

//forward declare Window
class Window;

template<typename T>
struct Screen
{
	Screen(T x, T y, T width, T height) : x(x), y(y), width(width), height(height) {}	
	
	int x;
	int y;
	int width;
	int height;
};

using screen_type = unsigned int;

class WindowManager
{
public:
	virtual ~WindowManager() {};

	virtual void init() = 0;
	virtual void shutdown() = 0;

	virtual void create_window(Screen<screen_type> screen, bool bordered = false) = 0;
	virtual std::shared_ptr<Window> get_top_window() const = 0;
	virtual std::shared_ptr<Window> get_window(unsigned int index) const = 0;

	virtual const Screen<screen_type>& get_screen() const = 0; 

	virtual void refresh() = 0;
protected:
	std::shared_ptr<Window> top_window;
};

