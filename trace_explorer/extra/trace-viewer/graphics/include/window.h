/**
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


 * @file Window.h
 * @brief Abstract window class
 * @author Henry Zhu
 * @version 0.01
 * @date 2018-05-18
 */

#pragma once

#include "window_manager.h"

class Window
{
public:
	explicit Window(Screen<screen_type> screen, bool bordered = false) : screen(screen), bordered(bordered) {};
	explicit Window(std::weak_ptr<Window> parent, Screen<screen_type> screen, bool bordered = false) : parent(parent), screen(screen), bordered(bordered) {};
	explicit Window(std::weak_ptr<Window> parent, screen_type x, screen_type y, screen_type width, screen_type height) : parent(parent), screen(x, y, width, height) {};
	virtual ~Window() {};

	virtual void add_subwindow(const std::shared_ptr<Window> window) = 0;
	virtual const std::shared_ptr<Window> get_subwindow(size_t index) = 0;

	virtual void set_screen(Screen<screen_type> screen) = 0;
	virtual const Screen<screen_type>& get_screen() const = 0;

	virtual void clear() const = 0;
	virtual void refresh() const = 0;
	virtual void resize() = 0;
	virtual void move() = 0;
	virtual void draw_border() const = 0;
	virtual void draw_text(screen_type x, screen_type y, const std::string& text) = 0;
	virtual void draw_text(screen_type x, screen_type y, std::string text) = 0;
	virtual int getchar() = 0;
	virtual void set_cursor(long x, long y) = 0;
	virtual std::pair<long, long> get_cursor() = 0;
protected:
	std::weak_ptr<Window> parent;
	Screen<screen_type> screen;
	bool bordered = false;
	std::vector<std::shared_ptr<Window>> subwindows;
};

