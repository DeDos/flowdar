/**
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


 * @file CursesWindow.h
 * @brief Curses Window
 * @author Henry Zhu
 * @version 0.01
 * @date 2018-05-18
 */

#pragma once

#include <curses.h>

#include <cmath>

#include "window.h"

//Custom unique ptr used to deallocate pointer to curses window
template<typename T>
using custom_unique_ptr = std::unique_ptr<T, std::function<void(T*)>>;

class CursesWindow : public Window
{
public:
	explicit CursesWindow(Screen<screen_type> screen, bool bordered = false);
	explicit CursesWindow(std::weak_ptr<Window> parent, Screen<screen_type> screen, bool bordered = false);
	explicit CursesWindow(std::weak_ptr<Window> parent, screen_type x, screen_type y, screen_type width, screen_type height);
	virtual ~CursesWindow();
	CursesWindow(const CursesWindow& other) = default;
	CursesWindow& operator=(const CursesWindow& other) = default;
	CursesWindow(CursesWindow&& other) = default;
	CursesWindow& operator=(CursesWindow&& other) = default;

	virtual void add_subwindow(const std::shared_ptr<Window> window) override;
	virtual const std::shared_ptr<Window> get_subwindow(size_t index) override;
	
	virtual void set_screen(Screen<screen_type> screen) override;
	virtual const Screen<screen_type>& get_screen() const override;
	
	virtual void clear() const override;
	virtual void refresh() const override;
	virtual void outrefresh() const;
	virtual void resize() override;
	virtual void move() override;
	virtual void draw_border() const override;

	virtual void draw_text(screen_type x, screen_type y, const std::string& text) override;
	virtual void draw_text(screen_type x, screen_type y, std::string text) override;
	virtual int getchar() override;
	virtual void set_cursor(long x, long y) override;
	virtual std::pair<long, long> get_cursor() override;

	//wrap curses printf to window
	template<typename... Args>
	bool print_text(Args... args)
	{
		//check if screen is filled
		if(get_cursor().second >= get_screen().height)
		{
			return false;
		}
		
		//print text
		wprintw(window.get(), args...);
		return true;
	}

	void set_attribute_on(int attribute);
	void set_attribute_off(int attribute);
private:
	custom_unique_ptr<WINDOW> window;
	double percent_x_of_parent = 0.0f;
	double percent_y_of_parent = 0.0f;
	double percent_width_of_parent = 0.0f;
	double percent_height_of_parent = 0.0f;
};

