/*
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#include "trace_util.h"

/**
 * @brief Subtracts a value from an unsigned value and checks if underflow happens
 *
 * @tparam T Value to be subtracted from's type 
 * @tparam OtherT Value to subtract's type 
 * @param val Value to subtract from
 * @param val_to_sub Value to subtract
 *
 * @return 
 */

namespace TraceUtil
{
	void SubZeroIfNegative(int& value, int val_to_sub)
	{
		auto new_val = value - val_to_sub;
		if(new_val < 0)
		{
			value = 0;
		}
		else
		{
			value = new_val;
		}
	}

	/**
	 * @brief Finds the function that is called by the caller (the caller in this case is the function that wraps around the other function)
	 *
	 * @param trace_element
	 *
	 * @return 
	 */
	const std::string& FindCalledFunction(const NginxTracer::TraceElement& trace_element) 
	{
		if(trace_element.get_function_type() == NginxTracer::FunctionType::Call)
		{
			return trace_element.get_callee();
		}
		return trace_element.get_caller();
	}
	
	/**
	 * @brief Finds the function that is not called by the caller
	 *
	 * @param trace_element
	 *
	 * @return 
	 */
	const std::string& FindNotCalledFunction(const NginxTracer::TraceElement& trace_element) 
	{
		if(trace_element.get_function_type() == NginxTracer::FunctionType::Call)
		{
		       	return trace_element.get_caller();
		}
		return trace_element.get_callee();

	}

	const std::vector<std::string> SplitByDelimiter(const std::string& str, std::string&& delimiter) 
	{
		std::vector<std::string> results;
		size_t start = 0;
		size_t end   = 0;
		while ((end = str.find(delimiter, start)) != std::string::npos)
		{
			//push back the split string
			results.push_back(str.substr(start, end - start));

			start = end + delimiter.length();
		}
		//push last result back
		results.push_back(str.substr(start));

		return results;
	}
			
	//read the source file
	const std::vector<std::string> ReadSourceFile(const std::string& filename)
	{
		std::ifstream fs(filename);
		std::vector<std::string> lines;
		std::string line;
		while(std::getline(fs, line))
		{
			lines.emplace_back(line);
		}
		return lines;
	}
}
