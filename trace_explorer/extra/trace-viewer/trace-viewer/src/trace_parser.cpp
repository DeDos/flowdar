/**
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


 * @file StackTracer.cpp
 * @brief Main file
 * @author Henry Zhu
 * @version 0.01
 * @date 2018-05-18
 */

#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendAction.h"
#include "clang/Tooling/Tooling.h"
#include "clang/Tooling/CommonOptionsParser.h"

#include "tracer.h"
#include "screen_ui.h"
#include "function_visitor.h"

//clang
static llvm::cl::OptionCategory tool_category("categories");
static llvm::cl::opt<std::string> cli_trace_filename("tracefile", llvm::cl::desc("Specify a tracefile to read from"), llvm::cl::cat(tool_category));

//default trace file to look for if "tracefile" is not set
static std::string trace_filename = "trace_resolved.txt";

/**
 * @brief Runs anaylsis on the source code to find function calls
 *
 * @param argc Number of arguments passed into main
 * @param argv Arguments passed into main
 */

void RunClangAnalysis(int argc, const char* argv[])
{
	clang::tooling::CommonOptionsParser options_parser(argc, argv, tool_category);
	clang::tooling::ClangTool tool(options_parser.getCompilations(),
			options_parser.getSourcePathList());

	tool.run(clang::tooling::newFrontendActionFactory<FunctionVisitorAction>().get());
}

/**
 * @brief Runs the tracer and returns a vector of trace elements corresponding to each line in the trace
 *
 * @return trace elements
 */
const std::vector<NginxTracer::TraceElement> RunTracer()
{
	//resolve trace filename if there is one
	if(!cli_trace_filename.empty())
	{
		trace_filename = cli_trace_filename;
	}

	auto tracer = NginxTracer::Tracer::get_tracer();
	return tracer.parse_lines(trace_filename);
}

/**
 * @brief Setups a window manager with three windows - 1) borders 2)stack 3) source
 *
 * @return Window manager
 */
std::unique_ptr<WindowManager> RunWindowManager()
{
	std::unique_ptr<WindowManager> wm = std::make_unique<CursesWindowManager>();
	wm->init();

	//create a bordered outside window
	auto screen = wm->get_screen();
	wm->create_window(Screen<screen_type>(0, 0, screen.width, screen.height), true);

	//make sure the stack and source window dimensions are smaller, so it fits into the outside border window
	screen_type width = (screen.width - 2)/2;
	screen_type height = screen.height - 2;

	//create two windows that are the stack and source window
	wm->create_window(Screen<screen_type>{1, 1, width, height});
	wm->create_window(Screen<screen_type>{width + 3, 1, width - 2, height});

	wm->refresh();

	return wm;
}

int main(int argc, const char* argv[])
{
	RunClangAnalysis(argc, argv);

	auto trace_elements = RunTracer();

	auto wm = RunWindowManager();

	ScreenUI screen_ui(std::move(wm), trace_elements);

	while(true)
	{
		screen_ui.refresh_screen();
		screen_ui.draw_source_screen();
		screen_ui.draw_stack_screen();
		screen_ui.fetch_and_process_keyboard_press();
		if(screen_ui.get_screen_state() == ScreenState::Shutdown)
		{
			break;
		}
	}

	return 0;
}

