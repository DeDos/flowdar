/**
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


 * @file ScreenUI.cpp
 * @brief Implementation of ScreenUI
 * @author Henry Zhu
 * @version 0.01
 * @date 2018-05-18
 */
#include "screen_ui.h"

//text color configurations
#define RETURN_PAIR 1
#define HISTORY_ONE_PAIR 2
#define HISTORY_TWO_PAIR 3
#define HISTORY_THREE_PAIR 4
#define HISTORY_FOUR_PAIR 5
#define HISTORY_FIVE_PAIR 6
#define HIGHLIGHT_PAIR 7
#define CALLEE_CALLER_PAIR 8
#define ARG_PAIR 9
#define FUNCTION_DEF_PAIR 10
#define TITLE_WINDOW_PAIR 11
#define TITLE_ARG_PAIR 12
#define TITLE_RETURN_PAIR 13

//number of lines to see before the caller function
static int num_backlines = 10;

//number of entires in history to select
static size_t history_cap = 20;

//how much to align the left argument's name when printing
static size_t left_argument_alignment = 20;

static int prev_traceline_num ;

void InitColors()
{
	init_pair(RETURN_PAIR, COLOR_BLACK, COLOR_RED);
	init_pair(HISTORY_ONE_PAIR, COLOR_BLACK, COLOR_GREEN);
	init_pair(HISTORY_TWO_PAIR, COLOR_BLACK, COLOR_YELLOW);
	init_pair(HISTORY_THREE_PAIR, COLOR_BLACK, COLOR_BLUE);
	init_pair(HISTORY_FOUR_PAIR, COLOR_BLACK, COLOR_MAGENTA);
	init_pair(HISTORY_FIVE_PAIR, COLOR_BLACK, COLOR_CYAN);
	init_pair(CALLEE_CALLER_PAIR, COLOR_BLACK, COLOR_YELLOW);
	init_pair(HIGHLIGHT_PAIR, COLOR_BLACK, COLOR_GREEN);
	init_pair(ARG_PAIR, COLOR_BLACK, COLOR_WHITE);
	init_pair(FUNCTION_DEF_PAIR, COLOR_YELLOW, COLOR_BLACK);
	init_pair(TITLE_WINDOW_PAIR, COLOR_BLACK, COLOR_WHITE);
	init_pair(TITLE_RETURN_PAIR, COLOR_CYAN, COLOR_BLACK);
	init_pair(TITLE_ARG_PAIR, COLOR_CYAN, COLOR_BLACK);
}

/**
 * @brief ScreenUI constructor
 *
 * @param wm_ window manager
 * @param trace_elements_ trace elements
 */
ScreenUI::ScreenUI(std::unique_ptr<WindowManager> wm_, const std::vector<NginxTracer::TraceElement>& trace_elements_) : wm(std::move(wm_)), trace_elements(trace_elements_), cur_filename(std::string{}), cur_lines(std::vector<std::string>{}), screen_state(ScreenState::Trace)
{
	stack_window = std::static_pointer_cast<CursesWindow>(wm->get_window(1));
	source_window = std::static_pointer_cast<CursesWindow>(wm->get_window(2));
	InitColors();
	trace_stacks.push_back(std::vector<std::string>());
	history_cap = stack_window->get_screen().height - 3;
}

/**
 * @brief The implementation for drawing a container on screen through a number of colors
 *
 * @tparam PrintCallback Function that takes in (window, element of container)
 * @tparam Iter Iterator of container
 * @param window Window to draw on
 * @param index_pair Keeps track of the color index we are on
 * @param start_pair Start index for color
 * @param end_pair End index for color
 * @param print_callback Callback that gets called when the container element is ready to be drawn
 * @param begin Container beginnning
 * @param end Container end
 */
template<typename PrintCallback, typename Iter>
void DrawColorRangeImpl(std::shared_ptr<CursesWindow> window, int index_pair, int start_pair, int end_pair, PrintCallback print_callback, Iter begin, Iter end)
{
	std::for_each(begin, end, [&](const auto& element)
		{
			window->set_attribute_on(COLOR_PAIR(index_pair));
			
			if(print_callback(window, element)) { return; }
			
			window->set_attribute_off(COLOR_PAIR(index_pair));
			
			if(++index_pair > end_pair)
			{
				index_pair = start_pair;
			}
		}
	);
}

/**
 * @brief Wrapper for DrawColorRangeImpl
 *
 * @param SetupFunc Setup function that takes in (start_pair, end_pair)
 * @param PrintCallback Callback for printing element to screen
 * @param Iter Iterator of container
 * @param window Window to draw on
 * @param setup_func Function that sets the color pair indices
 * @param print_callback Callback that gets called when the container element is ready to be drawn
 * @param begin Container beginnning
 * @param end Container end
 */
template <typename SetupFunc, typename PrintCallback, typename Iter>
void DrawColorRange(std::shared_ptr<CursesWindow> window, SetupFunc setup_func, PrintCallback print_callback, Iter begin, Iter end) 
{
	int start_pair = 0;
	int end_pair = 0;
	setup_func(start_pair, end_pair);
	DrawColorRangeImpl(window, start_pair, start_pair, end_pair, print_callback, begin, end);
}

const ScreenState& ScreenUI::get_screen_state() const
{
	return screen_state;
}

void ScreenUI::go_to_line(int go_to_linenum)
{
	while(trace_linenum < go_to_linenum)
	{
		key_pressed = KEY_DOWN;
		
		trace_linenum++;
		
		//update the stack/history
		fetch_next_screen_state(function_visitor->get_function_decls(), true);
		
	}
	while(trace_linenum > go_to_linenum)
	{
		key_pressed = KEY_UP;
		
		TraceUtil::SubZeroIfNegative(trace_linenum, 1);
		
		//update the stack/history
		fetch_next_screen_state(function_visitor->get_function_decls(), true);
	}
}

bool ScreenUI::fetch_next_screen_state(const std::map<std::string, FunctionASTNode>& function_nodes, bool force_update = false)
{
	//clear history if at beginning
	if(trace_linenum == 0) { history.clear(); }

	//check bounds
	if(static_cast<size_t>(trace_linenum) > trace_elements.size() - 1)
	{ 
		trace_linenum = trace_elements.size() - 1;
	}

	//check if we need to not update state (invalid means in the next call to process key presses, the screen will be drawn again since we are in an invalid state)
	if(screen_state == ScreenState::Invalid)
	{
		return false;
	}
	if(!screen_updated || force_update)
	{
		//going forward in trace
		auto forward_direction = key_pressed == KEY_DOWN || key_pressed == KEY_RIGHT;
		auto backward_direction = key_pressed == KEY_UP || key_pressed == KEY_LEFT;
		
		trace_element = &trace_elements[trace_linenum];

		auto FindFunctionNode = [&function_nodes](const auto& function_name)
			{
				auto function_node_itr = function_nodes.find(function_name);
				if(function_node_itr != std::end(function_nodes))
				{
					return function_node_itr;
				}
				return std::end(function_nodes);
			};

		if(!trace_element->get_is_invalid())
		{
			callee_node = FindFunctionNode(trace_element->get_callee()); 
			caller_node = FindFunctionNode(trace_element->get_caller()); 
		}
		else
		{
			callee_node = std::end(function_nodes);
			caller_node = std::end(function_nodes);
		}
		
		function_node = trace_element->get_function_type() == NginxTracer::FunctionType::Call ? callee_node : caller_node;

		//append/pop from history
		if(history.size() > history_cap)
		{
			if(!history.empty())
			{
				history.pop_back();
			}
		}
		if(forward_direction)
		{
			//check that we already have the previous last element on the stack
			if(!(static_cast<size_t>(trace_linenum) == trace_elements.size() - 1 && trace_element->get_callee() == history[0].second))
			{
				history.push_front({trace_element->get_function_type(), trace_element->get_callee()});
			}
		}
		else if(backward_direction)
		{
			//pop if moving backwards
			if(!history.empty())
			{
				history.pop_front();
			}
		}
		
		if(forward_direction)
		{
			if(static_cast<size_t>(trace_linenum) >= trace_stacks.size())
			{
				if(trace_element->get_function_type() == NginxTracer::FunctionType::Call)
				{
					//format the string
					std::stringstream ss;
					ss << std::left << std::setw(left_argument_alignment) << "Line: " + std::to_string(trace_linenum);
					
					//push onto stack
					trace_stack.push_back(ss.str() + trace_element->get_callee());
				}
				else 
				{
					if(!trace_stack.empty())
					{
						trace_stack.pop_back();
						prev_traceline_num = trace_linenum;
					}
				}
				trace_stacks.push_back(trace_stack);
			}
		}

		screen_updated = true;
	}

	return true;
}

/**
 * @brief Fetches a keyboard press
 */
void ScreenUI::fetch_keyboard_press()
{
	source_window->refresh();
	key_pressed = stack_window->getchar();
}

/**
 * @brief Processes a keyboard press and then updates internal state
 */
void ScreenUI::fetch_and_process_keyboard_press()
{
	//check screen state
	if(screen_state == ScreenState::Invalid)
	{
		//put us back into a state that is valid (basically a refresh to the screen and state)
		screen_state = ScreenState::Trace;
		key_pressed = '=';
	}
	else
	{
		fetch_keyboard_press();
	}
	
	switch(key_pressed)
	{
		case KEY_UP:
			{
				switch(screen_state)
				{
				case ScreenState::Trace:
				{
					TraceUtil::SubZeroIfNegative(trace_linenum, 1);
					break;
				}
				case ScreenState::ViewSource:
				{
					TraceUtil::SubZeroIfNegative(cur_fileline, 1);
					break;
				}
				default:
					break;
				}
				break;
			}
		case KEY_DOWN:
			{
				switch(screen_state)
				{
				case ScreenState::Trace:
				{
					if(static_cast<size_t>(trace_linenum) == trace_elements.size() - 1)
					{
						break;
					}
					trace_linenum++;
					break;
				}
				case ScreenState::ViewSource:
				{
					if(static_cast<size_t>(cur_fileline) == cur_lines.size() - 1)
					{
						break;
					}
					cur_fileline++;
					break;
				}
				default:
					break;
				}
				break;
			}
		case KEY_LEFT:
			{
				for(auto i = 0; i < num_backlines; i++)
				{
					TraceUtil::SubZeroIfNegative(trace_linenum, 1);
					fetch_next_screen_state(function_visitor->get_function_decls(), true);
				}
				//invalidate the screen, so we can be put back in a valid state in fetch_and_process_key_board_press 
				screen_state = ScreenState::Invalid;
				break;
			}
		case KEY_RIGHT:
			{
				for(auto i = 0; i < num_backlines; i++)
				{
					trace_linenum++;
					fetch_next_screen_state(function_visitor->get_function_decls(), true);
				}
				//invalidate the screen, so we can be put back in a valid state in fetch_and_process_key_board_press 
				screen_state = ScreenState::Invalid;
				break;
			}
		case 'v':
			{
				//fork
				const char* editor_raw = std::getenv("EDITOR");
				std::string editor;
				if(editor_raw == nullptr)
				{
					//default to our custom editor
					screen_state = screen_state == ScreenState::Trace ? ScreenState::ViewSource : ScreenState::Trace;
					break;
				}
				else
				{
					editor = std::string{editor_raw};
				}

				pid_t editor_pid = fork();
				
				if(editor_pid == -1)
				{
					std::cerr << strerror(errno) << std::endl;
					exit(-1);
				}
				else if(!editor_pid)
				{
					//open whatever editor they have
					execlp(editor.c_str(), editor.c_str(), cur_filename.c_str(), (char*)NULL);
				}
				else
				{
					int status = 0;
					waitpid(editor_pid, &status, 0);
				}
				
				//invalidate the screen, so we can be put back in a valid state in fetch_and_process_key_board_press 
				screen_state = ScreenState::Invalid;
				keypad(stdscr, TRUE);

				break;
			}
		case ' ':
			{
				stack_window_minimized = !stack_window_minimized;
				
				//invalidate the screen, so we can be put back in a valid state in fetch_and_process_key_board_press 
				screen_state = ScreenState::Invalid;
				break;
			}
		case 'g':
			{
				std::string input; 

				//fetch keys until user presses enter (numpad or enter)
				while(true)
				{
					refresh_screen();
					stack_window->set_cursor(0, 0);
					stack_window->print_text("Enter line number: %s\n", input.c_str());
					fetch_keyboard_press();

					if(key_pressed == KEY_ENTER || key_pressed == '\n')
					{
						break;
					}
					if(key_pressed == KEY_BACKSPACE)
					{
						if(!input.empty())
						{
							input.pop_back();
						}
						continue;
					}

					input.push_back(key_pressed);
				}

				//location where value has a digit
				size_t pos = 0;

				auto PrintErrorResponseMessage = [this, &input]()
				{
					stack_window->print_text("Line number: %s is not valid\nPress any key to continue", input.c_str());
					stack_window->getchar();
				};

				//check if the input is valid (we don't have exceptions enabled, so throwing an exception means that the program will die)
				//https://stackoverflow.com/questions/4654636/how-to-determine-if-a-string-is-a-number-with-c
				if(input.empty() || input.find_first_not_of("0123456789") != std::string::npos || input.size() > sizeof(long))
				{
					PrintErrorResponseMessage();
					break;
				}

				//convert to string
				auto line_to_go_to = std::stol(input, &pos);

				//check if valid string 
				if(pos && static_cast<size_t>(line_to_go_to) < trace_elements.size())
				{
					go_to_line(line_to_go_to);
				}
				else
				{
					PrintErrorResponseMessage();
				}
				
				//invalidate the screen, so we can be put back in a valid state in fetch_and_process_key_board_press 
				screen_state = ScreenState::Invalid;
				break;
			}
		case 'q':
			{
				screen_state = ScreenState::Shutdown;
				break;
			}
		default:
			{
				break;
			}
	}
	
	//screen can now be drawn
	screen_updated = false;
}

/**
 * @brief Clears and redraws the scereen
 */
void ScreenUI::refresh_screen() const
{
	wm->refresh();

	stack_window->clear();
	source_window->clear();

	//draw titles
	stack_window->set_cursor(0, 0);
	source_window->set_cursor(0, 0);
}

void DrawSourceCode(std::shared_ptr<CursesWindow> window, const std::vector<std::string>& lines, int called_function_linenum)
{

	//check if out of bounds (beyond the number of lines in source)
	for(auto current_source_function_linenum = called_function_linenum - num_backlines; static_cast<size_t>(current_source_function_linenum) < lines.size(); current_source_function_linenum++)
	{
		//check if before bounds (negative line number)
		if(current_source_function_linenum < 0)
		{
			current_source_function_linenum = 0;
		}

		//check if we need to highlight the line we want the user to see
		if(current_source_function_linenum == called_function_linenum)
		{
			window->set_attribute_on(COLOR_PAIR(HIGHLIGHT_PAIR));
			if(!window->print_text("%d %s\n", current_source_function_linenum, lines[current_source_function_linenum].c_str())) { break; }
			window->set_attribute_off(COLOR_PAIR(HIGHLIGHT_PAIR));
			continue;
		}

		//draw the line from source onto screen
		if(!window->print_text("%d %s\n", current_source_function_linenum, lines[current_source_function_linenum].c_str())) { break; }
		
		//check if we have printed over the height of screen
		if(window->get_cursor().second == window->get_screen().height - 1) { break; }
	}
}



/**
 * @brief Draws the stack screen
 */
void ScreenUI::draw_stack_screen() 
{
	switch(screen_state)
	{
	case ScreenState::Trace:
	{
		//Draw title
		stack_window->set_attribute_on(COLOR_PAIR(TITLE_WINDOW_PAIR));
		stack_window->print_text("%s\n", "Detailed Stack");
		stack_window->set_attribute_off(COLOR_PAIR(TITLE_WINDOW_PAIR));
		
		stack_window->print_text("Current trace line: %d Max: %lu\n", trace_linenum, trace_elements.size());

		const auto& function_nodes = function_visitor->get_function_decls();
		if(!fetch_next_screen_state(function_nodes))
		{
			break;
		}

		const auto& function_definition = function_node->second.getLiteralString();

		//should change to somewhere else
		if(stack_window_minimized)
		{
			auto tabs = 0;
		
			//find if is just used to iterate through history deque iterators
			std::find_if(std::rbegin(history), std::rend(history), [this, &tabs](const auto& history_element)
				{
					const auto type = history_element.first;
					const auto function_name = history_element.second;
					
					//default to calling
					auto function_type_format = "==>";

					//move spaces based how deep call stack is
					if(type == NginxTracer::FunctionType::Call)
					{
						tabs++;
					}
					else
					{
						function_type_format = "<==";
					}
					
					
					auto formatted_function_name = std::string(tabs * 2, ' ') + function_type_format + " " + function_name;
				
					auto arg_color = (HISTORY_FIVE_PAIR - HISTORY_ONE_PAIR) + HISTORY_ONE_PAIR; 
					stack_window->set_attribute_on(COLOR_PAIR(tabs % arg_color));
					auto overflow_screen = stack_window->print_text("%s\n", formatted_function_name.c_str());
					stack_window->set_attribute_off(COLOR_PAIR(tabs % arg_color));

					if(type == NginxTracer::FunctionType::Return)
					{
						if(--tabs < 0)
						{
							tabs = 0;
						}
					}

					//stop based on whether or not we can fit the screen with elements	
					return !overflow_screen;
				}
			);
		}
		else
		{
			//check if function exists
			if(!callee_node->first.empty())
			{
				stack_window->set_attribute_on(COLOR_PAIR(CALLEE_CALLER_PAIR));

				stack_window->print_text("\n%s %s %s\n\n",
						trace_element->get_caller().c_str(),
						trace_element->get_function_type() == NginxTracer::FunctionType::Call ? ">" : "<",
						trace_element->get_callee().c_str()
						);
				
				stack_window->set_attribute_off(COLOR_PAIR(CALLEE_CALLER_PAIR));
				
				auto PrintFunctionFilename = [this] (const auto& function, const auto& filename, auto file_linenum)
				{
					std::stringstream ss;
					ss << std::left << std::setw(left_argument_alignment) << function;
					stack_window->print_text("%s (%s %d)\n",
							ss.str().c_str(), 
							filename.c_str(), 
							file_linenum
							);
				};
				
				//draw caller
				if(caller_node != std::end(function_nodes))
				{
					PrintFunctionFilename(trace_element->get_caller(), caller_node->second.getFilename(), trace_element->get_caller_linenum());
				}

				//draw callee
				if(callee_node != std::end(function_nodes))
				{
					PrintFunctionFilename(trace_element->get_callee(), callee_node->second.getFilename(), trace_element->get_callee_linenum());
				}

				//draw func definition
				stack_window->set_attribute_on(COLOR_PAIR(FUNCTION_DEF_PAIR));
				stack_window->print_text("\n%s\n\n",
						function_definition.c_str()
						);
				stack_window->set_attribute_off(COLOR_PAIR(FUNCTION_DEF_PAIR));

				//draw return value only if function is a return
				if(trace_element->get_function_type() == NginxTracer::FunctionType::Return)
				{
					//find function name -- ex uint function_name(... this will find end of function_name(
					auto function_name_itr = function_definition.find(function_node->first);
					if(function_name_itr == std::string::npos) { return; }

					//extract the return value
					auto return_value_line = function_definition.substr(0, function_name_itr - 1);
					
					stack_window->set_attribute_on(COLOR_PAIR(TITLE_RETURN_PAIR));
					stack_window->print_text("Return value:\n");
					stack_window->set_attribute_off(COLOR_PAIR(TITLE_RETURN_PAIR));
				
					//format the string
					std::stringstream ss;
					ss << std::left << std::setw(left_argument_alignment) << return_value_line;
					stack_window->print_text("%s %s\n\n", ss.str().c_str(), trace_element->get_return_value().c_str());
				}
				//this is a call, so print out the arguments
				else
				{
					//find each function argument	
					auto argument_line = function_definition.substr(function_definition.find("(") + 1, function_definition.rfind(")") - function_definition.find("(") - 1);
					auto arguments = TraceUtil::SplitByDelimiter(argument_line, ", ");

					stack_window->set_attribute_on(COLOR_PAIR(TITLE_ARG_PAIR));
					stack_window->print_text("Arguments:\n");
					stack_window->set_attribute_off(COLOR_PAIR(TITLE_ARG_PAIR));
					
					//draw arguments
					auto i = NginxTracer::MIN_ARG_INDEX;
					for(const auto& arg : arguments)
					{
						//make sure argument fetched is in range
						auto arg_no = std::min(static_cast<size_t>(i), NginxTracer::MAX_ARG_INDEX);

						//format the string
						std::stringstream ss;
						ss.str(std::string{});
						ss.clear();
						ss << std::left << std::setw(left_argument_alignment) << arg;

						stack_window->print_text("%s %s\n", ss.str().c_str(), trace_element->get_arg(arg_no).c_str());
						i++;
					}
				}

				//print trace stack
				const auto& current_trace_stack = trace_stacks[trace_linenum];
				if(!current_trace_stack.empty())
				{
					auto height = stack_window->get_screen().height - 1;
					
					//get an iterator of last num_backlines items of stack
					auto begin_current_trace_stack_itr = std::end(current_trace_stack) - num_backlines < std::begin(current_trace_stack) ? std::begin(current_trace_stack) : std::end(current_trace_stack) - num_backlines;
					std::for_each(begin_current_trace_stack_itr, std::end(current_trace_stack) - 1, [&](const std::string& element)
						{
							//set cursor left side
							stack_window->set_cursor(0, height--);
							
							//draw element
							stack_window->print_text("%s", element.c_str());
						}
					);

					//highlight top of stack
					auto element = current_trace_stack[current_trace_stack.size() - 1];
					stack_window->set_cursor(0, height);
					stack_window->set_attribute_on(COLOR_PAIR(HIGHLIGHT_PAIR));
					stack_window->print_text("%s\n", element.c_str());
					stack_window->set_attribute_off(COLOR_PAIR(HIGHLIGHT_PAIR));

					//draw stack size of top
					stack_window->set_cursor(0, --height);
					stack_window->print_text("Stack size: %lu\n", current_trace_stack.size());
				}
			}
		}
		break;
	}
	case ScreenState::ViewSource:
	{
		DrawSourceCode(stack_window, cur_lines, cur_fileline);
		break;
	}
	default:
		break;
	}
}

/**
 * @brief Draws the source screen
 */
void ScreenUI::draw_source_screen() 
{
	switch(screen_state)
	{
	case ScreenState::Trace:
	{
		//draw title
		source_window->set_attribute_on(COLOR_PAIR(TITLE_WINDOW_PAIR));
		source_window->print_text("%s\n", "Source");
		source_window->set_attribute_off(COLOR_PAIR(TITLE_WINDOW_PAIR));
		
		//is > calling or < returning function
		const auto& function_nodes = function_visitor->get_function_decls();
		if(!fetch_next_screen_state(function_nodes))
		{
			return;
		}

		//find the function to actually draw
		//not called function is the function that is not being called (ie, the function that is wrapping around or calling the other function. So, for example, if the current traceline is a call, then we need to get the function definition for the caller, else if the current traceline is a return, we fetch the callee)
		auto const& not_called_function_name = TraceUtil::FindNotCalledFunction(*trace_element);
		auto not_called_function_name_decl = function_nodes.find(not_called_function_name);
		if(not_called_function_name_decl != function_nodes.end())
		{
			//print filename
			source_window->print_text("File: %s\n", not_called_function_name_decl->second.getFilename().c_str()); 

			auto function_definition_linenum = not_called_function_name_decl->second.getLineNum();
			cur_filename = not_called_function_name_decl->second.getFilename();
			cur_lines = TraceUtil::ReadSourceFile(cur_filename);

			//starts at the callee's function body and starts to search in the body until the callee line number is found
			auto FindSourceLineNumber =  [this, &function_definition_linenum](const std::string& function_name_to_find) 
			{
				auto result_itr = std::find_if(std::begin(cur_lines) + function_definition_linenum, std::end(cur_lines), [&](std::string line) { return line.find(function_name_to_find) != std::string::npos; });
				if(result_itr != std::end(cur_lines))
				{
					return result_itr - std::begin(cur_lines);
				}
				return 0L;
			};

			//get the function we want to highlight line number	
			cur_fileline = FindSourceLineNumber(not_called_function_name);
			
			DrawSourceCode(source_window, cur_lines, cur_fileline);
		}
		else
		{
			source_window->print_text("%s\n", "Could not find trace function"); 
			source_window->print_text("%s%s\n", "Callee: ", trace_elements[trace_linenum].get_callee().c_str());
			source_window->print_text("%s%s\n", "Caller: ", trace_elements[trace_linenum].get_caller().c_str());
			source_window->print_text("Trying to find %s\n", not_called_function_name.c_str());
		}
		break;
	}
	case ScreenState::ViewSource:
	{
		break;
	}
	default:
		break;
	}
}


