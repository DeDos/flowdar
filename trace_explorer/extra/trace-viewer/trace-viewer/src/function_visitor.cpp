/**
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


 * @file FunctionVisitor.cpp
 * @brief Implementation of FunctionVisitor
 * @author Henry Zhu
 * @version 0.01
 * @date 2018-05-18
 */
#include "function_visitor.h"

using Base = clang::RecursiveASTVisitor<FunctionVisitor>;

/*
 * Static variable initializations
 */
std::map<std::string, FunctionASTNode> FunctionVisitor::functions{};
std::unique_ptr<FunctionVisitor> function_visitor(nullptr);

/**
 * @brief ASTVisitor feeds us a function when it encounters one in the ast tree 
 *
 * @param function_decl function declaration (node that was found)
 * @return Stop iterating through the AST
 */
bool FunctionVisitor::VisitFunctionDecl(clang::FunctionDecl* function_decl)
{
	//check if is definition
	if(function_decl->hasBody()) 
	{
		auto function_name = function_decl->getName();

		//insert to map
		auto ast_node = ASTNode<clang::FunctionDecl>{function_decl, context->getSourceManager()};
		functions.insert({function_name, std::move(ast_node)});
	}
	return Base::VisitFunctionDecl(function_decl);
}

/**
 * @brief Returns a map containing {function declaration name, function node}
 *
 * @return reference to a map containing {function declaration name, function node}
 */
const std::map<std::string, FunctionASTNode>& FunctionVisitor::get_function_decls() 
{
	return functions;
}

/**
 * @brief Returns the context of the visitor 
 *
 * @return context containing state of the visitor
 */
clang::ASTContext* FunctionVisitor::get_context() const
{
	return context;
}

