/**
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


 * @file ASTNode.h
 * @brief 
 * @author Henry Zhu
 * @version 0.01
 * @date 2018-05-18
 */

#pragma once

#include <type_traits>
#include <string>
#include <algorithm>

#include "clang/Basic/SourceManager.h"
#include "clang/Frontend/TextDiagnostic.h"
#include "clang/Frontend/TextDiagnosticPrinter.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Lex/Lexer.h"

//https://github.com/include-what-you-use/include-what-you-use/blob/master/iwyu_ast_util.h
template<typename T>
class ASTNode
{
public:
	/**
	 * @brief ASTNode represents a small copy of the actual clang node. 
	 *
	 * @param node Clang node
	 * @param sm Source manager
	 */
	ASTNode(const T* node, const clang::SourceManager& sm) : literal_string(std::string{}), filename(std::string{}), node(node), sm(sm)
	{
		static_assert(std::is_base_of<clang::Decl, T>::value, "T needs to be of type clang::Decl");

		auto function_name = std::string(node->getName());
		if(!function_name.empty())
		{
			//Gather information about node
			clang::SourceLocation loc_begin(node->getLocStart());
			clang::SourceLocation loc_end(node->getLocEnd());

			//clang::SourceLocation eend(end_of_the_end(end, sm));
			literal_string = std::string{sm.getCharacterData(loc_begin), sm.getCharacterData(loc_end)};
			if(!literal_string.empty())
			{
				literal_string.erase(literal_string.find(")"));
				literal_string += ")";

				//keep everything on one line for function definition
				std::replace(std::begin(literal_string), std::end(literal_string), '\n', ' ');
						  
				//remove multuple white spaces
				literal_string.erase(std::unique(std::begin(literal_string), std::end(literal_string), [](const auto& lhs, const auto& rhs) { return std::isspace(lhs) && std::isspace(rhs) && lhs == rhs; } ), std::end(literal_string));
				line_num = sm.getExpansionLineNumber(loc_begin);
				col_num = sm.getExpansionColumnNumber(loc_begin);

				auto filename_ref = sm.getFilename(loc_begin);
				if(!filename_ref.empty())
				{
					filename = filename_ref.str();
				}
			}
		}
	}

	/**
	 * @brief Get line number
	 *
	 * @return line number
	 */
	unsigned int getLineNum() const
	{
		return line_num;
	}

	/**
	 * @brief Get Column number
	 *
	 * @return column number
	 */
	unsigned int getColNum() const
	{
		return col_num;
	}

	/**
	 * @brief Get internal clang node
	 *
	 * @return Internal clang node
	 * @warning Not guarantee to be valid
	 */
	const T* getClangNode() const
	{
		return node;
	}

	/**
	 * @brief Get node string
	 *
	 * @return Node string
	 */
	const std::string& getLiteralString() const
	{
		return literal_string;
	}
	
	/**
	 * @brief Get filename where the node resides
	 *
	 * @return Filename
	 */
	const std::string& getFilename() const
	{
		return filename;
	}
	
	/**
	 * @brief Get source manager
	 *
	 * @return Source manager
	 * @warning Most likely not valid
	 */
	const clang::SourceManager& getSourceManager() const
	{
		return sm;
	}
private:
	unsigned int line_num = 0;
	unsigned int col_num = 0;
	std::string literal_string;
	std::string filename;
	const T* node;
	const clang::SourceManager& sm;
};


