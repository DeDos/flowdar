/**
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


 * @file FunctionVisitor.h
 * @brief Function visitor handles visits the ast tree of the source code through clang's libtooling
 * @author Henry Zhu
 * @version 0.01
 * @date 2018-05-18
 */

#pragma once

#include <vector>
#include <string>
#include <map>

#include "ast_node.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/FrontendAction.h"

//forward declaration for function_visitor
class FunctionVisitor;

/**
 * @brief A global reference to the ast visitor
 * @warning This is breaking the design clang wanted -- you shouldn't have a reference to the internal state of the ast visitor, but we need this because we want to hold the state of the visitor
 */
//TODO look at ycm's state handler
//Can be less hacky by not having a refence to the ast visitor itself and just a reference to the function decls (programmer can't reference function_visitor directly)
extern std::unique_ptr<FunctionVisitor> function_visitor;

using FunctionASTNode = ASTNode<clang::FunctionDecl>;

class FunctionVisitor : public clang::RecursiveASTVisitor<FunctionVisitor>
{
using Base = clang::RecursiveASTVisitor<FunctionVisitor>;
public:
	explicit FunctionVisitor(clang::ASTContext* context) : context(context), textd(llvm::outs(), context->getLangOpts(), &context->getDiagnostics().getDiagnosticOptions()) {}
	bool VisitFunctionDecl(clang::FunctionDecl* function_decl);
	
	static const std::map<std::string, FunctionASTNode>& get_function_decls();
	clang::ASTContext* get_context() const;
private:
	clang::ASTContext* context;
	clang::TextDiagnostic textd;
	static std::map<std::string, FunctionASTNode> functions;
};

class FunctionVisitorConsumer : public clang::ASTConsumer
{
public:
	explicit FunctionVisitorConsumer(clang::ASTContext* context) 
	{ 
		function_visitor = std::make_unique<FunctionVisitor>(context);
	} 

	/**
	 * @brief Iterates over the nodes of the ast tree
	 */
	virtual bool HandleTopLevelDecl(clang::DeclGroupRef decl_group_ref) override
	{
		for(auto&& decl_ref : decl_group_ref)
		{
			function_visitor->TraverseDecl(decl_ref);
		}
		return true;
	}
};

class FunctionVisitorAction : public clang::ASTFrontendAction
{
public:
	virtual std::unique_ptr<clang::ASTConsumer> CreateASTConsumer(clang::CompilerInstance &Compiler, llvm::StringRef InFile) 
	{
		//ignore and suppress all warnings
		Compiler.getDiagnostics().setClient(new clang::IgnoringDiagConsumer());

		return std::make_unique<FunctionVisitorConsumer>(&Compiler.getASTContext());
	}
};
