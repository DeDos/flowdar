/**
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


 * @file ScreenUI.h
 * @brief ScreenUI represents what the user can interact with (key press, draw, etc)
 * @author Henry Zhu
 * @version 0.01
 * @date 2018-05-18
 */
#pragma once 

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>

#include <iomanip>
#include <iostream>
#include <memory>
#include <vector>
#include <algorithm>
#include <iterator>
#include <deque>
#include <cctype>

#include "trace_util.h"
#include "function_visitor.h"
#include "curses_window_manager.h"
#include "tracer.h"

enum class ScreenState
{
	Shutdown,
	Invalid,
	Trace,
	ViewSource
};

using HistoryElement = std::pair<NginxTracer::FunctionType, std::string>;

class ScreenUI
{
public:
	explicit ScreenUI(std::unique_ptr<WindowManager> wm_, const std::vector<NginxTracer::TraceElement>& trace_elements_);
	void fetch_and_process_keyboard_press();
	void refresh_screen() const;
	void draw_stack_screen();
	void draw_source_screen();
	const ScreenState& get_screen_state() const;
private:
	void go_to_line(int go_to_linenum);
	bool fetch_next_screen_state(const std::map<std::string, FunctionASTNode>& function_nodes, bool force_update);
	void fetch_keyboard_press();

	int key_pressed = 0;
	int trace_linenum = 0;

	//curses windows
	std::unique_ptr<WindowManager> wm;
       	std::vector<NginxTracer::TraceElement> trace_elements;
	std::shared_ptr<CursesWindow> stack_window;
	std::shared_ptr<CursesWindow> source_window;

	//there are individual trace stack for state in the trace file
	std::vector<std::string> trace_stack;
	std::vector<std::vector<std::string>> trace_stacks;

	//history is used to keep track of the calls/returns in the trace file
	std::deque<HistoryElement> history;

	std::string cur_filename;
	std::vector<std::string> cur_lines;
	int cur_fileline = 0;
	ScreenState screen_state;
	bool stack_window_minimized = false;
	
	//the current trace element from the trace file
	NginxTracer::TraceElement* trace_element;

	//These variables keep track of which callee/caller node that was fetched from the clang ast tree
	//Each time fetch_and_process_key_press is called, the callee/caller nodes are updated with the matching nodes to the trace callee/caller names
	//function node keeps track of which node to display on the the screen (ex, the function definition is fetched and its arguments are separated. This node references a callee/caller node depending on whether the current line in the trace is a call/return. For example, if the current line is call in the trace, the function_node would point to the callee_node to fetch its function definition, arguments, etc
	std::map<std::string, FunctionASTNode>::const_iterator callee_node;
	std::map<std::string, FunctionASTNode>::const_iterator caller_node;
	std::map<std::string, FunctionASTNode>::const_iterator function_node;

	//variables should not be updated when this is set to true
	bool screen_updated = false;
};

