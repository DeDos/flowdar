#!/bin/sh
#
#Copyright (c) 2019, University of Pennsylvania
#All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the <organization> nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
#DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


#find all c files

program_name="trace-viewer"

export TERM=xterm-color
if [ "$#" -eq 0 ]; then	
	C_FILES="$(find nginx/ -name "*.c")"
	H_FILES="$(find nginx/ -type d -exec echo "-I{}" \;)"
	./bin/$program_name $C_FILES -tracefile=trace_resolved.txt -- $H_FILES
elif [ "$#" -eq 1 ]; then
	C_FILES="$(find "$1"/ -name "*.c")"
	H_FILES="$(find "$1"/ -type d -exec echo "-I{}" \;)"
	./bin/$program_name $C_FILES -tracefile=trace_resolved.txt -- $H_FILES
elif [ "$#" -eq 2 ]; then
	C_FILES="$(find "$1"/ -name "*.c")"
	H_FILES="$(find "$1"/ -type d -exec echo "-I{}" \;)"
	./bin/$program_name $C_FILES -tracefile=$2 -- $H_FILES
#valgrind check	
elif [ "$#" -eq 4 ]; then
	#test
	valgrind --track-origins=yes -v --log-file=test.txt --leak-check=full ./bin/$program_name one.c two.c --
#gdb	
elif [ "$#" -eq 5 ]; then
	#debug
	C_FILES="$(find nginx/ -name "*.c")"
	H_FILES="$(find nginx/ -type d -exec echo "-I{}" \;)"
	gdb -ex=r --args ./bin/$program_name $C_FILES -- $H_FILES
else
	echo "Usage: ./ngnix_src_fetch_run.sh [directory to search for c files] [trace file]"
fi

