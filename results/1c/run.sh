# Apache 2.4.26, unpatched
# Place apache under slowloris attack for 50s

#!/bin/bash
APACHE_BUILD_PATH="/home/jinzihao/apache_build_env/bin"
TRACER_PATH="/home/jinzihao/chopchop/dyngraph/generation"
TARGETIP='localhost'
TARGETPORT='80'
CURRENTDIR=`pwd`

cp $APACHE_BUILD_PATH/bin/httpd ./
cd $APACHE_BUILD_PATH/bin/
sudo ./apachectl start

cd $CURRENTDIR
sudo ./slowloris.pl -dns $TARGETIP -port $TARGETPORT -timeout 30 -num 500 -tcpto 5 &
sleep 50
sudo pkill 'slowloris.pl'

sudo $TRACER_PATH/dwarf_utils/getlibaddr.sh httpd libapr-1 > apr_lib_addr.txt
sudo $TRACER_PATH/dwarf_utils/getlibaddr.sh httpd libaprutil-1 > aprutil_lib_addr.txt
sudo $TRACER_PATH/dwarf_utils/getlibaddr.sh httpd mod_alias > mod_alias_lib_addr.txt
sudo $TRACER_PATH/dwarf_utils/getlibaddr.sh httpd mod_dir > mod_dir_lib_addr.txt
sudo $TRACER_PATH/dwarf_utils/getlibaddr.sh httpd mod_autoindex > mod_autoindex_lib_addr.txt
sudo $TRACER_PATH/dwarf_utils/getlibaddr.sh httpd mod_status > mod_status_lib_addr.txt
sudo $TRACER_PATH/dwarf_utils/getlibaddr.sh httpd mod_unixd > mod_unixd_lib_addr.txt
sudo $TRACER_PATH/dwarf_utils/getlibaddr.sh httpd mod_version > mod_version_lib_addr.txt
sudo $TRACER_PATH/dwarf_utils/getlibaddr.sh httpd mod_setenvif > mod_setenvif_lib_addr.txt
sudo $TRACER_PATH/dwarf_utils/getlibaddr.sh httpd mod_headers > mod_headers_lib_addr.txt
sudo $TRACER_PATH/dwarf_utils/getlibaddr.sh httpd mod_env > mod_env_lib_addr.txt
sudo $TRACER_PATH/dwarf_utils/getlibaddr.sh httpd mod_log_config > mod_log_config_lib_addr.txt
sudo $TRACER_PATH/dwarf_utils/getlibaddr.sh httpd mod_mime > mod_mime_lib_addr.txt
sudo $TRACER_PATH/dwarf_utils/getlibaddr.sh httpd mod_filter > mod_filter_lib_addr.txt
sudo $TRACER_PATH/dwarf_utils/getlibaddr.sh httpd mod_reqtimeout > mod_reqtimeout_lib_addr.txt
sudo $TRACER_PATH/dwarf_utils/getlibaddr.sh httpd mod_auth_basic > mod_auth_basic_lib_addr.txt
sudo $TRACER_PATH/dwarf_utils/getlibaddr.sh httpd mod_access_compat > mod_access_compat_lib_addr.txt
sudo $TRACER_PATH/dwarf_utils/getlibaddr.sh httpd mod_authz_core > mod_authz_core_lib_addr.txt
sudo $TRACER_PATH/dwarf_utils/getlibaddr.sh httpd mod_authz_user > mod_authz_user_lib_addr.txt
sudo $TRACER_PATH/dwarf_utils/getlibaddr.sh httpd mod_authz_groupfile > mod_authz_groupfile_lib_addr.txt
sudo $TRACER_PATH/dwarf_utils/getlibaddr.sh httpd mod_authz_host > mod_authz_host_lib_addr.txt
sudo $TRACER_PATH/dwarf_utils/getlibaddr.sh httpd mod_authn_core > mod_authn_core_lib_addr.txt
sudo $TRACER_PATH/dwarf_utils/getlibaddr.sh httpd mod_authn_file > mod_authn_file_lib_addr.txt
#for id in `ps -A | grep httpd | awk '{print($1)}'`
#do
#	echo '<<< PID = '$id' >>>'
#	sudo cat "/proc/"$id"/maps" | grep 'r-xp'
#done
cd $APACHE_BUILD_PATH/bin/
#sudo ./apachectl stop # Commented out to avoid another instance of Apache starting
sudo pkill httpd
cd $CURRENTDIR
echo '#!/bin/bash' > resolver.sh
echo -n $TRACER_PATH'/dwarf_utils/function_resolver ' >> resolver.sh
echo $TRACER_PATH'/dwarf_utils/httpd_function_map.txt 0'' '$TRACER_PATH'/dwarf_utils/mod_alias_function_map.txt '`head -n 1 mod_alias_lib_addr.txt | awk '{print($2);}'`' '$TRACER_PATH'/dwarf_utils/mod_dir_function_map.txt '`head -n 1 mod_dir_lib_addr.txt | awk '{print($2);}'`' '$TRACER_PATH'/dwarf_utils/mod_autoindex_function_map.txt '`head -n 1 mod_autoindex_lib_addr.txt | awk '{print($2);}'`' '$TRACER_PATH'/dwarf_utils/mod_status_function_map.txt '`head -n 1 mod_status_lib_addr.txt | awk '{print($2);}'`' '$TRACER_PATH'/dwarf_utils/mod_unixd_function_map.txt '`head -n 1 mod_unixd_lib_addr.txt | awk '{print($2);}'`' '$TRACER_PATH'/dwarf_utils/mod_version_function_map.txt '`head -n 1 mod_version_lib_addr.txt | awk '{print($2);}'`' '$TRACER_PATH'/dwarf_utils/mod_setenvif_function_map.txt '`head -n 1 mod_setenvif_lib_addr.txt | awk '{print($2);}'`' '$TRACER_PATH'/dwarf_utils/mod_headers_function_map.txt '`head -n 1 mod_headers_lib_addr.txt | awk '{print($2);}'`' '$TRACER_PATH'/dwarf_utils/mod_env_function_map.txt '`head -n 1 mod_env_lib_addr.txt | awk '{print($2);}'`' '$TRACER_PATH'/dwarf_utils/mod_log_config_function_map.txt '`head -n 1 mod_log_config_lib_addr.txt | awk '{print($2);}'`' '$TRACER_PATH'/dwarf_utils/mod_mime_function_map.txt '`head -n 1 mod_mime_lib_addr.txt | awk '{print($2);}'`' '$TRACER_PATH'/dwarf_utils/mod_filter_function_map.txt '`head -n 1 mod_filter_lib_addr.txt | awk '{print($2);}'`' '$TRACER_PATH'/dwarf_utils/mod_reqtimeout_function_map.txt '`head -n 1 mod_reqtimeout_lib_addr.txt | awk '{print($2);}'`' '$TRACER_PATH'/dwarf_utils/mod_auth_basic_function_map.txt '`head -n 1 mod_auth_basic_lib_addr.txt | awk '{print($2);}'`' '$TRACER_PATH'/dwarf_utils/mod_access_compat_function_map.txt '`head -n 1 mod_access_compat_lib_addr.txt | awk '{print($2);}'`' '$TRACER_PATH'/dwarf_utils/mod_authz_core_function_map.txt '`head -n 1 mod_authz_core_lib_addr.txt | awk '{print($2);}'`' '$TRACER_PATH'/dwarf_utils/mod_authz_user_function_map.txt '`head -n 1 mod_authz_user_lib_addr.txt | awk '{print($2);}'`' '$TRACER_PATH'/dwarf_utils/mod_authz_groupfile_function_map.txt '`head -n 1 mod_authz_groupfile_lib_addr.txt | awk '{print($2);}'`' '$TRACER_PATH'/dwarf_utils/mod_authz_host_function_map.txt '`head -n 1 mod_authz_host_lib_addr.txt | awk '{print($2);}'`' '$TRACER_PATH'/dwarf_utils/mod_authn_core_function_map.txt '`head -n 1 mod_authn_core_lib_addr.txt | awk '{print($2);}'`' '$TRACER_PATH'/dwarf_utils/mod_authn_file_function_map.txt '`head -n 1 mod_authn_file_lib_addr.txt | awk '{print($2);}'`' '$TRACER_PATH'/dwarf_utils/aprutil_function_map.txt '`head -n 1 aprutil_lib_addr.txt | awk '{print($2);}'`' '$TRACER_PATH'/dwarf_utils/apr_function_map.txt '`head -n 1 apr_lib_addr.txt | awk '{print($2);}'` | awk '{
    for (i = 1; i <= NF; i += 2) {
        print($i" "$(i+1)" ");
    }
}' | sort -k 2 | awk '{printf($1" "$2" ");}' >> resolver.sh
sudo chmod +x resolver.sh
