#!/bin/bash
CURRENTDIR=`pwd`
APACHE_BUILD_PATH="/home/jinzihao/apache_build_env/bin"
APR_BUILD_PATH="/usr/local/apr"
TRACER_PATH="/home/jinzihao/chopchop/dyngraph/generation"
OUTPUT_PATH="/home/jinzihao/apache_build_env/outputs"

rm -f $CURRENTDIR/trace.txt
cd $TRACER_PATH
./trace_reader -i $OUTPUT_PATH/trace.bin -o $CURRENTDIR/trace.txt -c
cd $TRACER_PATH/dwarf_utils
./function_map.sh $APACHE_BUILD_PATH/bin/httpd > httpd_function_map.txt
./function_map.sh $APR_BUILD_PATH/lib/libapr-1.so > apr_function_map.txt
./function_map.sh $APR_BUILD_PATH/lib/libaprutil-1.so > aprutil_function_map.txt
./function_map.sh $APACHE_BUILD_PATH/modules/mod_alias.so > mod_alias_function_map.txt
./function_map.sh $APACHE_BUILD_PATH/modules/mod_dir.so > mod_dir_function_map.txt
./function_map.sh $APACHE_BUILD_PATH/modules/mod_autoindex.so > mod_autoindex_function_map.txt
./function_map.sh $APACHE_BUILD_PATH/modules/mod_status.so > mod_status_function_map.txt
./function_map.sh $APACHE_BUILD_PATH/modules/mod_unixd.so > mod_unixd_function_map.txt
./function_map.sh $APACHE_BUILD_PATH/modules/mod_version.so > mod_version_function_map.txt
./function_map.sh $APACHE_BUILD_PATH/modules/mod_setenvif.so > mod_setenvif_function_map.txt
./function_map.sh $APACHE_BUILD_PATH/modules/mod_headers.so > mod_headers_function_map.txt
./function_map.sh $APACHE_BUILD_PATH/modules/mod_env.so > mod_env_function_map.txt
./function_map.sh $APACHE_BUILD_PATH/modules/mod_log_config.so > mod_log_config_function_map.txt
./function_map.sh $APACHE_BUILD_PATH/modules/mod_mime.so > mod_mime_function_map.txt
./function_map.sh $APACHE_BUILD_PATH/modules/mod_filter.so > mod_filter_function_map.txt
./function_map.sh $APACHE_BUILD_PATH/modules/mod_reqtimeout.so > mod_reqtimeout_function_map.txt
./function_map.sh $APACHE_BUILD_PATH/modules/mod_auth_basic.so > mod_auth_basic_function_map.txt
./function_map.sh $APACHE_BUILD_PATH/modules/mod_access_compat.so > mod_access_compat_function_map.txt
./function_map.sh $APACHE_BUILD_PATH/modules/mod_authz_core.so > mod_authz_core_function_map.txt
./function_map.sh $APACHE_BUILD_PATH/modules/mod_authz_user.so > mod_authz_user_function_map.txt
./function_map.sh $APACHE_BUILD_PATH/modules/mod_authz_groupfile.so > mod_authz_groupfile_function_map.txt
./function_map.sh $APACHE_BUILD_PATH/modules/mod_authz_host.so > mod_authz_host_function_map.txt
./function_map.sh $APACHE_BUILD_PATH/modules/mod_authn_core.so > mod_authn_core_function_map.txt
./function_map.sh $APACHE_BUILD_PATH/modules/mod_authn_file.so > mod_authn_file_function_map.txt
./addr2func.sh $CURRENTDIR/trace.txt $CURRENTDIR/trace_resolved.txt $CURRENTDIR/resolver.sh
cd $CURRENTDIR
rm -rf thread_separated
mkdir thread_separated
cd ..
python sep_thread.py $CURRENTDIR/trace_resolved.txt $CURRENTDIR/thread_separated/
