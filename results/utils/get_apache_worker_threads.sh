#!/bin/bash

# Extract the filenames of the "real" worker threads inside a thread_separated folder.
# Usage:
# ./get_apache_worker_threads.sh <path to thread_separated directory>

for file in $1/*
do
	head -n 2 $file | tail -n 1 | awk '{if ($1 == ">" && $2 == "dummy_worker:44" && $3 == "worker_thread:0") {print("'$file'")}}'
done
