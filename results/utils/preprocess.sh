#!/bin/bash

# preprocess.sh: extract information for timeline.cpp, see README.md for details

if [ -z $1 ] || [ ! -d $1 ]
then
    echo "Usage: "$0" <path to thread_separated directory>"
    exit
fi

for file in $1/*
do
    filename=`echo $file | rev | cut -d '/' -f -1 | rev`
    process_id=`echo $filename | cut -d '_' -f 2`
    thread_id=`echo $filename | cut -d '_' -f 3`

    begin_time=`head -n 1 $file | cut -d ' ' -f 5,6`
    end_time=`tail -n 1 $file | cut -d ' ' -f 5,6`

    echo $process_id" "$thread_id" "$begin_time" "$end_time
done
