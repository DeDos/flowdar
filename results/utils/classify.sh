#!/bin/bash

# Usage:
# ./classify.sh <directory> <line number>

# Example:
# Classify trace files in ../1c/thread_separated according to the
# <direction, caller, callee> of the second function call in these trace files,
# output the second function call and number of occurrences:
#    ./classify.sh ../1c/thread_separated 2
# Output:
#      1 > main:91 init_process:0
#      1 > worker_pre_config:237 ap_listen_pre_config:0
#     16 > dummy_worker:44 listener_thread:0
#     16 > dummy_worker:44 start_threads:0
#     16 < make_child:14a apr_signal:0
#    400 > dummy_worker:44 worker_thread:0

# What the output means:
# We call each line of the output a "category". The above output shows 6
# categories.  The values in the first column total to 450, which is the number
# of threads that the script analysed. These threads are broken down into
# subsets having a common line 2 in their trace (since "2" was the parameter
# given to `classify.sh`). Thus 1 thread's trace had "1 > main:91
# init_process:0" in its 2nd line, but 400 had "400 > dummy_worker:44
# worker_thread:0" in that line.

# Note:
# In a web server (e.g. Apache), usually there is a single entry point function
# for threads.
# By passing different parameters to that entry point function, it will spawn
# threads with different characteristics, which means they gradually diverge at
# different points.
# As for Apache, the first few major divergence points (when the largest
# category is broken down into smaller ones) are the 1st, 2nd and 16th function
# call of a thread (shown below).
#
# > ./classify.sh ../1c/thread_separated/ 1
#
#      1 > *INVALID*:a9dd1b main:0
#      1 < worker_pre_config:1b3 apr_proc_detach:0
#     16 > make_child:14a apr_signal:0
#    432 > *INVALID*:e4eba5 dummy_worker:0
#
# > ./classify.sh ../1c/thread_separated/ 2
#
#      1 > main:91 init_process:0
#      1 > worker_pre_config:237 ap_listen_pre_config:0
#     16 > dummy_worker:44 listener_thread:0
#     16 > dummy_worker:44 start_threads:0
#     16 < make_child:14a apr_signal:0
#    400 > dummy_worker:44 worker_thread:0
#
# > ./classify.sh ../1c/thread_separated/ 16
#
#      1 > ap_queue_info_set_idle:a4 apr_atomic_cas32:0
#      1 > apr_palloc:33 pool_concurrency_set_used:0
#      1 > apr_pool_initialize:90 apr_pool_create_ex:0
#     16 > ap_queue_info_set_idle:c6 apr_thread_mutex_lock:0
#     16 < apr_pool_create_ex:1a7 apr_allocator_mutex_get:0
#     16 < apr_thread_mutex_create:4a apr_palloc:0
#     16 > impl_pollset_create:66 apr_palloc:0
#    383 < worker_thread:179 ap_queue_info_set_idle:0
#
# Starting from the 2nd function call, and up until the 15th function call,
# Apache's threads could be classified into 6 categories, the largest of which
# has a size of 400. That is, when `classify.sh` is given parameters N=2 to N=15,
# its output shows 6 categories, and one of the categories is consistently
# shown to have size 400 (i.e., there are 400 threads with a matching line N
# for a specific version of N, though the line's contents might vary for
# different versions of N across the same category).
# The size of the largest category (400) matches the MaxRequestWorkers in
# conf/httpd-mpm.conf, which indicates that that category of threads are the
# actual request workers.

for file in $1/*; do head -n $2 $file | tail -n 1; done | cut -d ' ' -f 1,2,3 | sort | uniq -c | sort -k 1n
