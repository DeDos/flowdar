/*
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


timeline.cpp: tracking the number of threads alive, see README.md for details
Zihao Jin, Tsinghua, Dec 2017
*/

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

using namespace std;

enum EVENTTYPE {BEGIN, END};
string EVENT_TYPE_STRING[] = {"->", "<-"};

class Event {
public:
    EVENTTYPE type;
    unsigned int process_id;
    unsigned int thread_id;
    unsigned int time_s;
    unsigned int time_ms;

    Event() : type(BEGIN), process_id(0), thread_id(0), time_s(0), time_ms(0) {}
    Event(EVENTTYPE type, unsigned int process_id, unsigned int thread_id, unsigned int time_s, unsigned int time_ms) :
        type(type), process_id(process_id), thread_id(thread_id), time_s(time_s), time_ms(time_ms) {}
};

bool event_precedes(const Event& event1, const Event& event2) {
    if (event1.time_s < event2.time_s) {
        return true;
    }
    else if (event1.time_s == event2.time_s) {
        if (event1.time_ms < event2.time_ms) {
            return true;
        }
    }
    return false;
}

int main() {
    unsigned int process_id, thread_id, begin_time_s, begin_time_ms, end_time_s, end_time_ms;
    vector<Event> events;

    while (cin >> process_id >> thread_id >> begin_time_s >> begin_time_ms >> end_time_s >> end_time_ms) {
        events.push_back(Event(BEGIN, process_id, thread_id, begin_time_s, begin_time_ms));
        events.push_back(Event(END, process_id, thread_id, end_time_s, end_time_ms));
    }

    sort(events.begin(), events.end(), event_compare);

    unsigned int threads_alive = 0;

    for (vector<Event>::iterator it = events.begin(); it != events.end(); ++it) {
        if (it->type == BEGIN) {
            ++threads_alive;
        }
        else if (it->type == END) {
            --threads_alive;
        }
        // cout << EVENT_TYPE_STRING[it->type] << " " << it->process_id << " " << it->thread_id << " " << it->time_s << " " << it->time_ms << endl;
        cout << it->time_s << " " << it->time_ms << " " << threads_alive << endl;
    }
    return 0;
}
