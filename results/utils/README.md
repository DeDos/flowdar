Build
--
```
g++ -O2 timeline.cpp -o timeline
```

Run
--
```
./preprocess.sh <path to thread_separated directory> | ./timeline
```

Thread classification
--
See the comments in [classify.sh](classify.sh]) for a description of how
threads are classified into worker threads.

