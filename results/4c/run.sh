# Nginx 1.13.6, unpatched
# Place apache under slowloris attack for 50s

#!/bin/bash
NGINX_BUILD_PATH="/home/jinzihao/apache_build_env/nginx_bin"
TRACER_PATH="/home/jinzihao/chopchop/dyngraph/generation"
TARGETIP='localhost'
TARGETPORT='80'
CURRENTDIR=`pwd`

cp $NGINX_BUILD_PATH/sbin/nginx ./
cd $NGINX_BUILD_PATH/sbin/
sudo ./nginx

cd $CURRENTDIR
sudo ./slowloris.pl -dns $TARGETIP -port $TARGETPORT -timeout 30 -num 500 -tcpto 5 &
sleep 50
sudo pkill 'slowloris.pl'
sudo pkill nginx

cd $CURRENTDIR
echo '#!/bin/bash' > resolver.sh
echo -n $TRACER_PATH'/dwarf_utils/function_resolver ' >> resolver.sh
echo $TRACER_PATH'/dwarf_utils/nginx_function_map.txt 0' >> resolver.sh
sudo chmod +x resolver.sh