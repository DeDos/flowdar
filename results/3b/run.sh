# Apache 2.4.26, unpatched
# Download a large file (GP010020.MP4) one time
# File content:
# MP4 video, with a size of 2.3GB

#!/bin/bash
HOMEDIR=/home/jinzihao
CURRENTDIR=`pwd`
TARGET='http://127.0.0.1/GP010020.MP4'
cp $HOMEDIR/apache_build_env/bin/bin/httpd ./
cd $HOMEDIR/apache_build_env/bin/bin/
sudo ./apachectl start
wget -q -O /dev/null $TARGET
cd $CURRENTDIR
sudo $HOMEDIR/chopchop/dyngraph/generation/dwarf_utils/getlibaddr.sh httpd libapr-1 > apr_lib_addr.txt
sudo $HOMEDIR/chopchop/dyngraph/generation/dwarf_utils/getlibaddr.sh httpd libaprutil-1 > aprutil_lib_addr.txt
cd $HOMEDIR/apache_build_env/bin/bin/
#sudo ./apachectl stop # Commented out to avoid another instance of Apache starting
sudo pkill httpd
cd $CURRENTDIR
