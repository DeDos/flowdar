# Nginx 1.13.6, unpatched
# Download a large file (GP010095.MP4)
# File content:
# MP4 video, with a size of 2.3GB

#!/bin/bash
NGINX_BUILD_PATH="/home/jinzihao/apache_build_env/nginx_bin"
TRACER_PATH="/home/jinzihao/chopchop/dyngraph/generation"
TARGET='http://127.0.0.1/GP010095.MP4'
CURRENTDIR=`pwd`

cp $NGINX_BUILD_PATH/sbin/nginx ./
cd $NGINX_BUILD_PATH/sbin/
sudo ./nginx
wget -q -O /dev/null $TARGET
sudo pkill nginx

cd $CURRENTDIR
echo '#!/bin/bash' > resolver.sh
echo -n $TRACER_PATH'/dwarf_utils/function_resolver ' >> resolver.sh
echo $TRACER_PATH'/dwarf_utils/nginx_function_map.txt 0' >> resolver.sh
sudo chmod +x resolver.sh
