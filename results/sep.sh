#!/bin/bash
for dir in `ls -d */`
do
	rm -r $dir"thread_separated"
	mkdir $dir"thread_separated"
	python sep_thread.py $dir"trace.txt" $dir"thread_separated/"
done
