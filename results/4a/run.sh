# Nginx 1.13.6, unpatched
# Download a small file (index.html)
# File content:
# ```
# <!DOCTYPE html>
# <html>
# <head>
# <title>Welcome to nginx!</title>
# <style>
#     body {
#         width: 35em;
#         margin: 0 auto;
#         font-family: Tahoma, Verdana, Arial, sans-serif;
#     }
# </style>
# </head>
# <body>
# <h1>Welcome to nginx!</h1>
# <p>If you see this page, the nginx web server is successfully installed and
# working. Further configuration is required.</p>
#
# <p>For online documentation and support please refer to
# <a href="http://nginx.org/">nginx.org</a>.<br/>
# Commercial support is available at
# <a href="http://nginx.com/">nginx.com</a>.</p>
# 
# <p><em>Thank you for using nginx.</em></p>
# </body>
# </html>
# ```

#!/bin/bash
NGINX_BUILD_PATH="/home/jinzihao/apache_build_env/nginx_bin"
TRACER_PATH="/home/jinzihao/chopchop/dyngraph/generation"
TARGET='http://127.0.0.1/index.html'
CURRENTDIR=`pwd`

cp $NGINX_BUILD_PATH/sbin/nginx ./
cd $NGINX_BUILD_PATH/sbin/
sudo ./nginx
wget -q -O /dev/null $TARGET
sudo pkill nginx

cd $CURRENTDIR
echo '#!/bin/bash' > resolver.sh
echo -n $TRACER_PATH'/dwarf_utils/function_resolver ' >> resolver.sh
echo $TRACER_PATH'/dwarf_utils/nginx_function_map.txt 0' >> resolver.sh
sudo chmod +x resolver.sh
