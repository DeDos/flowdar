#!/bin/bash
CURRENTDIR=`pwd`
NGINX_BUILD_PATH="/home/jinzihao/apache_build_env/nginx_bin"
TRACER_PATH="/home/jinzihao/chopchop/dyngraph/generation"
OUTPUT_PATH="/home/jinzihao/apache_build_env/outputs"

rm -f $CURRENTDIR/trace.txt
cd $TRACER_PATH
./trace_reader -i $OUTPUT_PATH/trace.bin -o $CURRENTDIR/trace.txt -c
cd $TRACER_PATH/dwarf_utils
./function_map.sh $NGINX_BUILD_PATH/sbin/nginx > nginx_function_map.txt
./addr2func.sh $CURRENTDIR/trace.txt $CURRENTDIR/trace_resolved.txt $CURRENTDIR/resolver.sh
cd $CURRENTDIR
rm -rf thread_separated
mkdir thread_separated
cd ..
python sep_thread.py $CURRENTDIR/trace_resolved.txt $CURRENTDIR/thread_separated/
