# Apache 2.4.26, patched with apache2.4.26_backendworkers.patch
# Download a small file (index.html) <del>three times</del>
# File content:
# ```
# <html><body><h1>It works!</h1></body></html>
# ```

#!/bin/bash
HOMEDIR=/home/jinzihao
TARGET='http://127.0.0.1/index.html'
cp $HOMEDIR/apache_build_env/bin/bin/httpd ./
cd $HOMEDIR/apache_build_env/bin/bin/
PWD=`pwd`
sudo ./apachectl start
#for i in {1..3}
#do
	wget -q -O /dev/null $TARGET
#done
cd $CURRENTDIR
sudo $HOMEDIR/chopchop/dyngraph/generation/dwarf_utils/getlibaddr.sh httpd libapr-1 > apr_lib_addr.txt
sudo $HOMEDIR/chopchop/dyngraph/generation/dwarf_utils/getlibaddr.sh httpd libaprutil-1 > aprutil_lib_addr.txt
cd $HOMEDIR/apache_build_env/bin/bin/
#sudo ./apachectl stop # Commented out to avoid another instance of Apache starting
sudo pkill httpd
cd $CURRENTDIR
