#!/bin/bash
HOMEDIR=/home/jinzihao
TRACEDIR=`pwd`

patch $HOMEDIR/apache_build_env/httpd-2.4.25/server/mpm/worker/worker.c $1 -R

cd $HOMEDIR
./r tr &
./r ab -DCALLSTACK
sudo pkill trace_buff
sudo pkill trace_reader
./r tr > $TRACEDIR/1a/trace.txt &
cd $TRACEDIR/1a
sudo pkill --signal INT httpd
./run.sh
sudo pkill trace_buff
sudo pkill trace_reader
cp $HOMEDIR/apache_build_env/bin/bin/httpd $TRACEDIR/1a/

cd $HOMEDIR/apache_build_env/bin/htdocs
ln -s /home/jinzihao/GP010020.MP4 GP010020.MP4
cd $HOMEDIR
./r tr > $TRACEDIR/1b/trace.txt &
cd $TRACEDIR/1b
sudo pkill --signal INT httpd
./run.sh
sudo pkill trace_buff
sudo pkill trace_reader
cp $HOMEDIR/apache_build_env/bin/bin/httpd $TRACEDIR/1b/

patch $HOMEDIR/apache_build_env/httpd-2.4.25/server/mpm/worker/worker.c $1

cd $HOMEDIR
./r tr &
./r ab -DCALLSTACK
sudo pkill trace_buff
sudo pkill trace_reader
./r tr > $TRACEDIR/2a/trace.txt &
cd $TRACEDIR/2a
sudo pkill --signal INT httpd
./run.sh
sudo pkill trace_buff
sudo pkill trace_reader
cp $HOMEDIR/apache_build_env/bin/bin/httpd $TRACEDIR/2a/

cd $HOMEDIR/apache_build_env/bin/htdocs
ln -s /home/jinzihao/GP010020.MP4 GP010020.MP4
cd $HOMEDIR
./r tr > $TRACEDIR/2b/trace.txt &
cd $TRACEDIR/2b
sudo pkill --signal INT httpd
./run.sh
sudo pkill trace_buff
sudo pkill trace_reader
cp $HOMEDIR/apache_build_env/bin/bin/httpd $TRACEDIR/2b/

