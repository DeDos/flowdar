#!/usr/bin/env python
# Split a trace into subtraces pertaining to different threads.
# Nik Sultana, UPenn, June 2017

import argparse

arg_parser = argparse.ArgumentParser(description = "Split a trace into subtraces pertaining to different threads.")
arg_parser.add_argument("in_trace", metavar = "FILEPATH", type = str,
        help = "Path to the file containing the trace to process")
arg_parser.add_argument("out_trace_prefix", metavar = "FILEPATH", type = str,
        help = "Path to the output file, used as a prefix for the name of the file containing each thread's trace")
arg_parser.add_argument('--verbose',
        help = 'Verbose output', required = False, action = 'store_const', const = True)
args = arg_parser.parse_args()

# FIXME repeated from trace_to_bmp.py
expected_number_of_cols = 7
col_direction = 0
col_caller = 1
col_callee = 2
col_pid = 3
col_sec = 4
col_usec = 5
col_tid = 6

fn_call_symbol = ">"
fn_retn_symbol = "<"
# end of FIXME

threads = dict()
subtrace = dict()

# FIXME here too can reuse from trace_to_bmp.py
with open(args.in_trace, 'r') as infile:
  for line in infile:
    line = line.rstrip()
    cols = line.split()
    # if len(cols) <> expected_number_of_cols:
    #   print "Malformed entry? Was expecting " + str(expected_number_of_cols) + " columns: '" + line + "'"
    #   exit(1)
# end of FIXME
    key = cols[col_pid] + "_" + cols[col_tid]
    if key not in subtrace:
      # FIXME check if exists before opening
      outfile_name = args.out_trace_prefix + "_" + key
      print ("Writing thread subtrace to: " + outfile_name)
      subtrace[key] = open(outfile_name, 'w')
      threads[cols[col_tid]] = [cols[col_pid], line]
    [pid, orig_line] = threads[cols[col_tid]]
    if args.verbose == True and cols[col_pid] <> pid:
      print "Warnings, possible thread id recycling -- was expecting pid to be " + pid + " for thread (" + cols[col_tid] + ") that reported this: '" + line + "', based on '" + orig_line + "'"
    subtrace[key].write(line + "\n")

for out_fd in subtrace:
  subtrace[out_fd].close()

print ("No. of subtraces: " + str(len(subtrace.items())))

print "Done"
exit(0)
