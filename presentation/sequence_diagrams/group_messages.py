'''
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


Achala Rao
group_messages.py - Identify consecutively occurring patterns with window_size specified by user and group them into one.
These patterns are demarcated in the trace with a begin and end line. 
The begin line also contains the number of times the enclosed pattern repeats.
Usage: python group_messages.py <window_size> <input_trace_file> <output_trace_file>
UPenn, 2017
'''
import sys
import linecache
import subprocess
import os
import tempfile


def group_by_pattern(window_size, input_trace_file, out_trace):
    # Keeps track of the line number to be read
    line_pointer = 1
    pattern = ""
    pattern_lines = ""
    outfile = open(out_trace, "w")
    pattern_count = 0
    eof = False
    existing_pattern = False
    # Clear cache for every call
    linecache.clearcache()

    while True:
        # Reset the lines variable for every iteration
        lines = ""
        lines_to_compare = ""
        # If a new pattern has to be found
        if pattern_count == 0:
            for i in range(line_pointer, line_pointer + window_size):
                line = linecache.getline(input_trace_file, i)
                # End of file reached. getline returns empty line after EOF reached.
                if line == "":
                    if pattern_lines != "":
                        outfile.write(pattern_lines)
                    eof = True
                    break

                # If an existing pattern already in file, do not parse, write to output directly
                if line.split(" ")[1] == "begin\n":
                    if pattern_lines != "":
                        outfile.write(pattern_lines)

                    while line != "end\n":
                        outfile.write(line)
                        i += 1
                        line = linecache.getline(input_trace_file, i)

                    outfile.write(line)
                    line_pointer = i + 1
                    existing_pattern = True
                    break

                else:
                    # the first string contains just the columns to be compared. The second string contains the
                    # entire line to be written to file. The first 3 columns contain the direction of the message as
                    # well as the functions that are communicating. Append next line to pattern
                    pattern += " ".join(line.split()[:3])
                    pattern_lines += line

            # Reset pattern and its count
            if existing_pattern:
                pattern = ""
                pattern_lines = ""
                pattern_count = 0
                existing_pattern = False
                continue

            # Pattern observed. Move window ahead by window_size
            line_pointer += window_size

            if eof:
                break

        # Read the next few lines based on window size and compare to pattern
        for i in range(line_pointer, line_pointer + window_size):

            line = linecache.getline(input_trace_file, i)
            # EOF reached
            if line == "":
                if pattern_lines != "":
                    # If some previous pattern found, write that to file first
                    if pattern_count != 0:
                        outfile.write(str(pattern_count + 1) + " begin\n" + pattern_lines + "end\n")
                    else:
                        outfile.write(pattern_lines)

                if lines != "":
                    outfile.write(lines)

                eof = True
                break

            # If pattern already exists in file, write to output directly
            if line.split(" ")[1] == "begin\n":

                if pattern_lines != "":
                    if pattern_count != 0:
                        outfile.write(str(pattern_count + 1) + " begin\n" + pattern_lines + "end\n")
                    else:
                        outfile.write(pattern_lines)

                # Write any observed patterns to file
                if lines != "":
                    outfile.write(lines)

                while line != "end\n":
                    outfile.write(line)
                    i += 1
                    line = linecache.getline(input_trace_file, i)

                outfile.write(line)
                line_pointer = i + 1
                existing_pattern = True
                break

            # The first string contains just the columns to be compared. The second string contains the entire line
            # to be written to file
            lines_to_compare += " ".join(linecache.getline(input_trace_file, i).split()[:3])
            lines += linecache.getline(input_trace_file, i)

        # Reset pattern and its count
        if existing_pattern:
            pattern = ""
            pattern_lines = ""
            existing_pattern = False
            pattern_count = 0
            continue

        if eof:
            break

        # If pattern repeated, increment count and move sliding window
        if pattern == lines_to_compare:
            pattern_count += 1
            line_pointer = line_pointer + window_size

        else:
            # If pattern matched some number of times, enclose this pattern within "begin" and "end" along with
            # number of repetitions
            if pattern_count > 0:
                outfile.write(str(pattern_count + 1) + " begin\n" + pattern_lines + "end\n")

            else:
                # If pattern not matched even once, move line pointer by 1 and try matching the next pattern.
                outfile.write(linecache.getline(input_trace_file, line_pointer - window_size))
                line_pointer = line_pointer - window_size + 1

            # Reset pattern
            pattern = ""
            pattern_lines = ""
            pattern_count = 0

    outfile.close()


window_size = int(sys.argv[1])
input_file = sys.argv[2]
output_file = sys.argv[3]

if os.path.exists(output_file):
    user_input = input("Output trace file already exists. Overwrite? (y/n)")

    while user_input.lower() != "y" and user_input.lower() != "n":
        user_input = input("Please enter (y/n):")

    if user_input.lower() == "n":
        print("Exiting program..")
        sys.exit(0)

subprocess.call(["cp", input_file, output_file])

while window_size > 1:
    # Create temporary file to store intermediate output files, which is rewritten after every iteration
    temp_output_file = tempfile.NamedTemporaryFile(prefix='temp_', delete=False)
    group_by_pattern(window_size, output_file, temp_output_file.name)
    subprocess.call(["mv", temp_output_file.name, output_file])
    window_size -= 1
