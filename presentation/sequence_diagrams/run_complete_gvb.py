#!/usr/bin/env python
#
#Copyright (c) 2019, University of Pennsylvania
#All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the <organization> nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
#DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#
# Runs the toolchain on a group of good traces versus a group of bad traces in order to find major points of divergence
# Pardis Pashakhanloo, UPenn, February 2018

import os
import sys
import subprocess

# Example Usage: python3 run_complete_gvb.py gvb/good gvb/bad gvb/results ta_final_results 2 4 200
good_folder = sys.argv[1]
bad_folder = sys.argv[2]
output_folder = sys.argv[3]
TA_final_result_filename = sys.argv[4]
num_points_div = int(sys.argv[5])
depth = sys.argv[6]
thresh = sys.argv[7]  # microseconds

i = 1

for good in os.listdir(good_folder):
    for bad in os.listdir(bad_folder):
        if not (('DS_Store' in bad) or ('DS_Store' in good)):
            subprocess.call(
                ["python3", "run_single_gvb.py", good_folder + '/' + good, bad_folder + '/' + bad, output_folder, depth, thresh])

with open(output_folder + '/' + TA_final_result_filename, 'w') as ta_result:
    for item in os.listdir(output_folder):
        if item.startswith('TA'):
            with open(output_folder + '/' + item, 'r') as ta_single:
                for i in range(0, num_points_div):
                    line = ta_single.readline()
                    ta_result.write(line)

for item in os.listdir(output_folder):
    if not item == TA_final_result_filename:
        os.remove(output_folder + '/' + item)
