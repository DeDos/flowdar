'''
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


Achala Rao 
log_to_pdf.py - Converts the logs of function calls to a sequence diagram depicting these calls and stores them in a
graphical form as a PDF file.
UPenn, 2017 
'''
import subprocess
import sys
import os.path
import argparse
import tempfile
import shutil
import glob
import linecache

# Note: This constant represents the maximum multiple of the threshold time gap between two messages, that has been
# chosen to be represented on the sequence diagram. Changing this constant will affect the visibility of the dots in
# the sequence diagram. The value has been optimized to achieve best results for viewing the document. Please take a
# look at the \postlevel command in Latex for better spacing options.
TIME_MULTIPLE_CONSTANT = 5


# Reads the log file and writes the messages in a formatted way into a file depending on the direction of the
# function call
def format_messages(logfile, tmp_directory_name, max_interval):
    infile = open(logfile, "r")
    ts_prev = 0
    # Create temporary file to store the entire set of messages
    messages_full_file = tempfile.NamedTemporaryFile(prefix='messages_full_', dir=tmp_directory_name, delete=False)
    outfile = open(messages_full_file.name, "w")

    # Format the messages file as needed by latex
    for line in infile:
        if line.startswith("end"):
            outfile.write("\\end{sdblock}\n")
            # Draw a thick border around the block
            outfile.write(
                "\\path (nw)--coordinate (aux)(se);\n\\draw[ultra thick] (se -| se)--(nw |- se)--(nw |- nw)--(nw -| se)--(se |- se);\n")
            continue

        elif line.split(" ")[1] == "begin\n":
            # When groups of messages found, draw a block around it, indicating the number of times this group of
            # messages repeats
            outfile.write("\\begin{sdblock}[green]{" + line.split(" ")[0] + "}{}\n")
            continue

        cols = line.split(" ")
        args = ""

        # If time interval more than a given threshold, add a small red dot for every threshold number of seconds on
        # the timeline to indicate longer interval of time
        if (len(
                cols) > 4):  # If the number of columns in the trace file greater than 4, it means timestamp
            # information is present.
            ts_current = (int(cols[4]) * 1000000) + int(cols[5])
            difference = ts_current - ts_prev

            if difference > max_interval and ts_prev != 0:
                time_intervals = int(difference / max_interval)
                # If the time interval is less than 3 times the threshold, the red dots for every time the threshold
                # time has passed can be accommodated in the space between the messages.
                if time_intervals < (TIME_MULTIPLE_CONSTANT - 2):
                    draw_message(cols, outfile)

                    # For every threshold period of time, mark with red dot
                    for i in range(time_intervals):
                        outfile.write("\\path(mess to) -- (mess from) node[red, left=0mm, above=" + str(
                            i * 2) + "mm]{$\\bullet$};\n")
                else:
                    # Widen the gap between the messages
                    outfile.write("\\postlevel\n")

                    draw_message(cols, outfile)

                    # If the time taken between the messages is more than 5 times the threshold duration, draw 4 red
                    # dots and a timestamp on the top to indicate how much time has passed in ms.
                    if time_intervals > TIME_MULTIPLE_CONSTANT:

                        for i in range(TIME_MULTIPLE_CONSTANT - 1):
                            outfile.write("\\path(mess to) -- (mess from) node[red, left=0mm, above=" + str(
                                i * 2) + "mm]{$\\bullet$};\n")
                        outfile.write("\\path(mess to) -- (mess from) node[blue, left=0mm, above=8mm]{" + str(
                            difference / float(1000)) + "ms};\n")
                    # Draw a red dot for every threshold period of time
                    else:
                        for i in range(time_intervals):
                            outfile.write("\\path(mess to) -- (mess from) node[red, left=0mm, above=" + str(
                                i * 2) + "mm]{$\\bullet$};\n")
            else:
                draw_message(cols, outfile)

            ts_prev = ts_current
        # if no timestamp information is present, write message to output
        else:
            if cols[0] == ">":
                outfile.write("\\mess{" + cols[1].strip() + "}{}{" + cols[2].rstrip("\n") + "}\n")
            elif cols[0] == "<":
                outfile.write("\\mymess{dashed}{" + cols[2].rstrip("\n") + "}{}{" + cols[1].strip() + "}\n")

    infile.close()
    outfile.close()

    return messages_full_file.name


# ##################################################################### To draw a message line based on call or return
def draw_message(cols, outfile):
    if cols[0] == ">":
        # If the number of columns is more than 7 in the trace file, it means that the trace file contains
        # information about the parameters and return values of the functions
        if len(cols) > 7:
            # If its a function call, display the function parameters
            args = cols[7] + ", " + cols[8] + ", " + cols[9] + ", " + cols[10] + ", " + cols[11] + ", " + cols[12]

        # Write the messages in a formatted manner to be input to latex. \\mess depicts a message. \\mymess is a
        # custom message to produce a dashed line.
        outfile.write("\\mess{" + cols[1].strip() + "}{" + args + "}{" + cols[2].strip() + "}\n")

        # If the caller and callee is the same function, draw an arc from the function thread to itself
        if cols[1] == cols[2]:
            outfile.write("\\path(mess from)--(mess to){};\n\\draw [->] (mess to.100) arc (-260:70:2mm){};\n")

    elif cols[0] == "<":
        if len(cols) > 7:
            # If its a function return, display the return value
            args = cols[7]
        outfile.write("\\mymess{dashed}{" + cols[2].strip() + "}{" + args + "}{" + cols[1].strip() + "}\n")

        # If the caller and callee is the same function, draw an arc from the function thread to itself, and dashed
        # line to indicate return
        if cols[1] == cols[2]:
            outfile.write(
                "\\path(mess from)--(mess to){};\n\\draw [->, densely dashed] (mess to.100) arc (-260:70:2mm){};\n")


#######################################################################################################################
# Reads the formatted messages in batches and writes them into a file called "messages"
def create_batch(messages_full_file, line_no, num_messages, max_functions, max_messages, group_messages):
    functions = set()
    # Write messages of current batch to a temporary file called "messages". This is fed as input to the
    # seq_diagram.tex file
    message_outfile = open("messages", "w")
    no_lines = 1

    # Read messages from the full_messages file until either the number of functions or the number of messages has
    # reached the max limit for one pdf, or if all messages hvae been read.
    while len(functions) < max_functions and no_lines < max_messages and line_no <= num_messages:
        # efficiently get a particular line from a file
        line = linecache.getline(messages_full_file, line_no)
        line_no += 1
        # \postlevel is used to give extra spacing between messages. In this module, the spacing is useful to
        # accomodate the extra red/blue dots. Hence, the next line which follows(the message line) as well as the
        # subsequent lines which are used to draw the red dots(\path) should all be included in the same message group.
        if line.startswith("\\postlevel"):
            message_outfile.write(line)
            no_lines += 1
            line = linecache.getline(messages_full_file, line_no)

            if line.startswith("\\mymess"):
                msgs = line.split("{")
                # The first element in this split array is the command mymess. The second element is the type of
                # line. The 3rd and 5th elements are the function names
                if msgs[2].strip("}") not in functions:
                    functions.add(msgs[2].strip("}"))
                if msgs[4].strip("}\n") not in functions:
                    functions.add(msgs[4].strip("}\n"))
                # Write this message to the temporary messages file
                message_outfile.write(line)
                no_lines += 1
                line_no += 1
            else:
                msgs = line.split("{")
                if msgs[1].strip("}") not in functions:
                    functions.add(msgs[1].strip("}"))
                if msgs[3].strip("}\n") not in functions:
                    functions.add(msgs[3].strip("}\n"))
                # Write this message to the temporary messages file
                message_outfile.write(line)
                no_lines += 1
                line_no += 1

            line = linecache.getline(messages_full_file, line_no)

            if line.startswith("\\path"):
                message_outfile.write(line)
                no_lines += 1
                line_no += 1
                line = linecache.getline(messages_full_file, line_no)

                if line.startswith("\\draw"):
                    message_outfile.write(line)
                    no_lines += 1
                    line_no += 1
                    line = linecache.getline(messages_full_file, line_no)

                while line.startswith("\\path"):
                    message_outfile.write(line)
                    no_lines += 1
                    line_no += 1
                    line = linecache.getline(messages_full_file, line_no)

                continue

        # Subsequent lines beginning with path also reference previous messages and therefore must be included in the
        #  same file
        if line.startswith("\\path"):
            message_outfile.write(line)
            no_lines += 1
            line = linecache.getline(messages_full_file, line_no)

            if line.startswith("\\draw"):
                message_outfile.write(line)
                no_lines += 1
                line_no += 1
                line = linecache.getline(messages_full_file, line_no)

            while line.startswith("\\path"):
                message_outfile.write(line)
                no_lines += 1
                line_no += 1
                line = linecache.getline(messages_full_file, line_no)

            continue

        # if a group of messages is encountered, write as is to file
        if line.startswith("\\begin"):
            message_outfile.write(line)
            no_lines += 1

            # Write entire group of messages until end is encountered
            while True:
                line = linecache.getline(messages_full_file, line_no)

                if line.startswith("\\end"):
                    message_outfile.write(line)
                    no_lines += 1
                    line_no += 1
                    line = linecache.getline(messages_full_file, line_no)

                    # Check if next two lines have path and draw (used to draw thick border). If so, write to file.
                    if line.startswith("\\path"):
                        message_outfile.write(line)
                        if linecache.getline(messages_full_file, line_no + 1).startswith("\\draw"):
                            message_outfile.write(linecache.getline(messages_full_file, line_no + 1))
                            no_lines += 1
                            line_no += 1
                        no_lines += 1
                        line_no += 1

                    break

                elif line.startswith("\\postlevel"):
                    message_outfile.write(line)
                    no_lines += 1
                    line_no += 1
                    continue
                # If \path encountered, write that line as well as subsequent lines beginning with \path to file. All
                #  these lines are in reference to the previous message.
                elif line.startswith("\\path"):
                    message_outfile.write(line)
                    no_lines += 1
                    line_no += 1
                    line = linecache.getline(messages_full_file, line_no)

                    if line.startswith("\\draw"):
                        message_outfile.write(line)
                        no_lines += 1
                        line_no += 1
                        line = linecache.getline(messages_full_file, line_no)

                    while line.startswith("\\path"):
                        message_outfile.write(line)
                        no_lines += 1
                        line_no += 1
                        line = linecache.getline(messages_full_file, line_no)

                    continue

                elif line.startswith("\\mymess"):
                    msgs = line.split("{")
                    # The first element in this split array is the command mymess. The second element is the type of
                    # line. The 3rd and 5th elements are the function names
                    if msgs[2].strip("}") not in functions:
                        functions.add(msgs[2].strip("}"))
                    if msgs[4].strip("}\n") not in functions:
                        functions.add(msgs[4].strip("}\n"))
                    # Write this message to the temporary messages file
                    message_outfile.write(line)
                    no_lines += 1
                    line_no += 1
                    continue

                # extract the function names interacting in this message and add them to the set of messages for this
                #  particular batch
                msgs = line.split("{")

                if msgs[1].strip("}") not in functions:
                    functions.add(msgs[1].strip("}"))
                if msgs[3].strip("}\n") not in functions:
                    functions.add(msgs[3].strip("}\n"))

                # Write this message to the temporary messages file
                message_outfile.write(line)
                no_lines += 1
                line_no += 1

        # If customized return message line encountered, extract functions and write to file
        elif line.startswith("\\mymess"):
            msgs = line.split("{")

            # The first element in this split array is the command mymess. The second element is the type of line.
            # The 3rd and 5th elements are the function names
            if msgs[2].strip("}") not in functions:
                functions.add(msgs[2].strip("}"))
            if msgs[4].strip("}\n") not in functions:
                functions.add(msgs[4].strip("}\n"))
            # Write this message to the temporary messages file
            message_outfile.write(line)
            no_lines += 1

        # If message line encountered, write to file
        else:
            msgs = line.split("{")
            if msgs[1].strip("}") not in functions:
                functions.add(msgs[1].strip("}"))
            if msgs[3].strip("}\n") not in functions:
                functions.add(msgs[3].strip("}\n"))
            # Write this message to the temporary messages file
            message_outfile.write(line)
            no_lines += 1

    # Include path line in the same file since it references the previous message
    line = linecache.getline(messages_full_file, line_no)

    if line.startswith("\\postlevel"):
        message_outfile.write(line)
        line_no += 1
        line = linecache.getline(messages_full_file, line_no)

        if line.startswith("\\mymess"):
            msgs = line.split("{")
            # The first element in this split array is the command mymess. The second element is the type of line.
            # The 3rd and 5th elements are the function names
            if msgs[2].strip("}") not in functions:
                functions.add(msgs[2].strip("}"))
            if msgs[4].strip("}\n") not in functions:
                functions.add(msgs[4].strip("}\n"))
            # Write this message to the temporary messages file
            message_outfile.write(line)
            line_no += 1
        else:
            msgs = line.split("{")
            if msgs[1].strip("}") not in functions:
                functions.add(msgs[1].strip("}"))
            if msgs[3].strip("}\n") not in functions:
                functions.add(msgs[3].strip("}\n"))
            # Write this message to the temporary messages file
            message_outfile.write(line)
            line_no += 1

        line = linecache.getline(messages_full_file, line_no)

    # If any subsequent lines contain path, include in the same messages file.
    if line.startswith("\\path"):
        message_outfile.write(line)
        line_no += 1
        line = linecache.getline(messages_full_file, line_no)

        if line.startswith("\\draw"):
            message_outfile.write(line)
            line_no += 1
            line = linecache.getline(messages_full_file, line_no)

        while line.startswith("\\path"):
            message_outfile.write(line)
            line_no += 1
            line = linecache.getline(messages_full_file, line_no)

    message_outfile.close()

    # Obtain the names of functions that are interacting in this batch of messages and write them to a file in a
    # formatted manner, to be fed as input to the tex file.
    functions_outfile = open("functions_tex", "w")
    sorted_functions = sorted(list(functions))

    for item in sorted_functions:
        if item == "":
            continue
        functions_outfile.write("\\newthread{" + item + "}{" + item + "}{}\n")

    functions_outfile.close()
    functions.clear()

    return line_no


#######################################################################################################################
# Converts the messages into sequence diagrams and stores them as PDF files in batches. Returns the number of batches
def convert_to_pdf(messages_full_file, save_temp, max_functions, max_messages, group_messages, display_tex):
    # Count the number of function calls
    with open(messages_full_file) as f:
        for i, l in enumerate(f):
            pass

    num_messages = i + 1
    line_no = 1
    batches = 0

    while line_no < num_messages:
        # Create the batch of messages with size as specified by user
        line_no = create_batch(messages_full_file, line_no, num_messages, max_functions, max_messages, group_messages)
        batches += 1
        print("Segment " + str(batches))

        if display_tex == "False":
            # Call pdflatex and write output to null
            with open('/dev/null', 'w') as file:
                subprocess.call(["pdflatex", "seq_diagram.tex"], stdout=file)
        else:
            subprocess.call(["pdflatex", "seq_diagram.tex"])

        # Write to pdfs in batches with sequential file numbers.
        outfile = "seq_diagram_" + str(batches) + ".pdf"
        subprocess.call(["cp", "seq_diagram.pdf", outfile])
        print("Wrote " + outfile)

    print("Done")

    if not save_temp:
        os.remove("messages")

    return batches


#######################################################################################################################
# Merges all PDFs together into a single PDF using pdftk
def merge_pdfs(batches):
    cmd = "pdfunite "
    for i in range(1, batches + 1):
        outfile = "seq_diagram_" + str(i) + ".pdf"
        cmd += " " + outfile
    cmd += " seq_diagram.pdf"
    print("Command is " + cmd)
    subprocess.call(cmd, shell=True)


#######################################################################################################################
if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Convert log file to PDF')

    parser.add_argument('--logfile',
                        help='File that contains the message logs', required=True)
    parser.add_argument('--batch_size', '-b', default="800",
                        help='Size of the message batch to be processed at once')
    parser.add_argument('--max_interval', default="500",
                        help='Time interval in micro seconds to be marked in the diagram')
    parser.add_argument('--group_messages', '-g', default="True",
                        help='Option to group messages based on patterns')
    parser.add_argument('-s', default="False",
                        help='Save temporary files')
    parser.add_argument('-l', default="50",
                        help='Limit on the number of functions in a PDF')
    parser.add_argument('--display_latex', '-d',
                        help='Print the output of LaTeX to console', default="False", required=False,
                        action='store_const', const=True)

    args = parser.parse_args()

    if args.logfile is None:
        print(parser.print_help())
        sys.exit(0)

    if args.s.lower() != "true" and args.s.lower() != "false":
        print("The -s (Save temp files) option takes only true or false")
        print(parser.print_help())
        sys.exit(0)

    if args.s.lower() == "true":
        save_temp = True
    else:
        save_temp = False

    if args.group_messages.lower() != "true" and args.group_messages.lower() != "false":
        print("The -g or --group_messages(group messages based on patterns) option takes only true or false")
        print(parser.print_help())
        sys.exit(0)

    if args.group_messages.lower() == "true":
        group_messages = True
    else:
        group_messages = False

    # NOTE: The maximum number of messages that can be accommodated in a single PDF is constrained by the maximum
    # dimensions allowed by pdflatex which roughly amounts to 800.
    # Also, the maximum number of functions in one PDF has been carefully set so as to not overburden the system by
    # creating too many PDFs while demonstrating repetition of patterns in a single PDF.
    # The user is still given the option to tweak these parameters within their boundaries.

    if int(args.l) > 50:
        print(
            "Warning: The ideal value for this parameter is 50, since it maintains the balance between how much data "
            "the PDF can accommodate and the number of PDFs created. Exceeding a value greater than 200 may result in "
            "errors from pdflatex since it cannot accommodate such large dimensions.")

    if int(args.batch_size) > 800:
        print(
            "Sorry, this value exceeds the maximum number of messages that can be accommodated in a PDF. (Due to "
            "Latex's limitations on size of a PDF). Please try with a smaller limit.")
        sys.exit(1)

    tmp_directory_name = tempfile.mkdtemp()
    messages_full_file = format_messages(args.logfile, tmp_directory_name, int(args.max_interval))
    batches = convert_to_pdf(messages_full_file, save_temp, int(args.l), int(args.batch_size), group_messages,
                             args.display_latex)
    merge_pdfs(batches)

    if not save_temp:
        shutil.rmtree(tmp_directory_name)
        os.remove("functions_tex")
        for file in glob.glob("seq_diagram_*.pdf"):
            os.remove(file)

    else:
        print("Log files saved to " + tmp_directory_name)
