'''
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


Achala Rao 
filter_messages.py - Take the output of the diff command as input and eliminate lines that differ in the two files for side by side comparison.
Write these lines from the original trace(including timing information) into new files. 
UPenn, 2017 
'''
import sys
import linecache

def filter_messages(diff_filename, in1, in2, output1, output2):
	
	diff_file = open(diff_filename, "r")

	out1 = open(output1, "w")
	out2 = open(output2, "w")
	
	line_no = 0

	for line in diff_file:
		
		line_no += 1
		#If the line is the same in both traces w.r.t the first three columns, write the original lines from the filtered traces.
		if len(line.split()) == 6:
			out1.write(linecache.getline(in1, line_no))
			out2.write(linecache.getline(in2, line_no))

	diff_file.close()
	out1.close()
	out2.close()

if __name__ == "__main__":

	diff_filename = sys.argv[1] #Output of diff of traces

	#Input files which contain the two traces after filtering out the 
	#unnecessary messages from structural_analysis.py
	in1 = sys.argv[2]
	in2 = sys.argv[3]

	#File path to store the new output trace files
	out1 = sys.argv[4]
	out2 = sys.argv[5]

	filter_messages(diff_filename, in1, in2, out1, out2)

