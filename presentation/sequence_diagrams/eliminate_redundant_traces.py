'''
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


Achala Rao 
eliminate_redundant_traces.py - Use the result from compare_traces.py to copy only one representative file in a group of
 similar files to a different folder. This new folder will contain all unique files.
UPenn, 2017 
'''
import os
import sys
import subprocess
import shutil

unique_files = set()
input_dir = sys.argv[2]
comparison_file = open(sys.argv[1], "r")
output_dir = sys.argv[3]

if os.path.exists(output_dir):
    input_option = raw_input("Output directory already exists. Overwrite? (y/n)")
    if input_option == "n":
        print("Exiting..")
        sys.exit(0)
    else:
        shutil.rmtree(output_dir)
else:
    os.mkdir(output_dir)

for line in comparison_file:
    data = line.split(" ")

    if "similar" in line:
        # If one of the similar files is already included in unique files, do not consider the other one unique.
        # Else, add one of them to the unique set.
        if data[1].strip(",") in unique_files and data[3].strip(",") in unique_files:
            unique_files.remove(data[1].strip(","))
        if data[1].strip(",") not in unique_files and data[3].strip(",") not in unique_files:
            unique_files.add(data[1].strip(","))
    else:
        unique_files.add(data[1].strip(","))
        unique_files.add(data[3].strip(","))

# Only copy unique files
for file in os.listdir(input_dir):
    if file in unique_files:
        subprocess.call(["cp", input_dir + "/" + file, output_dir + "/"])

comparison_file.close()
