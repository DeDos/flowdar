'''
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


Achala Rao
limit_depth.py - Takes a trace file as input and eliminates all function calls beyond a user-specified
depth. This simplified output is then written to a different file. UPenn, 2017
'''
import os
import sys
import argparse

def limit_depth(input_file, output_file, include_line_no, threshold):
	
	parity_count = 0 #keep count of total parity to ensure depth is maintained
	infile = open(input_file, "r")
	outfile = open(output_file, "w")

	line_count = 0

	depth_reached = False 

	for line in infile:
		
		line_count += 1
		
		if(depth_reached == True):
			#Ignore all messages of further depth until the return of the message is reached
			if line.startswith(message_return):
				parity_count -= 1
				if(include_line_no):
					outfile.write(line.strip("\n") + " " + str(line_count) + "\n")
				else:
					outfile.write(line)

				depth_reached = False
			continue

		msgs = line.split(" ")
		
		if msgs[0] == ">":
			parity_count += 1
			
			if(include_line_no):
				outfile.write(line.strip("\n") + " " + str(line_count) + "\n")
			else:
				outfile.write(line)			
			
			if parity_count == threshold:
				message_return = "< " + msgs[1] + " " + msgs[2]
				depth_reached = True
				continue
		
		elif msgs[0] == "<":
			parity_count -= 1
			
			if(include_line_no):
				outfile.write(line.strip("\n") + " " + str(line_count) + "\n")
			else:
				outfile.write(line)

	infile.close()
	outfile.close()
		
if __name__ == "__main__":

	parser = argparse.ArgumentParser(description = 'Limit depth of messages')

	parser.add_argument('-i',
                    help = 'Input trace file', required = True)
	parser.add_argument('-o',
                    help = 'Output trace file', required = True)
	parser.add_argument('-d',
                    help = 'Depth limit', required = True)
	parser.add_argument('--line_no', '-l',
	                    help = 'Include line numbers in the output', default = False, required = False, action = 'store_const', const = True)
	parser.add_argument('-u',
	                    help = 'Take user confirmation before deleting existing files. If option not given, files will be automatically overwritten', default = False, required = False, action = 'store_const', const = True)
	
	args = parser.parse_args()

	infile = args.i
	outfile = args.o

	if(args.u):
		if os.path.exists(outfile):
			user_input = input("Output trace file already exists. Overwrite? (y/n)")
			while user_input.lower() != "y" and user_input.lower() != "n":
				user_input = input("Please enter (y/n):")
			if user_input.lower() == "n":
				print("Exiting program..")
				sys.exit(0)

	limit_depth(infile, outfile, args.line_no, int(args.d))

        
