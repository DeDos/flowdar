The temporal analysis has been automated with a script that works as follows:

`run_termporal_analysis.py`: Calling this script with the required arguments first invokes `temporal_analysis.py` and then aggregates the results. It runs the temporal analysis between all good and bad trace files.

Usage: `python3 run_temporal_analysis.py <SA_directory> <TA_directory> <results_filename> <num_div_points> <threshold>`

    SA_directory: path to the directory where the final results of the structural analyzer are saved
    TA_directory: path to the directory to save the results of the temporal analyzer
    results_filename: filename to save the result of the temporal analyzer
    num_div_points: number of divergence points to take into consideration in each comparison
    threshold: the minimum significant time difference (in microsecond)


`temporal_analysis.py` is called by `run_temporal_analysis.py`. It takes the following parameters:

Usage: `python3 temporal_analysis.py <SA_output> <TA_output> threshold`

    SA_output: path to the (merged) output from the structural analyzer
    TA_output: path where result will be saved
    threshold: the minimum significant time difference (in microsecond)

Each line in `<TA_output>` has the following format:
`>< caller callee time_diff time_good time_bad line_good line_bad origin_file`

A usage example for this automated script has been demonstrated in usage_example_temporal.py.
