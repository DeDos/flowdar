These scripts can be used to convert a generated trace file into a sequence diagram containing the functions as threads and the exchange of messages between them as arrows between these threads, pointing in the direction of the call. 
There are a few pre-processing tools to validate the traces as well as simplify the traces by limiting the depth of function calls. 
Ideally you would invoke the group_messages.py file to group messages based on recurring patterns in the trace file and generate a new trace. Alternatively, you can invoke the time_efficient_grouping.py file which only looks for patterns beginning from a function belonging to a set of top k frequently occurring functions. Finally, you call the log_to_pdf.py file to convert this trace to an easily analyzable UML sequence diagram, stored in PDF format.

More details about the scripts and their parameters are listed below:	

Pre-processing scripts
===

**limit_depth.py:**

Limits the depth of the function calls in the trace. Takes a trace file as input as well as the threshold depth. Any function calls beyond the specified depth are ignored by the tool until the return for the threshold depth is reached. 

	Usage: python3 limit_depth.py -i <input_trace_file> -o <output_trace_file> -d <threshold_depth> < -l > < -u > 


	input_trace_file: This parameter specifies the file path of the generated trace file whose depth of function calls have to be limited.
	
	output_trace_file: This parameter specifies where the user wants the new trace file to be written. 

	threshold_depth: This parameter specifies at what depth to stop tracing function calls and their returns. 

	-l or --line_no: Flag to include line numbers in the output

	-u: Flag to take user confirmation before deleting existing files. If option not given, files will be automatically overwritten.

validate_traces.py:

Tool to validate a generated trace file by keeping track of calls and returns and ensuring the right order is followed. The output of the tool specifies if a call has not been returned or a return does not have a corresponding call in the trace. 

Usage: `python3 validate_traces.py <input_trace_file>`

A sample output of this module would look something like this:

		This call violates the flow of the thread: Line 3:fn10 fn11

		This return violates the flow of the thread: Line 5:fn12 fn13

		These functions were called but they never returned
		Line 31:fn1 fn2

		These messages were returned but never called
		Line 41:fn3 fn4 

		Stack not empty, these functions were called but did not return
		Line 51:fn5 fn6

Note: When a message appears out-of-the-blue, it is recorded as a violation, but the subsequent messages are always validated using their previous message as reference. 

**validator.py**

Takes a folder name as input and calls the function "validate_trace" in validate_traces.py on each of the files. The output is then written to a file.

Usage: `python3 validator.py <input_trace_folder> <output_file>`

Cases covered by the validator:
	
	1. a function call never returns -- in this case, the output indicates that the call has not returned.
	For e.g.,
	n   : > a b  (where n is the call depth)
	n+1 : > b c
	...
  	but we don't have "n : < a b" when we reach depth "n" again in the future.

	2. a function return was never called -- here the output indicates that this return does not have a corresponding call.
	For e.g.,
	  ...
	  n   : < a b
	  but we didn't have "n : > a b" prior

	3. a call "out of the blue" (maybe from a different thread). In this case, the output indicates that this call appears out of the flow of this thread.
	For e.g.,
	  n   : > a b
	  n+1 : > c d

	4. a return "out of the blue" (maybe from a different thread). In this case, the output indicates that this return appears out of the flow of this thread.
	For e.g.,
	  n   : > a b
	  n+1 : < c d

Scripts to convert a trace file to a sequence diagram:
===

**process_function_naming.py:**

Making changes to a trace file in order to remove unnecessary information from function names and make them latex-friendly in a way which can be then fed into log_to_pdf.py

Usage: `python3 process_function_naming.py -i <input_trace_file> -o <output_trace_file> [--replace_underscores] [--remove_offset]`

	input_trace_file: This parameter specifies the file path of the original trace file
	
	output_trace_file: This parameter specifies where the user wants the new trace file to be written. 

	--replace_underscores: If this flag is set, all of the occurrences of "_" will be replaced by "\string_".

	--remove_offset: If this flag is set, all offset information will be removed from messages.

**group_messages.py**
	
Takes a trace file as input and generates a new trace file with messages grouped based on recurring patterns. The window size for these patterns must be specified as a parameter to this file. The program iteratively finds message patterns of smaller window sizes as well.

Usage: `python3 group_messages.py <window_size> <input_trace_file> <output_trace_file>`

	window_size: This is the number of messages in a group which can repeat a number of times.

	input_trace_file: This parameter specifies the input trace file to the program.

	output_trace_file: This parameter specifies the name of the new trace file to be generated.

**time_efficient_grouping.py:**
	
Takes a trace file as input along with a parameter specifying what fraction of the functions to consider. The module then finds the top k frequently occuring functions based on the fraction specified and tries to find patterns in the trace file only if they begin with one of the functions in the top set. This method allows the user to achieve results that may not be 100% complete, but in a reasonably shorter period of time as compared to the regular grouping. In most experiments, this module manages to achieve 100% accuracy with the fraction size set to 0.3, and does much better than the regular grouping algorithm with regard to time-efficiency.

Usage: `python3 time_efficient_grouping.py --input_trace [-i] <Input trace file> --output_trace [-o] <Output trace file> --window_size [-w] <Window size to look for patterns> [--display_functions -d <Print the top functions to screen>] [--fraction_func -k <fraction of functions>]`

	input_trace(-i): Input trace file 
	
	output_trace(-o): Output trace file

	window_size(-w): Required option that specifies window_size in which to look for patterns.

	display_functions(-d): Print the top k fraction of frequently occurring functions to the console.

	fraction_func(-k): Floating point value - fraction of top frequently occurring functions to consider while looking for patterns. Default value = 0.3.

**log_to_pdf.py:**

Takes a trace file as input and produces a PDF with the trace represented as UML sequence diagrams. Makes use of the pgf-umlsd package in pdflatex to achieve this. 

Usage: `python3 log_to_pdf.py --logfile <trace_file> [--batch_size -b <size_of_each_message_batch>] [--max_interval <Time interval in micro seconds to be marked in the diagram>] [--group_messages -g <option to group recurring patterns into one>] [-s <save_temporary_files>] [-l <limit_on_the_number_of_functions_in_the_PDF>] [--display_latex -d <Print the output of LaTeX to console>]`

	logfile: This is the trace file to be represented as a sequence diagram. 

	batch_size: The messages are divided up into fixed size batches specified by this parameter. Its default value is 400.

	max_interval: Specifies the threshold duration (in microseconds) between two messages. For every such interval, a red dot is placed between the messages to indicate the passage of threshold duration of time. If the duration exceeds 5 times the threshold, a blue dot is placed at the top of the 5 red dots to indicate longer passge of time than indicated.The default value is 500 microseconds. 

	group_messages: This option allows the user to choose if he requires the output sequence diagram to contain groupings of patterns or not. The default value is True.
	
	s: Specifies if the user wants to retain the temporary files created during execution. If this parameter is set to True, the path of the directory containing these temporary files is displayed at the end of execution of the program. Its default value is False.

	l: This parameter can be used by the user to specify a limit on the number of functions to be seen in one PDF. This is primarily because Latex cannot accomodate these functions in a single PDF after a certain width is achieved. 

	display_latex: This flag lets the user opt to view the output of the latex command on the console. 

The new trace file generated from the example trace file can be used as input to this script to view its behaviour and output.

