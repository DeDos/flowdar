'''
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


Achala Rao 
compare_traces.py - Compares all trace files in a directory with each other and outputs the ratio of the number of
similar to different lines for each pair of files.
UPenn, 2017 

Basic definitions:
similar lines: Lines that are identical to each other w.r.t the first three columns 
similar files: All lines in one file are identical to that of the other file w.r.t the first three columns in both
(Side by side comparison)
All comparisons are made w.r.t the first three columns(in lines and files)
'''

import itertools
import os
import sys
import subprocess
import math
import tempfile

# If two files differ in size by more than COMPARISON_FILESIZE_CUTOFF bytes then we don't continue the analysis.
COMPARISON_FILESIZE_CUTOFF = 1000

input_dir = sys.argv[1]
output_file = sys.argv[2]

if os.path.exists(output_file):
    user_input = raw_input("Output file already exists. Overwrite? (y/n)")
    while user_input.lower() != "y" and user_input.lower() != "n":
        user_input = raw_input("Please enter (y/n):")
    if user_input.lower() == "n":
        print("Exiting program..")
        sys.exit(0)

file_list = os.listdir(input_dir)
# Dictionary containing the output ratios for tuples of files that are different or the number of similar lines for
# similar files. The tuple of files can be either similar or not. If they are similar, their dict value indicates the
#  number of similar lines between the two files. Else, it indicates the ratio of similar to different lines between
# the two files. To distinguish between these two types of values while generating the output, the value indicating
# the number of similar lines is marked with a negative sign. For example, a positive dictionary value of 2.3 for a
# tuple of files, say (f1, f2), indicates that the ratio of number of similar to number of different lines between f1
#  and f2 is 2.3. Instead, if (f1, f2) had a negative value of -25, it would indicate that f1 and f2 both have 25
# lines and all of them are similar.
output_dict = {}
# List of categories of files. Each category(pool) contains identical files.
pools = []
outfile = open(output_file, "w")
# Pick pairs of files with each pair occurring only once
for f1, f2 in itertools.combinations(file_list, 2):
    if f1 == ".DS_Store" or f2 == ".DS_Store":
        continue

    match_found = False
    # Iterate through all pools in the list to check if the files being compared is present in one of the pools. If
    # so, check if a comparison between the other file and the files in the pool exists. If so, use that value.
    for file_set in pools:
        if f1 in file_set:
            for files in file_set:
                if (f2, files) in output_dict:
                    # A positive dict value indicates a ratio between number of similar lines to number of different
                    # lines between two files which have some differences.
                    # A negative dict value indicates that the two files are similar and the absolute value is equal to
                    # the number of lines in each file.
                    # If there was no distinction between the above values, we would have no way to classify two files
                    # as similar or different when compared. Every value in the dictionary would be interpreted as a
                    # ratio between the number of similar to different lines in the two files being compared.
                    # As a result, the output for the comparison of two different files would be
                    # "..sim/diff ratio = {dict_value}" and the output for two similar files being compared would also
                    # be "..sim/diff ratio = {dict_value}" as opposed to "..all lines similar(dict_value)".
                    # This would cause the module eliminate_redundant_traces.py to consider two similar files as
                    # different(since the module looks for the word "similar" in the comparison output between two files
                    #  to conclude that they are similar) and hence not eliminate redundant files.
                    # To distinguish between a value being a ratio and a value being the number of lines, the values
                    # indicating number of lines are marked with a negative sign.

                    if output_dict[f2, files] < 0:
                        outfile.write("File1: " + f1 + ", File2: " + f2 + ", all lines similar (" + str(
                            math.fabs(output_dict[f2, files])) + ")" + "\n")
                    else:
                        outfile.write("File1: " + f1 + ", File2: " + f2 + ", sim/diff ratio = " + str(
                            output_dict[f2, files]) + "\n")
                    match_found = True
                    break

        if not match_found:
            if f2 in file_set:
                for files in file_set:
                    if (f1, files) in output_dict:
                        if output_dict[f1, files] < 0:
                            outfile.write("File1: " + f1 + ", File2: " + f2 + ", all lines similar (" + str(
                                math.fabs(output_dict[f1, files])) + ")" + "\n")
                        else:
                            outfile.write("File1: " + f1 + ", File2: " + f2 + ", sim/diff ratio = " + str(
                                output_dict[f1, files]) + "\n")
                        match_found = True
                        break

    if match_found:
        continue

    # Compare files only if the difference in their sizes is below 1kb to avoid unnecessary comparison. If not,
    # declare as different.
    f1_size = os.stat(input_dir + "/" + f1).st_size
    f2_size = os.stat(input_dir + "/" + f2).st_size

    if math.fabs(f1_size - f2_size) > COMPARISON_FILESIZE_CUTOFF:
        outfile.write("File1: " + f1 + ", File2: " + f2 + ", not compared since their sizes differ by more than " + str(
            COMPARISON_FILESIZE_CUTOFF) + " bytes\n")
        continue

    # diff -y gives side by side comparison of files. Since only the first 3 columns of each file need to be
    # compared, these are extracted from the files using awk. Store the results in a temporary file which gets
    # overwritten after every iteration

    result_file = tempfile.NamedTemporaryFile(prefix='result_', delete=False)

    cmd = "diff -y <( awk '{print $1,$2,$3}' " + input_dir + "/" + f1 + " ) <( awk '{print $1,$2,$3}' " + input_dir + "/" + f2 + " )"

    with open(result_file.name, 'w') as results:
        subprocess.call(['/bin/bash', '-c', cmd], stdout=results)

    differences = 0
    similarities = 0

    file = open(result_file.name, "r")

    for line in file:
        if "begin\n" in line or "end\n" in line or "\\path" in line or "\\draw" in line or "\\postlevel" in line:
            continue

        cols = line.split()
        # If there are 7 columns, then there exists a line in both files which differ from each other.
        if len(cols) == 7:
            differences += 2
        # If there are 6 columns, the lines are displayed side by side with a white space in between, implying that
        # they are identical.
        elif len(cols) == 6:
            similarities += 1
        # If there are 4 columns, it implies that there are no more lines left to read in one of the files and only
        # the line from one file is displayed.
        elif len(cols) == 4:
            differences += 1

    # If the files are similar, add them to an existing pool or if no pool exists, create a new one
    if differences == 0:
        outfile.write("File1: " + f1 + ", File2: " + f2 + ", all lines similar (" + str(similarities) + ")" + "\n")
        # Store negative values for number of similar lines to differentiate from ratio between similar to different
        # lines
        output_dict[f1, f2] = output_dict[f2, f1] = -1 * similarities

        found = False
        for item in pools:
            if f1 in item and f2 not in item:
                found = True
                item.add(f2)
                break
            elif f1 not in item and f2 in item:
                found = True
                item.add(f1)
                break

        if not found:
            new_file_pool = set()
            new_file_pool.add(f1)
            new_file_pool.add(f2)
            pools.append(new_file_pool)

    else:
        outfile.write(
            "File1: " + f1 + ", File2: " + f2 + ", sim/diff ratio = " + str(similarities / float(differences)) + "\n")
        output_dict[f1, f2] = output_dict[f2, f1] = similarities / float(differences)

    file.close()
