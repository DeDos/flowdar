'''
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


Achala Rao 
single_file_script.py - Script that runs the three modules to convert a trace to a sequence diagram on a single file.
UPenn, 2017 
'''
import subprocess
import sys
import os
import argparse

parser = argparse.ArgumentParser(description = 'Script to invoke all modules required to convert a trace file to a sequence diagram')

parser.add_argument('-i',
                help = 'Input trace file', required = True)
parser.add_argument('-e', required = True,
                help = 'Object file to be used to convert addresses to function names')
parser.add_argument('-w', default = "10",
                help = 'Window size of the messages to be grouped')
parser.add_argument('-t', default = False,
                help = 'Turn off time-efficient grouping of messages by using top k functions instead of smooth sliding window', required = False, action = 'store_const', const = True)
parser.add_argument('-k', default = "0.3",
                help = 'Fraction of functions to be used for time-efficient grouping')
parser.add_argument('-o', required = True,
                help = 'Output file name')
parser.add_argument('-s', required = False,
                help = 'Save temporary files', default = False, action = 'store_const', const = True)


args = parser.parse_args()

subprocess.call(["python3", "addr_to_func_name.py", "-e", args.e, "-i", args.i, "-o", "temp_" + args.o])
if args.t == True:
	subprocess.call(["python3", "group_messages.py", args.w, args.i, args.o])
else:
	subprocess.call(["python3", "time_efficient_grouping.py", "-i", "temp_" + args.o, "-o", args.o, "-w", args.w, "-k", args.k])
subprocess.call(["python3", "log_to_pdf.py", "--logfile", args.o])

if args.s == False:
	os.remove("temp_" + args.o)
	os.remove(args.o)
