'''
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


Achala Rao 
validate_traces.py - Takes a trace file as input and validates it to check if all function calls and returns are in order.
Produces a report of the discrepancies. Uses "-" as a special delimiter to keep track of the line numbers of the messages pushed onto the stack. 
This delimiter has no effect on the input or output of the script. The script makes an assumption that the special delimiter "-" does not appear
in the original trace text and will need to be updated if a situation arises where the "-" appears in the caller/callee text in the original trace.
UPenn, 2017 
'''
import sys


class Stack:
    def __init__(self):
        self.stack = []

    def push(self, item):
        self.stack.append(item)

    def pop(self):
        return self.stack.pop()

    def isEmpty(self):
        return len(self.stack) == 0
    
    def peek(self):
        return self.stack[len(self.stack) - 1]

    def display(self):
        for i in range(len(self.stack), 0, -1):
            print("Line " + self.stack[i - 1].split("-")[1] + ": " + self.stack[i - 1].split("-")[0])


class Queue:
    def __init__(self):
        self.queue = []

    def isEmpty(self):
        return len(self.queue) == 0

    def enqueue(self, item):
        self.queue.insert(0, item)

    def dequeue(self):
        return self.queue.pop()

    def display(self):
        for i in range(len(self.queue), 0, -1):
            print("Line " + self.queue[i - 1].split("-")[1] + ": " + self.queue[i - 1].split("-")[0])

def validate_trace(input_file):
    infile = open(input_file, "r")
    call_dict = {}  # to store the parity of a pair of functions
    s = Stack()
    excess_return_queue = Queue()  # to store calls that are never called but returned
    excess_call_queue = Queue()  # to store calls that never return
    previous_line = ""  # Keeps track of the previous call/return that was in the flow of the thread
    line_no = 0

    for line in infile:
        line_no += 1
        if line == "\n":
            continue

        words = line.split(" ")

        # If the present call or return does not originate from the end point of the previous message, the message is
        #  violating the flow of the thread
        if previous_line != "":

            if previous_line.split(" ")[0] == ">":
                if words[0] == ">":
                    if words[1].split(":")[0] != previous_line.split(" ")[2].split(":")[0]:
                        print(
                            "This call violates the flow of the thread: Line " + str(line_no) + ": " + words[1] + " " +
                            words[2])
                        previous_line = words[0] + " " + words[1].split(":")[0] + " " + words[2].split(":")[0]
                        continue

            elif previous_line.split(" ")[0] == "<":
                if words[0] == ">":
                    if words[1].split(":")[0] != previous_line.split(" ")[1].split(":")[0]:
                        print(
                            "This call violates the flow of the thread: Line " + str(line_no) + ": " + words[1] + " " +
                            words[2])
                        previous_line = words[0] + " " + words[1].split(":")[0] + " " + words[2].split(":")[0]
                        continue

            else:
                print("Sorry, first character in line not recognized: " + previous_line.split(" ")[
                    0] + ". Expected '>'(call) or '<'(return).")
                sys.exit(1)

        # If the message is a function call, push to stack and continue
        if line.startswith(">"):
            s.push(words[1].split(":")[0] + " " + words[2].split(":")[0] + "-" + str(line_no))

            if (words[1].split(":")[0] + " " + words[2].split(":")[0]) in call_dict:
                call_dict[words[1].split(":")[0] + " " + words[2].split(":")[0]] += 1
            else:
                call_dict[words[1].split(":")[0] + " " + words[2].split(":")[0]] = 1

        elif line.startswith("<"):
            # If return message encountered when stack empty, it means the call to this function is missing.
            if s.isEmpty():
                # If call violates continuity of the thread, show message to indicate the violation
                if previous_line != "":
                    if previous_line.split(" ")[0] == ">":
                        if words[2].split(":")[0] != previous_line.split(" ")[2].split(":")[0]:
                            print("This return violates the flow of the thread: Line " + str(line_no) + ": " + words[
                                1] + " " + words[2])
                            previous_line = words[0] + " " + words[1].split(":")[0] + " " + words[2].split(":")[0]
                            continue
                    elif previous_line.split(" ")[0] == "<":
                        if words[2].split(":")[0] != previous_line.split(" ")[1].split(":")[0]:
                            print("This return violates the flow of the thread: Line " + str(line_no) + ": " + words[
                                1] + " " + words[2])
                            previous_line = words[0] + " " + words[1].split(":")[0] + " " + words[2].split(":")[0]
                            continue
                    else:
                        print("Sorry, first character in line not recognized: " + previous_line.split(" ")[
                            0] + ". Expected '>'(call) or '<'(return).")
                        sys.exit(1)

                excess_return_queue.enqueue(words[1].split(":")[0] + " " + words[2].split(":")[0] + "-" + str(line_no))
                previous_line = words[0] + " " + words[1].split(":")[0] + " " + words[2].split(":")[0]
                continue

            popped = s.peek()

            # If return message is as expected, decrement parity and continue
            popped_words = popped.split("-")[0]
            if popped_words == (words[1].split(":")[0] + " " + words[2].split(":")[0]):
                call_dict[words[1].split(":")[0] + " " + words[2].split(":")[0]] -= 1
                s.pop()

            else:
                # If return message encountered when call not present, record and continue. If call violates
                # continuity of the thread, display message, else add to excess return queue
                if ((words[1].split(":")[0] + " " + words[2].split(":")[0]) not in call_dict) or (call_dict[words[1].split(":")[0] + " " + words[2].split(":")[0]] == 0):

                    if previous_line.split(" ")[0] == ">":
                        if words[2].split(":")[0] != previous_line.split(" ")[2].split(":")[0]:
                            print("This return violates the flow of the thread: Line " + str(line_no) + ": " + words[
                                1] + " " + words[2])
                            previous_line = words[0] + " " + words[1].split(":")[0] + " " + words[2].split(":")[0]
                            continue
                    elif previous_line.split(" ")[0] == "<":
                        if words[2].split(":")[0] != previous_line.split(" ")[1].split(":")[0]:
                            print("This return violates the flow of the thread: Line " + str(line_no) + ": " + words[
                                1] + " " + words[2])
                            previous_line = words[0] + " " + words[1].split(":")[0] + " " + words[2].split(":")[0]
                            continue
                    else:
                        print("Sorry, first character in line not recognized: " + previous_line.split(" ")[
                            0] + ". Expected '>'(call) or '<'(return).")
                        sys.exit(1)

                    excess_return_queue.enqueue(words[1].split(":")[0] + " " + words[2].split(":")[0] + "-" + str(line_no))
                    previous_line = words[0] + " " + words[1].split(":")[0] + " " + words[2].split(":")[0]
                    continue

                # Call to this return message is present somewhere in the stack, keep popping until the call is found.
                else:
                    s.pop()

                    while popped_words != (words[1].split(":")[0] + " " + words[2].split(":")[0]):
                        excess_call_queue.enqueue(popped)
                        popped = s.pop()
                        popped_words = popped.split("-")[0]

                    call_dict[words[1].split(":")[0] + " " + words[2].split(":")[0]] -= 1

        else:
            print("Sorry, first character in line not recognized: " + line.split(" ")[
                0] + ". Expected '>'(call) or '<'(return).")
            sys.exit(1)

        previous_line = words[0] + " " + words[1].split(":")[0] + " " + words[2].split(":")[0]

    if not excess_call_queue.isEmpty():
        print("\nThese functions were called but they never returned")
        excess_call_queue.display()

    if not excess_return_queue.isEmpty():
        print("\nThese messages were returned but never called")
        excess_return_queue.display()

    if not s.isEmpty():
        print("\nStack not empty, these functions were called but did not return")
        s.display()

    infile.close()


if __name__ == "__main__":
    input_file = sys.argv[1]
    validate_trace(input_file)
