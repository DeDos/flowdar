#!/usr/bin/env python
#
#Copyright (c) 2019, University of Pennsylvania
#All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the <organization> nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
#DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#
# Remove offsets from a trace file and/or replaces latex-unfriendly characters
# Pardis Pashakhanloo, UPenn, January 2018

import os
import sys
import argparse

CALLER, CALLEE = 1, 2

parser = argparse.ArgumentParser(description='Changing a trace file to make it LaTeX-friendly')

parser.add_argument('-i',
                    help='Input trace file name', required=True)
parser.add_argument('-o',
                    help='Output trace file name', required=True)
parser.add_argument('--remove_offset',
                    help='remove function offsets from each line', action='store_true', required=False)
parser.add_argument('--replace_underscores',
                    help='replace each _ with \string_ which is recognizable by latex', action='store_true', required=False)

args = parser.parse_args()

if os.path.exists(args.o):
    user_input = input("Output trace file already exists. Overwrite? (y/n)")

    while user_input.lower() != "y" and user_input.lower() != "n":
        user_input = input("Please enter (y/n):")

    if user_input.lower() == "n":
        print("Exiting program..")
        sys.exit(0)

with open(args.i) as f_input:
    with open(args.o, 'w+') as f_output:
        for line in f_input:
            if args.replace_underscores:
                # Latex treats "_" as a special character. To avoid that, replace "_" with "\string_"
                line = line.replace("_", "\\string_")
            if args.remove_offset:
                message = line.split()
                # remove anything after the colon, namely the offset
                message[CALLER] = (message[CALLER].split(":"))[0]
                message[CALLEE] = (message[CALLEE].split(":"))[0]
                line = " ".join(message) + '\n'

            f_output.write(line)
