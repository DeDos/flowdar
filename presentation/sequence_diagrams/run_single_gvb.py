#!/usr/bin/env python
#
#Copyright (c) 2019, University of Pennsylvania
#All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the <organization> nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
#DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#
# Runs the toolchain on a specific good and bad trace file in order to find major points of divergence
# Pardis Pashakhanloo, UPenn, February 2018

import sys
import subprocess
from check_equal_diff import isEqual


def run_single_gvb(good_file, bad_file, output_folder, depth, threshold):
    good_filename = good_file.split("/")[-1]
    bad_filename = bad_file.split("/")[-1]

    subprocess.call(["python3", "limit_depth.py", "-i", good_file, "-l", "-o", output_folder + "/lim_" + good_filename, "-d", str(depth)])
    subprocess.call(["python3", "limit_depth.py", "-i", bad_file, "-l", "-o", output_folder + "/lim_" + bad_filename, "-d", str(depth)])

    with open(output_folder + "/diff_" + good_filename + bad_filename, 'w') as file1:
        subprocess.call([
                            "bash exec_diff.sh " + output_folder + "/lim_" + good_filename + " " + output_folder + "/lim_" + bad_filename],
                        shell=True, stdout=file1)


    diff_filename = output_folder + "/diff_" + good_filename + bad_filename
    lim_good = output_folder + "/lim_" + good_filename
    lim_bad = output_folder + "/lim_" + bad_filename
    pure_good = output_folder + "/pure_" + good_filename
    pure_bad = output_folder + "/pure_" + bad_filename

    subprocess.call(["python3", "filter_messages.py", diff_filename, lim_good, lim_bad, pure_good, pure_bad])
    SA_output = output_folder + "/SA_" + good_filename + "-" + bad_filename
    subprocess.call(["python3", "format_messages.py", pure_good, pure_bad, SA_output])

    TA_output = output_folder + "/TA_" + good_filename + "-" + bad_filename
    subprocess.call(["python3", "temporal_analysis.py", SA_output, TA_output, threshold])

    return True

if __name__ == "__main__":
    good_file = sys.argv[1]
    bad_file = sys.argv[2]
    output_folder = sys.argv[3]
    depth = sys.argv[4]

    threshold = sys.argv[5]

    res = run_single_gvb(good_file, bad_file, output_folder, str(depth), str(threshold))

    print("Compared trace files " + good_file.split("/")[-1] + " and " + bad_file.split("/")[-1] + " at depth " + str(
        depth))
