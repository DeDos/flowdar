'''
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


Achala Rao 
structural_analysis.py - Filter out messages that are not of significance in the comparison between good and bad traces.
UPenn, 2017 
'''
import linecache
import sys

def get_requests(infile, s_direction, s_caller, s_callee, e_direction, e_caller, e_callee):
	
	requests = {}
	r_count = 1
	line_no = 1
	break_flag = False
	linecache.clearcache()

	while True:

		line = linecache.getline(infile, line_no)
		if line == "":
			break

		words = line.split(" ")
		#Only consider the messages that begin with the call to the start marker upto the call to the end marker, since this is where the thread is servicing a request
		if(words[0] == s_direction and words[1].split(":")[0] == s_caller and words[2].split(":")[0] == s_callee):
			begin = int(words[14].strip())
			line_begin = line_no

			while(not (words[0] == e_direction and words[1].split(":")[0] == e_caller and words[2].split(":")[0] == e_callee)):
				line_no += 1
				line = linecache.getline(infile, line_no)
				if line == "":
					break_flag = True
					break
				words = line.split(" ")

			if not break_flag:

				end = int(words[9].strip())
				line_end = line_no
				requests[r_count] = ((begin, end), (line_begin, line_end))
				r_count += 1
			
		else:
			line_no += 1
	
	return requests

#Function to write the request in the argument to a file
def write_request_to_file(request, filename, infile):

	outfile = open(filename, "w")
	for line_no in range(request[1][0], request[1][1] + 1):
		outfile.write(linecache.getline(infile, line_no))

	outfile.close()


if __name__ == "__main__":

	infile = sys.argv[1]

	#Information about which message to consider as beginning of a request
	s_direction = sys.argv[3] 
	s_caller = sys.argv[4]
	s_callee = sys.argv[5]

	#Information about which message to consider as end of a request
	e_direction = sys.argv[6] 
	e_caller = sys.argv[7]
	e_callee = sys.argv[8]

	requests = get_requests(infile, s_direction, s_caller, s_callee, e_direction, e_caller, e_callee)

	if len(requests) == 0:
		print("There are no requests made in this trace file")
		sys.exit()

	print("Here are the line numbers of the requests in the file : " + infile + ". Format: req_no (begin_line_no, end_line_no)")
	
	for item in requests:
		print(item, " ", requests[item][0])

	req_no = int(input("Pick the request number to be considered for the analysis: "))

	while req_no > len(requests):
		req_no = int(input("Kindly enter the number from the provided options: "))

	write_request_to_file(requests[req_no], sys.argv[2], infile)
	
