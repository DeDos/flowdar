'''
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


Achala Rao 
segregate_workers.py - Separate out only worker threads into a separate folder. 
This folder will be the input to the structural analysis toolchain.
UPenn, 2018 
'''
import os
from shutil import copy
import sys
import re


def segregate_workers(file_loc, input_folder, worker_folder, s_direction, s_caller, s_callee, e_direction, e_caller, e_callee):

	list_file = open(file_loc, "r")
	workers = set()

	if not os.path.exists(worker_folder):
		os.mkdir(worker_folder)

	#Read all worker filenames into a set. 
	for line in list_file:
		workers.add(line.split("/")[-1].strip("\n"))

	list_file.close()

	#Only copy worker traces into a separate folder 
	for file in os.listdir(input_folder):
		if file in workers:
			result = contains_request(input_folder + "/" + file, s_direction, s_caller, s_callee, e_direction, e_caller, e_callee)
			if result:
				copy(input_folder + "/" + file, worker_folder)


def contains_request(infile, s_direction, s_caller, s_callee, e_direction, e_caller, e_callee):

	f = open(infile, "r")
	count = 0

	regex_string_begin = s_direction + ".*" + s_caller + ".*" + s_callee + ".*"
	regex_string_end = e_direction + ".*" + e_caller + ".*" + e_callee + ".*"
	
	for line in f:
		

		if count == 0:
			regexp = re.compile(regex_string_begin)	
			if regexp.search(line):
				count += 1
		
		elif count == 1:

			regexp = re.compile(regex_string_end)	
			if regexp.search(line):
				count += 1

		if count == 2:
				return True

	f.close()
	return False
		
if __name__ == "__main__":

	file_loc = sys.argv[1]
	input_folder = sys.argv[2]
	worker_folder = sys.argv[3]

	#Information about which message to consider as beginning of a request
	s_direction = sys.argv[4] 
	s_caller = sys.argv[5]
	s_callee = sys.argv[6]

	#Information about which message to consider as end of a request
	e_direction = sys.argv[7] 
	e_caller = sys.argv[8]
	e_callee = sys.argv[9]

	segregate_workers(file_loc, input_folder, worker_folder, s_direction, s_caller, s_callee, e_direction, e_caller, e_callee)

