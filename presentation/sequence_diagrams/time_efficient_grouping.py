'''
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


Achala Rao
time_efficient_grouping.py - Groups repetitive messages by picking the top frequently occurring
functions and looking for patterns beginning from those functions. Users can specify a window_size in which to look
for a pattern. This is an optimized version of group_messages.py which uses a smooth sliding window approach and
looks at every recurring group of messages. This improvised algorithm was formulated due to the significantly long
running time of the naive algorithm.
UPenn, 2017
'''
import operator
import subprocess
import tempfile
import linecache
import argparse


# Function to iteratively decrease the window size and invoke the grouping function
def modify_trace(functions, output_file, window_size):
    while window_size > 1:
        temp_output_file = tempfile.NamedTemporaryFile(prefix='temp_', delete=False)
        group_by_pattern(int(window_size), output_file, temp_output_file.name, functions)
        subprocess.call(["mv", temp_output_file.name, output_file])
        window_size -= 1


# Function to make grouping of messages time-efficient by only considering the top few functions
# and find patterns starting from these functions.
def group_by_pattern(window_size, input_file, output_file, functions):
    # Keeps track of the line number to be read
    line_pointer = 1
    # Stores the pattern to be compared (just the first 3 columns of the trace)
    pattern = ""
    # Stores the entire pattern line corresponding to the pattern being compared
    pattern_lines = ""
    outfile = open(output_file, "w")
    # Count of number of times the pattern has repeated
    pattern_count = 0
    eof = False
    # Flag to find if an existing pattern is found while looking for a new pattern. (Can happen in
    # successive iterations of the program when a pattern of a higher window size has already been found)
    existing_pattern = False
    # clear cache for every call
    linecache.clearcache()

    while True:
        # Reset the lines variable for every iteration
        lines = ""
        lines_to_compare = ""
        # If a new pattern has to be found
        if pattern_count == 0:
            line = linecache.getline(input_file, line_pointer)
            if line == "":
                break
            fns = line.split(" ")
            # If an existing pattern already in file, do not parse, write to output directly
            if fns[1] == "begin\n":

                while line != "end\n":
                    outfile.write(line)
                    line_pointer += 1
                    line = linecache.getline(input_file, line_pointer)

                outfile.write(line)
                line_pointer += 1
                continue

            # Only if one of the top functions is present in this message exchange
            elif fns[1] in functions or fns[2] in functions:
                for i in range(line_pointer, line_pointer + window_size):
                    line = linecache.getline(input_file, i)

                    # End of file reached. getline returns empty line after EOF reached.
                    if line == "":
                        if pattern_lines != "":
                            outfile.write(pattern_lines)
                        eof = True
                        break

                    # If an existing pattern already in file, do not parse, write to output directly
                    if line.split(" ")[1] == "begin\n":
                        if pattern_lines != "":
                            outfile.write(pattern_lines)

                        while line != "end\n":
                            outfile.write(line)
                            i += 1
                            line = linecache.getline(input_file, i)

                        outfile.write(line)
                        line_pointer = i + 1
                        existing_pattern = True
                        break

                    else:
                        # the first string contains just the columns to be compared. The second string contains the
                        # entire line to be written to file. The first 3 columns contain the direction of the message
                        #  as well as the functions that are communicating.
                        pattern += " ".join(line.split()[:3])
                        pattern_lines += line

                # Reset pattern and its count if an existing pattern from previous iteration exists
                if existing_pattern:
                    pattern = ""
                    pattern_lines = ""
                    pattern_count = 0
                    existing_pattern = False
                    continue

                # Pattern observed. Move window ahead by window_size
                line_pointer += window_size

                if eof:
                    break

            else:
                outfile.write(line)
                line_pointer += 1
                continue

        # Read the next few lines based on window size and compare to pattern
        for i in range(line_pointer, line_pointer + window_size):

            line = linecache.getline(input_file, i)
            # EOF reached
            if line == "":
                if pattern_lines != "":
                    if pattern_count != 0:
                        outfile.write(str(pattern_count + 1) + " begin\n" + pattern_lines + "end\n")
                    else:
                        outfile.write(pattern_lines)

                if lines != "":
                    outfile.write(lines)

                eof = True
                break

            # If pattern already exists in file, write to output directly
            if line.split(" ")[1] == "begin\n":
                if pattern_lines != "":
                    if pattern_count != 0:
                        outfile.write(str(pattern_count + 1) + " begin\n" + pattern_lines + "end\n")
                    else:
                        outfile.write(pattern_lines)

                # write any observed patterns to file
                if lines != "":
                    outfile.write(lines)

                while line != "end\n":
                    outfile.write(line)
                    i += 1
                    line = linecache.getline(input_file, i)

                outfile.write(line)
                line_pointer = i + 1
                existing_pattern = True
                break

            # The first string contains just the columns to be compared. The second string contains the entire line
            # to be written to file
            lines_to_compare += " ".join(linecache.getline(input_file, i).split()[:3])
            lines += linecache.getline(input_file, i)

        # Reset pattern and its count
        if existing_pattern:
            pattern = ""
            pattern_lines = ""
            existing_pattern = False
            pattern_count = 0
            continue

        if eof:
            break

        # If pattern repeated, increment count and move sliding window
        if pattern == lines_to_compare:
            pattern_count += 1
            line_pointer = line_pointer + window_size

        else:
            # If pattern matched some number of times, enclose this pattern within "begin" and "end" along with
            # number of repetitions
            if pattern_count > 0:
                outfile.write(str(pattern_count + 1) + " begin\n" + pattern_lines + "end\n")

            else:
                # If pattern not matched even once, move line pointer by 1 and try matching the next pattern.
                outfile.write(linecache.getline(input_file, line_pointer - window_size))
                line_pointer = line_pointer - window_size + 1

            # Reset pattern
            pattern = ""
            pattern_lines = ""
            pattern_count = 0

    outfile.close()


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Function to group messages')

    parser.add_argument('--display_functions', '-d',
                        help='Print the top functions to screen', default=False, required=False, action='store_const',
                        const=True)
    parser.add_argument('--input_trace', '-i', required=True,
                        help='Input trace file')
    parser.add_argument('--output_trace', '-o', required=True,
                        help='Output trace file')
    parser.add_argument('--window_size', '-w', required=True,
                        help='Window size to look for patterns')
    parser.add_argument('--fraction_func', '-k', required=False,
                        help='Floating point value - fraction of functions to consider while looking for patterns',
                        default="0.3")

    args = parser.parse_args()
    trace_file = args.input_trace
    trace_input = open(trace_file, "r")
    functions = {}

    for line in trace_input:
        func = line.split(" ")

        if func[1] not in functions:
            functions[func[1]] = 1
        else:
            functions[func[1]] += 1

        if func[2] not in functions:
            functions[func[2]] = 1
        else:
            functions[func[2]] += 1

    trace_input.close()

    sorted_functions = sorted(functions.items(), key=operator.itemgetter(1), reverse=True)

    if args.display_functions:
        print(sorted_functions)

    functions_list = list([i[0] for i in sorted_functions])
    # Take top k of the original number of functions in order of frequency
    part_func = int(len(functions_list) * float(args.fraction_func))
    functions_list = functions_list[:part_func]

    # Create a set from the list of top functions
    func_set = set(functions_list)
    input_file = trace_file
    output_file = args.output_trace
    window_size = int(args.window_size)

    subprocess.call(["cp", input_file, output_file])
    modify_trace(func_set, output_file, window_size)
