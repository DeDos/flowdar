#!/usr/bin/env python
#
#Copyright (c) 2019, University of Pennsylvania
#All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the <organization> nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
#DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#
# Temporal analysis of a good and a bad trace file, given a file with the format of the "merged trace"
# Pardis Pashakhanloo, UPenn, February 2018

import sys
import queue


merged_trace_location = sys.argv[1]
hotspots_location = sys.argv[2]
thresh = int(sys.argv[3])  # in microseconds

DIR, CALLER, CALLEE, TIME_SEC_GOOD, TIME_MICROSEC_GOOD, TIME_SEC_BAD, TIME_MICROSEC_BAD, LINE_GOOD, LINE_BAD = 0, 1, 2, 3, 4, 5, 6, 7, 8

NEG_TIME_DIFF, DIR_ONLY, CALLER_CALLEE, TIME_DIFF_GOOD, TIME_DIFF_BAD, LINE_BOTH, ORIGIN_FILE = 0, 1, 2, 3, 4, 5, 6

q = queue.PriorityQueue()

directions = []
messages = []
time_goods = []
time_bads = []
lines = []
origin_files = []

# Read and process the input
with open(merged_trace_location, "r") as input_file:
    for line in input_file:
        message = line.split(' ')
        directions.append(message[DIR])
        messages.append(message[CALLER] + ' ' + message[CALLEE])
        time_goods.append(int(message[TIME_MICROSEC_GOOD]) + 1000000 * int(message[TIME_SEC_GOOD]))
        time_bads.append(int(message[TIME_MICROSEC_BAD]) + 1000000 * int(message[TIME_SEC_BAD]))
        lines.append(message[LINE_GOOD] + ' ' + message[LINE_BAD])
        origin_files.append(merged_trace_location.rsplit('/', 1)[-1])

marked_finished = [False] * len(messages)

time_diffs = [0] * len(messages)


# Compute time differences and add them to the priority queue
i = 0
while True:
    if (directions[i] is '>') and not marked_finished[i]:
        for j in range(i+1, len(messages)):
            if (directions[j] is '<') and messages[i] == messages[j] and not marked_finished[j]:
                # return is found
                marked_finished[i] = True
                marked_finished[j] = True
                time_duration_good = time_goods[j] - time_goods[i]
                time_duration_bad = time_bads[j] - time_bads[i]
                time_diff = time_duration_bad - time_duration_good
                if time_diff > thresh:
                    q.put((-time_diff, '><', messages[i], time_duration_good, time_duration_bad, lines[i], origin_files[i]))
                break
    i = i + 1
    if i >= len(messages):
        break

# Prepare the output
with open(hotspots_location, 'w') as hotspots:
    while not q.empty():
        q_entry = q.get()
        # output format: dir, caller, callee, time_diff, time_good, time_bad, line_good, line_bad, origin_file
        info = [q_entry[DIR_ONLY], q_entry[CALLER_CALLEE], -q_entry[NEG_TIME_DIFF], q_entry[TIME_DIFF_GOOD], q_entry[TIME_DIFF_BAD], (q_entry[LINE_BOTH]).strip(), (q_entry[ORIGIN_FILE]).strip()]
        hotspots.write(' '.join(str(item) for item in info))
        hotspots.write('\n')
