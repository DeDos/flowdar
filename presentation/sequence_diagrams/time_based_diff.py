'''
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


Achala Rao 
temporal_analysis.py - Make a temporal comparison between good and bad traces to identify the point of major divergence. Parses the input file which is the output of the diff -y command to identify the point of major divergence. 
UPenn, 2017 
'''
import sys
import linecache
import math

file = sys.argv[1]
line_no = 1
outfile = open(sys.argv[2], "w")
time_diff = 0
prev_msg = ""
next_msg = ""

while True:

	line = linecache.getline(file, line_no)
	if line == "":
		break

	#Identify unusual passage of time between two messages by identifying the blue number indicating the amount of time passed. 
	#TODO: This is a naive approach. If color used to indicate time changes, this code will not work. Change it to detect the passage of time in a more robust way. 
	if "\path blue" in line:
		outfile.write("Line " + str(line_no - 7) + ":" + linecache.getline(file, (line_no - 7)))
		outfile.write("Line " + str(line_no) + ":" + line)
		outfile.write("Line " + str(line_no - 5) + ":" + linecache.getline(file, (line_no - 5)))
		
		#Parse the diff file to indetify and compare time taken between the good and bad traces. 
		if line.strip().endswith("<"):
			time_taken = line.split()[2]
			time_taken = time_taken.strip("[{;]")
			time_taken = float(time_taken.strip("ms}"))

			if time_taken > time_diff:
				time_diff = time_taken
				prev_msg = linecache.getline(file, (line_no - 7))
				next_msg = linecache.getline(file, (line_no - 5))

		elif line.strip().startswith(">") and "|" not in line:
			time_taken = line.split()[3]
			time_taken = time_taken.strip("[{;]")
			time_taken = float(time_taken.strip("ms}"))

			if time_taken > time_diff:
				time_diff = time_taken
				prev_msg = linecache.getline(file, (line_no - 7))
				next_msg = linecache.getline(file, (line_no - 5))

		elif "|" in line:

			time_before = 0
			time_after = 0
			line_split = line.split("|")

			if "blue" in line_split[0]:
				time_before = line.split()[2]
				time_before = time_before.strip("[{;]")
				time_before = float(time_before.strip("ms}"))
				time_taken = time_before

			if "blue" in line_split[1]:
				time_after = line.split()[6]
				time_after = time_after.strip("[{;]")
				time_after = float(time_after.strip("ms}"))
				time_taken = time_after

			if time_before != 0 and time_after != 0:
				time_taken = math.fabs(time_after - time_before)

			if time_taken > time_diff:
				time_diff = time_taken
				prev_msg = linecache.getline(file, (line_no - 7))
				next_msg = linecache.getline(file, (line_no - 5))

	line_no = line_no + 1

print "POINT OF DIVERGENCE: Between messages \n" + prev_msg + "\n" + next_msg + "and time_difference is: " + str(time_diff) + "ms"

outfile.close()




