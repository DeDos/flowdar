#!/usr/bin/env python
#
#Copyright (c) 2019, University of Pennsylvania
#All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the <organization> nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
#DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#
# Runs the temporal analysis on the result from the structrual analyzer
# Pardis Pashakhanloo, UPenn, March 2018

# python3 run_temporal_analysis.py <SA_directory> <TA_directory> <results_filename> <num_div_points> <threshold>

import os
import sys
import subprocess

SA_directory = sys.argv[1]
TA_directory = sys.argv[2]
results_filename = sys.argv[3]
num_points_div = int(sys.argv[4])
thresh = sys.argv[5]  # microseconds

i = 1

for f in os.listdir(SA_directory):
    if not ('DS_Store' in f):
        subprocess.call(["python3", "temporal_analysis.py", SA_directory + "/" + f, TA_directory + "/" + "TA_" + f, thresh])

with open(TA_directory + '/' + results_filename, 'w') as ta_result:
    for item in os.listdir(TA_directory):
        if item.startswith('TA'):
            with open(TA_directory + '/' + item, 'r') as ta_single:
                for i in range(0, num_points_div):
                    line = ta_single.readline()
                    ta_result.write(line)

for item in os.listdir(TA_directory):
    if not item == results_filename:
        os.remove(TA_directory + '/' + item)
