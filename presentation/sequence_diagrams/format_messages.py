'''
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


Achala Rao 
format_messages.py - Format the messages in the raw trace to only contain formatting options related to displaying the latency between messages. Convert raw trace into a format that can be processed by diff -y efficiently. 
UPenn, 2017 
'''
import sys
import linecache

DIRECTION = 0 #Column number of the message direction
CALLER = 1 #Column number of the caller
CALLEE = 2 #Column number of the callee
TIME_SECS = 4 #Column number of the timestamp in seconds
TIME_MICRO = 5 #Column number of the timestamp in microseconds
LINE_NO_CALL = 14 #Column number of the message line number in a message call 
LINE_NO_RET = 9 #Column number of the message line number in a message return 

infile_good = sys.argv[1]
infile_bad = sys.argv[2]
outfile = open(sys.argv[3], "w")
line_no = 1

while True:
	
	line_good = linecache.getline(infile_good, line_no)	
	line_bad = 	linecache.getline(infile_bad, line_no)

	if line_good == "" or line_bad == "":
		break

	cols_good = line_good.split(" ")
	cols_bad = line_bad.split(" ")

	if cols_good[DIRECTION] == ">":
		good_line_col = LINE_NO_CALL
	elif cols_good[DIRECTION] == "<":
		good_line_col = LINE_NO_RET


	if cols_bad[DIRECTION] == ">":
		bad_line_col = LINE_NO_CALL
	elif cols_bad[DIRECTION] == "<":
		bad_line_col = LINE_NO_RET

	outfile.write(cols_good[DIRECTION] + " " + cols_good[CALLER] + " " + cols_good[CALLEE] + " " + cols_good[TIME_SECS] + " " + cols_good[TIME_MICRO] + " " + cols_bad[TIME_SECS] + " " + cols_bad[TIME_MICRO] + " " + cols_good[good_line_col].strip("\n") + " " + cols_bad[bad_line_col].strip("\n") + "\n") 
	line_no += 1

outfile.close()
