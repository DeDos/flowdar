'''
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
'''

import sys
import queue

merged_trace_location = sys.argv[1]
hotspots_location = sys.argv[2]
thresh_low = int(sys.argv[3])  # in microseconds
thresh_high = int(sys.argv[4])

DIR, CALLER, CALLEE, TIME_SEC_GOOD, TIME_MICROSEC_GOOD, TIME_SEC_BAD, TIME_MICROSEC_BAD, LINE_GOOD, LINE_BAD\
    = 0, 1, 2, 3, 4, 5, 6, 7, 8

NEG_TIME_DIFF, MSG, TIME_DIFF_GOOD, TIME_DIFF_BAD = 0, 1, 2, 3

LINE1, LINE2 = 0, 1

q = queue.PriorityQueue()

# Compute time differences and add them to the priority queue
merged_trace = open(merged_trace_location, "r")

line1 = merged_trace.readline()

while True:

    line2 = merged_trace.readline()

    if not line2:
        break  # EOF

    message1 = line1.split()
    time_good1 = int(message1[TIME_SEC_GOOD]) * 1000000 + int(message1[TIME_MICROSEC_GOOD])
    time_bad1 = int(message1[TIME_SEC_BAD]) * 1000000 + int(message1[TIME_MICROSEC_BAD])
        
    message2 = line2.split()
    time_good2 = int(message2[TIME_SEC_GOOD]) * 1000000 + int(message2[TIME_MICROSEC_GOOD])
    time_bad2 = int(message2[TIME_SEC_BAD]) * 1000000 + int(message2[TIME_MICROSEC_BAD])

    # time difference between two consequtive lines in the merged file represents the time needed for the first line to finish its execution
    time_diff_good = time_good2 - time_good1
    time_diff_bad = time_bad2 - time_bad1
    
    # then, we can compare that between good and bad for each message
    time_diff = time_diff_bad - time_diff_good

    if time_diff > thresh_low and time_diff < thresh_high:
        q.put((-time_diff, [message1, message2], time_diff_good, time_diff_bad)) # priority queue sorted by -time_diff

    line1 = line2

merged_trace.close()

# Write points of divergence to the output file
with open(hotspots_location, 'w') as hotspots:
    i = 0
    while not q.empty():
        q_entry = q.get() # q_entry = ( -TIME_DIFFERENCE, MESSAGE )
        info = [q_entry[MSG][LINE1][DIR], q_entry[MSG][LINE1][CALLER], q_entry[MSG][LINE1][CALLEE], -q_entry[NEG_TIME_DIFF], q_entry[TIME_DIFF_GOOD], q_entry[TIME_DIFF_BAD], q_entry[MSG][LINE1][LINE_GOOD], q_entry[MSG][LINE1][LINE_BAD]]
        hotspots.write(' '.join(str(item) for item in info))
        hotspots.write('\n')

