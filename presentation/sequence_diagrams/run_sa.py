'''
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


Achala Rao 
run_sa.py - Takes as parameters a good trace file, a bad trace file and an output folder. 
Invokes the scripts necessary to carry out the structural analysis on these trace files and 
stores the output in the specified folder. 
UPenn, 2018
'''
import subprocess
import os
import sys
from check_equal_diff import isEqual
from structural_analysis import get_requests, write_request_to_file
from shutil import copy
import argparse
from filter_messages import filter_messages

#Run the structural analysis on the provided input parameters
def run_sa(good_file, bad_file, output_folder, depth, s_direction, s_caller, s_callee, e_direction, e_caller, e_callee, interactive, diff_folder, remove_differences):
	
	good_filename = good_file.split("/")[-1]
	bad_filename = bad_file.split("/")[-1]
	final_res = True
	files_to_format = [] #A list of tuples. Each tuple contains the names of one good and one bad filename to be passed to format_messages.py (To prepare them for the temporal analyzer)

	invoke_limit_depth(good_file, bad_file, output_folder, good_filename, bad_filename, depth)
	
	#Interactively obtain user input to decide which requests to pick for comparison
	if(interactive == "True"):
		subprocess.call(["python3", "structural_analysis.py", output_folder + "/lim_" + good_filename, output_folder + "/str_" + good_filename, s_direction, s_caller, s_callee, e_direction, e_caller, e_callee])
		subprocess.call(["python3", "structural_analysis.py", output_folder + "/lim_" + bad_filename, output_folder + "/str_" + bad_filename, s_direction, s_caller, s_callee, e_direction, e_caller, e_callee])
		res = is_similar(output_folder, good_filename, bad_filename)

	else:
		#Perform the analysis for every combination of good abd bad requests
		good_requests = get_requests(output_folder + "/lim_" + good_filename, s_direction, s_caller, s_callee, e_direction, e_caller, e_callee)
		bad_requests = get_requests(output_folder + "/lim_" + bad_filename, s_direction, s_caller, s_callee, e_direction, e_caller, e_callee)

		if len(good_requests) == 0 or len(bad_requests) == 0:
			print("No requests found. Not comparing traces " + good_file + " and " + bad_file)

		else:
			#Compare each request with every other request in both trace files.
			for g_req in good_requests:
				for b_req in bad_requests:

					diff_file = output_folder + "/diff_" + str(g_req) + "_" + good_filename + "_" +  str(b_req) + "_" + bad_filename
					res = False					
					while res == False and depth > 0:

						invoke_limit_depth(good_file, bad_file, output_folder, good_filename, bad_filename, depth)
						new_good_req = {}
						new_bad_req = {}
						new_good_req = get_requests(output_folder + "/lim_" + good_filename, s_direction, s_caller, s_callee, e_direction, e_caller, e_callee)
						new_bad_req = get_requests(output_folder + "/lim_" + bad_filename, s_direction, s_caller, s_callee, e_direction, e_caller, e_callee)
						
						#No requests found at this depth in one of the trace files
						if len(new_good_req) == 0 or len(new_bad_req) == 0:
							print("Files " + good_file + " and " + bad_file + " not similar. Moving diff file to diff_folder")
							copy(diff_file, diff_folder)
							os.remove(diff_file)
							break

						#Write the requests to file and get the differences between them
						write_request_to_file(new_good_req[g_req], output_folder + "/str_" + str(g_req) + "_" + good_filename, output_folder + "/lim_" + good_filename)
						write_request_to_file(new_bad_req[b_req], output_folder + "/str_" + str(b_req) + "_" + bad_filename, output_folder + "/lim_" + bad_filename)
						res = is_similar(output_folder, str(g_req) + "_" + good_filename, str(b_req) + "_" + bad_filename, diff_folder)
						
						if not res:
							if remove_differences:
								filter_messages(diff_file, output_folder + "/str_" + str(g_req) + "_" + good_filename, output_folder + "/str_" + str(b_req) + "_" + bad_filename, output_folder + "/str_" + str(g_req) + "_" + good_filename + "_filtered", output_folder + "/str_" + str(b_req) + "_" + bad_filename + "_filtered")
								print("Eliminated differences between good and bad trace files. Diff file moved to diff_folder")
								files_to_format.append((str(g_req) + "_" + good_filename + "_filtered", str(b_req) + "_" + bad_filename + "_filtered"))
								copy(diff_file, diff_folder)
								os.remove(diff_file)
								break
							else:
								#If there are differences between the two requests, reduce depth
								depth = depth - 1
								print("Files not similar at depth " + str(depth) + ". Reducing depth..")

						else:
							#Else add these requests in the list of files to be formatted
							print("Compared request number " + str(g_req) + " in " + good_filename + " with request number " + str(b_req) + " in " + bad_filename + " at depth " + str(depth))
							files_to_format.append((str(g_req) + "_" + good_filename, str(b_req) + "_" + bad_filename))
							os.remove(diff_file)
							break
		
	return files_to_format

def invoke_limit_depth(good_file, bad_file, output_folder, good_filename, bad_filename, depth):

	subprocess.call(["python3", "limit_depth.py", "-i", good_file, "-l", "-o", output_folder + "/lim_" + good_filename, "-d", str(depth)])
	subprocess.call(["python3", "limit_depth.py", "-i", bad_file, "-l", "-o", output_folder + "/lim_" + bad_filename, "-d", str(depth)])

#Return true if there are no differences(Files are similar). Else return false.
def is_similar(output_folder, good_filename, bad_filename, diff_folder):

	with open(output_folder + "/diff_"+ good_filename + "_" + bad_filename, 'w') as file1:
		subprocess.call(["bash exec_diff.sh " +  output_folder + "/str_" + good_filename + " " + output_folder + "/str_" + bad_filename], shell=True, stdout=file1)

	if not isEqual(output_folder + "/diff_"+ good_filename + "_" +  bad_filename):
		return False

	return True

if __name__ == "__main__":

	parser = argparse.ArgumentParser()

	parser.add_argument("good_file")
	parser.add_argument("bad_file")
	parser.add_argument("output_folder")
	parser.add_argument("depth", type=int)

	#Information about which message to consider as beginning of a request
	parser.add_argument("s_direction")
	parser.add_argument("s_caller")
	parser.add_argument("s_callee")

	#Information about which message to consider as end of a request
	parser.add_argument("e_direction")
	parser.add_argument("e_caller")
	parser.add_argument("e_callee")

	#Output folder to contain final output files
	parser.add_argument("final_output_folder")
	parser.add_argument('-i', help = 'Interactive', default = False, required = False, action = 'store_const', const = True)
	parser.add_argument('-r', help = 'Remove differences between good and bad trace files', default = False, required = False, action = 'store_const', const = True)

	args = parser.parse_args()

	good_filename = args.good_file.split("/")[-1]
	bad_filename = args.bad_file.split("/")[-1]

	interactive =  args.i

	diff_folder = args.output_folder + "/diff_folder/"

	files_to_format = run_sa(args.good_file, args.bad_file, args.output_folder, args.depth, args.s_direction, args.s_caller, args.s_callee, args.e_direction, args.e_caller, args.e_callee, interactive, diff_folder, args.r)
	
	for goodfile, badfile in files_to_format:

		output_file = args.final_output_folder + "/" + goodfile + "_" + badfile #Specify which request number and which trace files are compared in this file
		subprocess.call(["python3", "format_messages.py", args.output_folder + "/str_" + goodfile, args.output_folder + "/str_" + badfile, output_file])
