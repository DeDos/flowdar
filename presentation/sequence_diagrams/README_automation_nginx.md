The analysis (for Nginx) has been automated in `run_complete_gvb.py` that works as follows:


Requirement: individual connections have been extracted and located in separate 'good' and 'bad' folders. (See trace_processing/README.md)


`run_complete_gvb.py` takes these parameters:
    
    good_directory: path to the directory of "good" connections
    bad_directory: path to the directory of "bad" connections
    output_directory: path to the directory where the results are saved
    results_filename: filename to save the result of the temporal analyzer
    num_div_points: number of divergence points to take into consideration in each comparison
    depth: the maximum function call depth that is used in the analysis
    threshold: the minimum significant time difference (in microsecond)

It compares all connections in <good_directory> against all connections in <bad_directory> at the depth of <depth> for the structural analysis and a threshold of <threshold> for temporal analysis, and saves the intermediate results in <output_directory>. The final results will be available to the user in <results_filename>.

Definition of Divergence Point: In temporal analysis, we compare function calls that exist in both good and bad traces, in the same order. Let t1 be the duration of a specific function call in the good trace, and t2 be the duration of that function call in the bad trace. The temporal analyzer computes td=t2-t1 for every function call and sorts the results in descending order. The major point of divergence is the highest difference among these values. There might be cases in which there is more than one point with a significant time difference. Therefore, recording more than one point provides the user with more insight into hotspots.


`run_complete_gvb.py` first calls `run_single_gvb.py` in a loop. Then, in another loop over individual results from the temporal analyzer, the <num_div_points> highest time differences (which are in fact major points of divergence) will be aggregated.
`run_single_gvb.py` takes these parameters:

Usage: `python3 run_single_gvb.py <good_trace_path> <bad_trace_path> <output_directory> <depth> <threshold>`

    good_trace_path: path to an individual "good" trace file
    bad_trace_path: path to an individual "bad" trace file
    output_directory: path to the directory where the results are saved
    depth: the maximum function call depth that is used in the analysis
    threshold: the minimum significant time difference (in microsecond)

`run_single_gvb.py` is used for comparing a single good trace file versus a single bad trace file, and works in two steps:

* Step 1: structural analysis

1. `limit_depth.py` is called on both good and bad trace files to limit their function call depth to <depth>.
2. A line-by-line `diff` is performed on the depth limited trace files.
3. Using `filter_messages.py`, only similar messages will be kept in each trace file.
4. `format_messages.py` will then be called on trace files resulting from the last step in order to merge and prepare them 
for the temporal analysis.

* Step 2: temporal analysis

`temporal_analysis.py` is called using the result from the final step of the structural analysis. It takes the following parameters:

    SA_output: path to the (merged) output from the structural analyzer
    TA_output: path where the result will be saved
    threshold: the minimum significant time difference (in microsecond)


A usage example for this automated script has been demonstrated in usage_example_structural_temporal_nginx.py.

