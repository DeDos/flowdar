'''
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


Achala Rao 
analysis.py - Takes as parameters a good trace folder, a bad trace folder and an output folder. 
Invokes the run_sa.py script on all combinations of good and bad trace files and stores the output in the specified folder.
UPenn, 2018
'''
import os, sys
import subprocess
import argparse
import time
import shutil
from segregate_workers import segregate_workers

parser = argparse.ArgumentParser()

parser.add_argument("input_good_folder")
parser.add_argument("input_bad_folder")
parser.add_argument("output_folder")
parser.add_argument("depth", type=int)

#Information about which message to consider as beginning of a request
parser.add_argument("s_direction")
parser.add_argument("s_caller")
parser.add_argument("s_callee")

#Information about which message to consider as end of a request
parser.add_argument("e_direction")
parser.add_argument("e_caller")
parser.add_argument("e_callee")

#Worker folders
parser.add_argument("worker_good_folder")
parser.add_argument("worker_bad_folder")

#Output folder to contain final output files
parser.add_argument("final_output_folder")

parser.add_argument('-i', help = 'Interactive', default = False, required = False, action = 'store_const', const = True)
parser.add_argument('-r', help = 'Remove differences between good and bad trace files', default = False, required = False, action = 'store_const', const = True)
parser.add_argument('-o', help = 'Flag to allow overwriting of files', default = False, required = False, action = 'store_const', const = True)

args = parser.parse_args()

with open('good_workers', 'w') as file1:
	subprocess.call(["../../results/utils/get_apache_worker_threads.sh " + args.input_good_folder.strip("/")], shell=True, stdout=file1)

with open('bad_workers', 'w') as file2:
	subprocess.call(["../../results/utils/get_apache_worker_threads.sh " + args.input_bad_folder.strip("/")], shell=True, stdout=file2)

segregate_workers("good_workers", args.input_good_folder, args.worker_good_folder, args.s_direction, args.s_caller, args.s_callee, args.e_direction, args.e_caller, args.e_callee)
segregate_workers("bad_workers", args.input_bad_folder, args.worker_bad_folder, args.s_direction, args.s_caller, args.s_callee, args.e_direction, args.e_caller, args.e_callee)

diff_folder = args.output_folder + "/diff_folder/"

#Delete and recreate diff folder and final_output_folder
if os.path.exists(diff_folder):
	if(args.o):
		shutil.rmtree(diff_folder)
	else:
		print(diff_folder + " already exists. Enable the -o flag to enable overwriting. Exiting..")
		exit(1)

if os.path.exists(args.final_output_folder):
	if(args.o):
		shutil.rmtree(args.final_output_folder)
	else:
		print(args.final_output_folder + " already exists. Enable the -o flag to enable overwriting. Exiting..")
		exit(1)
	
os.mkdir(diff_folder)
os.mkdir(args.final_output_folder)

for good_file in os.listdir(args.worker_good_folder):
	for bad_file in os.listdir(args.worker_bad_folder):
		if(good_file != ".DS_Store" and bad_file != ".DS_Store"):

			if args.i:
				if args.r:
					subprocess.call(["python3", "run_sa.py", args.worker_good_folder + "/" + good_file, args.worker_bad_folder + "/" + bad_file, args.output_folder, str(args.depth), args.s_direction, args.s_caller, args.s_callee, args.e_direction, args.e_caller, args.e_callee, "-i", args.final_output_folder, "-r"])
				else:
					subprocess.call(["python3", "run_sa.py", args.worker_good_folder + "/" + good_file, args.worker_bad_folder + "/" + bad_file, args.output_folder, str(args.depth), args.s_direction, args.s_caller, args.s_callee, args.e_direction, args.e_caller, args.e_callee, "-i", args.final_output_folder])

			else:
				if args.r:
					subprocess.call(["python3", "run_sa.py", args.worker_good_folder + "/" + good_file, args.worker_bad_folder + "/" + bad_file, args.output_folder, str(args.depth), args.s_direction, args.s_caller, args.s_callee, args.e_direction, args.e_caller, args.e_callee, args.final_output_folder, "-r"])
				else:
					subprocess.call(["python3", "run_sa.py", args.worker_good_folder + "/" + good_file, args.worker_bad_folder + "/" + bad_file, args.output_folder, str(args.depth), args.s_direction, args.s_caller, args.s_callee, args.e_direction, args.e_caller, args.e_callee, args.final_output_folder])
