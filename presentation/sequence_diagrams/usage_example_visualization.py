'''
Copyright (c) 2019, University of Pennsylvania
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the <organization> nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


Achala Rao 
usage_example_visualization.py - Script that demonstrates the steps to be followed to generate a visualization from a trace file
UPenn, 2018 
'''
import os
import sys
import subprocess



input_file = sys.argv[1]
output_file = sys.argv[2]

subprocess.call(["python3", "process_function_naming.py", "-i", input_file, "-o", output_file + "_edited", "--remove_offset", "--replace_underscores"])
subprocess.call(["python3", "limit_depth.py", "-i", output_file + "_edited", "-o", output_file + "_lim", "-d", "5"]) #Optional
# subprocess.call(["python3", "group_messages.py", "3", output_file + "_lim", output_file + "_grouped"]) #Optional
#Can be called instead of group_messages.py
subprocess.call(["python3", "time_efficient_grouping.py", "-i", output_file + "_lim", "-o", output_file + "_grouped", "-w", "3", "-k", "10"]) #Optional
subprocess.call(["python3", "log_to_pdf.py", "--logfile", output_file + "_grouped"]) #Use the right input logfile dpeending on which optional steps have been invoked
subprocess.call(["mv", "seq_diagram.pdf", output_file + ".pdf"])
