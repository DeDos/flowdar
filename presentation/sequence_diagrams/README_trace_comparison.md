The comparison tool
===

**compare_traces.py** - allows the user to specify a folder of traces as input and produces an output file with each line specifying the ratio of similarities to differences in each file. 

Usage: `python3 compare_traces.py <input_directory> <output_file>`

A sample of the results for the trace files in latest.apache.doing.nothing folder can be found in example/apache_traces_diff.

    The result lines can be of three types:

    * When both the files compared are identical:

        Example: File1: f1, File2: f2, all lines similar (25)

        This line indicates that the file named f1 and file named f2 are identical. The number in brackets indicates the number of lines that are similar between f1 and f2.

    * When there are some differences between the files:

        Example: File1: f1, File2: f2, sim/diff ratio = 2163.45

        This indicates the ratio of the number of identical lines to different lines between the two files f1 and f2. The higher the ratio, the more similar they are.

    * When the files are very different:

        Example: File1: f1, File2: f2, not compared since their sizes differ by more than 1000 bytes

        This line indicates that f1 and f2 were not compared since their sizes differ by the stated value (this can be changed through the COMPARISON_FILESIZE_CUTOFF constant). Although they may have some lines in common, the traces are very different from each other.


The filtering tool
===

**eliminate_redundant_traces.py** - Maintains a set of unique files based on the output from compare_traces.py and copies only these files into a separate directory.

Usage: `python3 eliminate_redundant_traces.py <comparison_file> <input_directory> <output_directory>`

    comparison_file: This is the output of compare_traces.py, used to determine which files are unique.

    input_directory: Directory from which traces were compared in compare_traces.py

    output_directory: Directory where unique trace files should be placed.