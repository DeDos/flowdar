Our visualisation tools help understand what a program is doing during its
runtime, through which we can obtain insight into how the program can be
reorganised to better meet different objectives.

This example summarises Apache's functional-call behaviour, showing distinct
phases and repetitions:

![](trace_square_matrix/apache_example.png)
