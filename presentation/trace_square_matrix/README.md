Patterns can often be discerned at a glance, as in [this
example](apache.redos.trace.named.1-3.bmp).
But it's often helpful to take real time and duration into account, in which case
the example linked-to previously shows up as:

![](apache_redos_timescaled.png)

To get this we used the following parameters:
```
./trace_to_bmp.py TRACE_IN BMP_OUT --wctime --timescaled --quantum 10000
```
View the [full image](apache.redos.trace.named.bmp) and the [trace](apache.redos.trace.named) from which it originates.
