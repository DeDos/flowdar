#!/usr/bin/env python
#
#Copyright (c) 2019, University of Pennsylvania
#All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the <organization> nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
#DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#
# Visualise a trace as a bitmap.
# Nik Sultana, UPenn, June 2017

import argparse
import hashlib
import math
import numpy
import os
import random
import scipy.misc
import sys

arg_parser = argparse.ArgumentParser(description = "Visualise a trace as a bitmap.")
arg_parser.add_argument("in_trace", metavar = "FILEPATH", type = str,
        help = "Path to the file containing the trace to process")
arg_parser.add_argument("out_img", metavar = "FILEPATH", type = str,
        help = "Path to the output file to contain the generated image")
arg_parser.add_argument('--wctime',
        help = 'Regard timestamps by breaking up the image into epochs (usually of 1 second)', required = False, action = 'store_const', const = True)
arg_parser.add_argument('--timescaled',
        help = 'Indicate (roughly) the duration of each event', required = False, action = 'store_const', const = True)
arg_parser.add_argument('--calls_only',
        help = 'Regard function calls only (ignoring function returns)', required = False, action = 'store_const', const = True)
arg_parser.add_argument('--returns_only',
        help = 'Regard function returns only (ignoring function calls)', required = False, action = 'store_const', const = True)
arg_parser.add_argument("--quantum", metavar = "INT", type = int,
        help = "Quantum (in microseconds) for time-scaling; the gap between successive messages is filled with one dot per quantum microseconds")
arg_parser.add_argument('--overwrite_output',
        help = 'Overwrite output file if it already exists', required = False, action = 'store_const', const = True)
arg_parser.add_argument('--heat',
        help = 'Pixels are coloured depending on how frequently their corresponding entry occurs in the trace', required = False, action = 'store_const', const = True)
arg_parser.add_argument('--number_of_columns_unchecked',
        help = 'This option disable the checking of each entry in the trace to ensure that it has the expected number of columns', required = False, action = 'store_const', const = True)
args = arg_parser.parse_args()

if os.path.exists(args.out_img) and args.overwrite_output <> True:
  sys.stderr.write("Output file already exists: " + args.out_img + "\n" +
         "Use --overwrite_output to overwrite it by default.\n")
  exit(1)

if args.timescaled == True and args.wctime <> True:
  sys.stderr.write("--timescaled only works with --wctime\n")
  exit(1)

if args.calls_only == True and args.returns_only == True:
  sys.stderr.write("Cannot have both --calls_only and --returns_only: drop both or pick one\n")
  exit(1)

quantum = 1000 # By default
if args.quantum <> None:
  if args.timescaled <> True:
    sys.stderr.write("--quantum only works with --timescaled\n")
    exit(1)
  else: quantum = args.quantum

if args.number_of_columns_unchecked == True:
  sys.stderr.write("WARNING: not checking whether each entry in the trace contains the expected number of columns\n")

codehash = dict()
invertedidx = dict()

random.seed(a = 0)

expected_number_of_cols = 6
col_direction = 0
col_caller = 1
col_callee = 2
col_pid = 3
col_sec = 4
col_usec = 5

fn_call_symbol = ">"
fn_retn_symbol = "<"

# Black is the background colour.
# Red is forbidden since it's used for the calibration line.
forbidden_colours = [0, (255 << 16)]
# Dull colours are forbidden.
for b in [0, 8, 16]:
  starting_colour = 1 << b
  for x in range(starting_colour, starting_colour + 9):
    forbidden_colours.append(x)

heat_colour_idx = dict()

def gen_colour(line):
  if args.heat == True:
    [_, result] = heat_colour_idx[line]
    return result
  else:
    initial = random.randint(1, 255) * random.randint(1, 255) * random.randint(1, 255)
    h = "0x" + hashlib.md5(line).hexdigest()
    candidate = (initial + int(h, 0)) % (255 * 255 * 255)
    if (candidate in invertedidx) or (candidate in forbidden_colours):
      candidate = gen_colour(line)
    return candidate

# "Calibration": the top row is painted red. Simple (and in time redundant)
# visual cue of where the diagram starts.
def calibrate(dimension, matrix):
  for x in range(0, dimension):
    matrix[0, x] = [255, 0, 0]

# An "entry" is a function's call or its return, without any furthr info (e.g., time info)
def process_entry(line):
  line = line.rstrip()
  if line not in codehash:
    codehash[line] = list()
    candidate = gen_colour(line)
    codehash[line] = [candidate & 255, (candidate >> 8) & 255, (candidate >> 16) & 255]
    invertedidx[candidate] = line

def trace_square_matrix():
  dimension = int(math.ceil(math.sqrt(line_count))) + 1 # Add 1 since we use the top row for calibration.
  matrix = numpy.zeros((dimension, dimension, 3), dtype = numpy.uint8)
  calibrate(dimension, matrix)

  x = 1
  y = 1
  with open(args.in_trace, 'r') as infile:
    for line in infile:
      line = line.rstrip()
      cols = line.split()
      if args.calls_only == True and cols[col_direction] <> fn_call_symbol: continue
      if args.returns_only == True and cols[col_direction] <> fn_retn_symbol: continue
      line = cols[col_direction] + " " + cols[col_caller] + " " + cols[col_callee]
      process_entry(line)

      matrix[y, x] = codehash[line]
      x = x + 1
      if x >= dimension:
        x = 0
        y = y + 1

  return matrix

break_interval = 30
def trace_wctime_matrix():
  max_x = 0
  cur_x = 0
  prev_line_x = 0
  cur_y = 0
  last_break = 0
  cur_sec = 0
  prev_usec = 0
  prev_line = None
  pre_matrix = []
  with open(args.in_trace, 'r') as infile:
    for line in infile:
      line = line.rstrip()
      cols = line.split()
      started_new_line = False
      if args.calls_only == True and cols[col_direction] <> fn_call_symbol: continue
      if args.returns_only == True and cols[col_direction] <> fn_retn_symbol: continue
      if cols[col_sec] <> cur_sec:
        started_new_line = True
        cur_sec = cols[col_sec]
        prev_line_x = cur_x
        cur_x = 0
        cur_y += 1
        pre_matrix.append([])

        #  Disabled this feature when --timescaled==True, since it complicates the "filling the previous line until the end" feature.
        if args.timescaled <> True:
          # Leave a gap, based on `break_interval`
          if last_break == 0:
            last_break = int(cols[col_sec])
          else:
            if last_break <= (int(cols[col_sec]) - break_interval):
              last_break = int(cols[col_sec])
              pre_matrix.append([])
              cur_y += 1

      line = cols[col_direction] + " " + cols[col_caller] + " " + cols[col_callee]
      process_entry(line)

      # Add indication of "scale" of previous call.
      if args.timescaled == True and prev_line <> None:
        if started_new_line:
          width = ((int(cols[col_usec]) + (1000000 - int(prev_usec))) / quantum)
          for i in range(0, width):
            pre_matrix[cur_y - 2].append(codehash[prev_line]) # Append to previous line, thus cur_y-2.
            prev_line_x += 1
          if prev_line_x > max_x:
            max_x = prev_line_x
        else:
          width = ((int(cols[col_usec]) - int(prev_usec)) / quantum)
          for i in range(0, width):
            pre_matrix[cur_y - 1].append(codehash[prev_line])
            cur_x += 1

      pre_matrix[cur_y - 1].append(codehash[line])
      cur_x += 1
      if cur_x > max_x:
        max_x = cur_x

      prev_line = line
      prev_usec = cols[col_usec]

  print ("Maximum width of one time unit: " + str(max_x))
  print ("Image length (including interval breaks): " + str(cur_y))

  max_x += 1 # Since 0-indexed
  cur_y += 2 # Since 0-indexed, and adding an extra line at the top for the "calibration" line

  matrix = numpy.zeros((cur_y, max_x, 3), dtype = numpy.uint8)
  calibrate(max_x, matrix)
  y = 0
  for yl in pre_matrix:
    y += 1
    x = 0
    for v in yl:
      matrix[y, x] = v
      x += 1

  return matrix

line_count = 0

# Count the lines in the file, filtering as appropriate.
# We also use this as an opportunity to check that the entry contains the expected number of columns.
# If the user specified --heat, then we gather information to later calculate the
# pixel colours accordingly.
with open(args.in_trace, 'r') as infile:
  for line in infile:
    line = line.rstrip()
    cols = line.split()
    if args.number_of_columns_unchecked <> True and len(cols) <> expected_number_of_cols:
      sys.stderr.write("Malformed entry? Was expecting " +
            str(expected_number_of_cols) + " columns: '" + line + "'\n")
      exit(1)
    if args.calls_only == True and cols[col_direction] <> fn_call_symbol: continue
    if args.returns_only == True and cols[col_direction] <> fn_retn_symbol: continue
    line_count += 1
    line = cols[col_direction] + " " + cols[col_caller] + " " + cols[col_callee]
    if args.heat == True:
      if line in heat_colour_idx:
        heat_colour_idx[line] = 1 + heat_colour_idx[line]
      else:
        heat_colour_idx[line] = 1

interval_size = 0
# If the user specified --heat, calculate the colour for each entry depending
# on its frequency in the entire trace.
if args.heat == True:
  ordered_mapping = []
  for line, count in heat_colour_idx.iteritems():
    ordered_mapping.append([line, count])
  ordered_mapping.sort(key = lambda l: l[1])
  interval_size = (255 * 255 * 255) / len(heat_colour_idx)
  offset = 0
  element_number = 0
  for [line, count] in ordered_mapping:
    element_number += 1
    # NOTE we distribute colours linearly:
    #      * we could make use of differently-sized intervals, depending on frequency.
    #      * we could instead increment in such a way that distributes across the RGB ranges.
    original_colour = (offset + element_number) * interval_size
    colour = original_colour
    while colour in forbidden_colours:
      colour += 1
      # This mechanism ensures that we avoid forbidden_colours, and that in so doing
      # we don't overrun the current interval_size (in case the interval_size is small..)
      # so we increment the offset to get more intervals -- but this is likely to result
      # in a squeeze at the last intervals, so it's best to have variably-sized intervals.
      if colour == original_colour + interval_size:
        sys.stderr.write("WARNING: interval_size not larget enough in one case, therefore incrementing offset\n")
        offset += 1
        original_colour = (offset + element_number) * interval_size
    heat_colour_idx[line] = [count, colour]

matrix = None

if args.wctime == True:
  matrix = trace_wctime_matrix()
else:
  matrix = trace_square_matrix()

print ("(1) Number of calls/returns: " + str(line_count))
print ("(2) Unique call/return symbols: " + str(len(codehash.items())))
print ("Ratio of (2) and (1) -- smaller is better: " + str(float(len(codehash.items())) / float(line_count)))

if args.heat == True:
  print ("Regarding --heat: interval_size=" + str(interval_size))

if len(codehash.items()) > (256 * 256 * 256):
  sys.stderr.write("WARNING: The current colour space can't distinguish all calls/returns\n")

result = scipy.misc.toimage(matrix)
result.save(args.out_img)

print "Done"
exit(0)
