#!/bin/bash
# Nik Sultana, UPenn, June 2017
# e.g., ./step2.sh cg_in cg.dot

FILE_IN=$1
FILE_OUT=$2
echo "graph G {" > ${FILE_OUT}
echo "mode = KK;" >> ${FILE_OUT}
echo "layout = neato;" >> ${FILE_OUT}
awk '{ print "\""$2"\" -- \""$3"\";" }' ${FILE_IN} >> ${FILE_OUT}
echo "}" >> ${FILE_OUT}
