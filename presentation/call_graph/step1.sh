#!/bin/bash
# Nik Sultana, UPenn, June 2017
# e.g., ./step1.sh ../../workloads/apache2/downloads/apache.SL-AB.trace.c23 > cg_in

sort $1 | uniq -c
