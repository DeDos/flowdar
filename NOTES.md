## Dependencies
Various dependencies were built locally, especially for Apache2:
* apr-1.5.2
* apr-util-1.5.4
* pcre-8.00
Building these was straightforward.

## Autoconfig blues
```
$ make clean
CDPATH="${ZSH_VERSION+.}:" && cd . && /bin/sh /home/DBGROUP/nsultana/wget-1.19/build-aux/missing aclocal-1.15 -I m4
/home/DBGROUP/nsultana/wget-1.19/build-aux/missing: line 81: aclocal-1.15: command not found
WARNING: 'aclocal-1.15' is missing on your system.
         You should only need it if you modified 'acinclude.m4' or
         'configure.ac' or m4 files included by 'configure.ac'.
         The 'aclocal' program is part of the GNU Automake package:
         <http://www.gnu.org/software/automake>
         It also requires GNU Autoconf, GNU m4 and Perl in order to run:
         <http://www.gnu.org/software/autoconf>
         <http://www.gnu.org/software/m4/>
         <http://www.perl.org/>
make: *** [aclocal.m4] Error 127
$ touch configure.ac aclocal.m4 configure Makefile.am Makefile.in
```

## OpenSSL
I built OpenSSL locally, it was needed by a couple of other systems (e.g., wget,
Apache2). Had to tune the building as follows (to generate .so files):
```
CFLAGS="-fPIC" ./config shared --prefix=//home/DBGROUP/nsultana/openssl_built_1_0_1d_again/ &&
CFLAGS="-fPIC" make
```
