## Description
Simply running it without any parameters.

## Command
```
$ pwd
/mnt/home/nsultana/wget-1.19_traced
$ hostname
dbclust1.cis.upenn.edu
$ whoami
nsultana
$ TRACE_FILE=openflow.trace controller/controller
controller: at least one vconn argument required; use --help for usage
$
```

## Raw trace
[openflow.trace](openflow.trace)

## Rendered trace
FIXME
