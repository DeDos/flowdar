Version **1.0.0** (from http://archive.openflow.org/wp/downloads/) built using:
```
TRACE_FILE=local.program.trace \
CFLAGS="-O0 -g -fno-function-cse -finstrument-functions" \
LDFLAGS="/home/DBGROUP/nsultana/chopchop/dyngraph/generation/tracer_hooks.o" \
./configure \
--prefix=/home/DBGROUP/nsultana/bins &&
TRACE_FILE=local.program.trace \
make
```

# NOTE
* Haven't checked if the multiprocessing is configurable (as it is with
  stunnel).

# Post-build
`local.program.trace` contains the following:
```
> 0x3d7081ed1d 0x4008cc 31499
< 0x3d7081ed1d 0x4008cc 31499
```
