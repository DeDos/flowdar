Version **5.41** built using:
```
TRACE_FILE=/home/DBGROUP/nsultana/stunnel-5.41_traced/stunnel.local.trace \
CFLAGS="--static -O0 -g -fno-function-cse -finstrument-functions -I /home/DBGROUP/nsultana/chopchop/dyngraph/generation" \
LDFLAGS="/home/DBGROUP/nsultana/chopchop/dyngraph/generation/tracer_hooks.o" \
./configure \
--with-ssl=/home/DBGROUP/nsultana/openssl_built_1_0_1d/ \
--with-threads=fork &&
TRACE_FILE=/home/DBGROUP/nsultana/stunnel-5.41_traced/stunnel.local.trace make
```

Configuration (in file `myconf`):
```
foreground = yes
debug = info

[https]
accept  = 192.168.1.1:443
connect = 192.168.1.1:80
cert = /home/DBGROUP/nsultana/stunnel-5.41/tools/stunnel.pem
TIMEOUTclose = 0
```

Running (in `~/stunnel-5.41_traced`):
```
sudo TRACE_FILE=/root/stunnel.trace src/stunnel myconf
```
paired with
```
trace_reader -i /root/stunnel.trace -o stunnel
```
