## Description
Start and stop stunnel.

## Command
```
$ hostname
dbclust1.cis.upenn.edu
$ pwd
/home/DBGROUP/nsultana/stunnel-5.41_traced
$ ./src/stunnel
```

## Raw trace
[stunnel_trace.log](stunnel_trace.log)

## Rendered trace
[stunnel_trace.pdf](stunnel_trace.pdf)
