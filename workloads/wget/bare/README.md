## Description
Successfully start wget with no parameters.

## Command
```
$ pwd
/mnt/home/nsultana/wget-1.19_traced
$ hostname
dbclust1.cis.upenn.edu
$ whoami
nsultana
$ LD_LIBRARY_PATH=/home/DBGROUP/nsultana/openssl_built_1_0_1d_again/lib/ TRACE_FILE=wget.trace ./src/wget
wget: missing URL
Usage: wget [OPTION]... [URL]...

Try `wget --help' for more options.
$
```

## Raw trace
[wget.trace](wget.trace)

## Rendered trace
FIXME
