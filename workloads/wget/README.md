Version **1.19** built using:
```
TRACE_FILE=local.program.trace \
CFLAGS="-O0 -g -fno-function-cse -finstrument-functions -L/home/DBGROUP/nsultana/openssl_built_1_0_1d_again/lib/ -I/home/DBGROUP/nsultana/openssl_built_1_0_1d_again/include/" \
LDFLAGS="/home/DBGROUP/nsultana/chopchop/dyngraph/generation/tracer_hooks.o" \
./configure \
--prefix=/home/DBGROUP/nsultana/bins \
--with-libssl-prefix=/home/DBGROUP/nsultana/openssl_built_1_0_1d_again \
--with-ssl=openssl &&
TRACE_FILE=local.program.trace \
make
```

# NOTE
* Haven't checked if the multiprocessing is configurable (as it is with stunnel)

# Summary of build options:
```
  Version:           1.19
  Host OS:           linux-gnu
  Install prefix:    /home/DBGROUP/nsultana/bins
  Compiler:          gcc
  CFlags:              -DNDEBUG -O0 -g -fno-function-cse -finstrument-functions -L/home/DBGROUP/nsultana/openssl_built_1_0_1d_again/lib/ -I/home/DBGROUP/nsultana/openssl_built_1_0_1d_again/include/ 
  LDFlags:           /home/DBGROUP/nsultana/chopchop/dyngraph/generation/tracer_hooks.o
  Libs:              -lssl -lcrypto -ldl -lz   
  SSL:               openssl
  Zlib:              yes
  PSL:               no
  Digest:            yes
  NTLM:              yes
  OPIE:              yes
  POSIX xattr:       yes
  Debugging:         yes
  Assertions:        no
  Valgrind:          Valgrind testing not enabled
  Metalink:          no
  Resolver:          libc, --bind-dns-address and --dns-servers not available
  GPGME:             no
  IRI:               no
```


After building, `local.program.trace` contains (not sure why):
```
> 0x3d7081ed1d 0x400b1c 15325
< 0x3d7081ed1d 0x400b1c 15325
```
