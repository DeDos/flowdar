Version **2.4.25** built using:
```
BUILD_PATH="/home/jinzihao/apache_build_env" \
TRACER_PATH="/home/jinzihao/chopchop/dyngraph/generation" \
TRACE_FILE="$BUILD_PATH/apache.local.trace" \
./configure --enable-ssl --with-mpm=worker --with-ssl=$BUILD_PATH/openssl_bin --prefix=$BUILD_PATH CFLAGS='-g -finstrument-functions -I $TRACER_PATH -DOPENSSL_NO_SSL2 -fno-inline -fno-optimize-sibling-calls -fno-omit-frame-pointer' LDFLAGS="$TRACER_PATH/tracer_hooks.o" --with-apr=$BUILD_PATH/apr-1.6.2/ --with-apr-util=$BUILD_PATH/apr-util-1.6.0/ --with-pcre=$BUILD_PATH/pcre-8.40/pcre-config --prefix=$BUILD_PATH/bin ; make
```

Run (with `sudo`) using:
```
# Check the config file.
httpd -t
# List linked-in modules.
httpd -l
# List loaded modules.
httpd -M
# Run as a single process (but possibly having multiple threads).
httpd -f ~/bins/conf/httpd.conf -X
```

# NOTE
* Haven't used `-O0 -static`
* Multiprocessing is configurable via "mpm" and the main configurations are "prefork", "worker" and "event".
* I built a bunch of the dependencies locally.
* Ignore warnings such as:
```
*** Warning: Linking the shared library mod_request.la against the non-libtool
*** objects  /home/DBGROUP/nsultana/chopchop/dyngraph/generation/tracer_hooks.o is not portable!
```

# Summary of build options:
```
    Server Version: 2.4.25
    Install prefix: /home/DBGROUP/nsultana/bins/
    C compiler:     gcc -std=gnu99
    CFLAGS:         -g -finstrument-functions -I /home/DBGROUP/nsultana/chopchop/dyngraph/generation -O2 -pthread
    LDFLAGS:        /home/DBGROUP/nsultana/chopchop/dyngraph/generation/tracer_hooks.o
    LIBS:           
    CPPFLAGS:        -DLINUX -D_REENTRANT -D_GNU_SOURCE
    C preprocessor: gcc -E
```

# Diff from default conf (of httpd.conf)
```
70c70
< #LoadModule authn_socache_module modules/mod_authn_socache.so
---
> LoadModule authn_socache_module modules/mod_authn_socache.so
105c105
< LoadModule mime_module modules/mod_mime.so
---
> #LoadModule mime_module modules/mod_mime.so
109c109
< LoadModule env_module modules/mod_env.so
---
> #LoadModule env_module modules/mod_env.so
111c111
< LoadModule headers_module modules/mod_headers.so
---
> #LoadModule headers_module modules/mod_headers.so
113,114c113,114
< LoadModule setenvif_module modules/mod_setenvif.so
< LoadModule version_module modules/mod_version.so
---
> #LoadModule setenvif_module modules/mod_setenvif.so
> #LoadModule version_module modules/mod_version.so
132c132
< #LoadModule ssl_module modules/mod_ssl.so
---
> LoadModule ssl_module modules/mod_ssl.so
151c151
< #LoadModule rewrite_module modules/mod_rewrite.so
---
> LoadModule rewrite_module modules/mod_rewrite.so
201c201
<     AllowOverride none
---
>     AllowOverride None
238c238
<     AllowOverride None
---
>     AllowOverride All
499,502c499,502
< <IfModule ssl_module>
< SSLRandomSeed startup builtin
< SSLRandomSeed connect builtin
< </IfModule>
---
> #<IfModule ssl_module>
> #SSLRandomSeed startup builtin
> #SSLRandomSeed connect builtin
> #</IfModule>
503a504,505
> RewriteEngine On
> ExtendedStatus On

```

