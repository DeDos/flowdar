## Description
Successfully starting Apache2, and have it run while pages/files are downloaded.

## Command
```
# pwd
/root/run_apache
# TRACE_FILE=/root/apache.trace /home/DBGROUP/nsultana/bins/bin/httpd -X
```

## Raw trace
* [Getting the index.html file 3 times](apache_trace.tgz)
* [Downloading Apache from Apache](apache_download_trace.tgz)
