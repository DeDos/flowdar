## Description
Successfully starting Apache2, and have it run idle for a few seconds.

## Command
```
$ whoami
nsultana
$ hostname
dbclust1.cis.upenn.edu
$ pwd
/home/DBGROUP/nsultana/httpd-2.4.25_traced
$ sudo /httpd
$
```

## Raw trace
[program.trace](program.trace)

## Rendered trace
FIXME
