## Description
Attempting to start httpd without the right permissions.

## Command
```
$ whoami
nsultana
$ hostname
dbclust1.cis.upenn.edu
$ pwd
/home/DBGROUP/nsultana/httpd-2.4.25_traced
$ ./httpd
(13)Permission denied: AH00072: make_sock: could not bind to address [::]:80
(13)Permission denied: AH00072: make_sock: could not bind to address 0.0.0.0:80
no listening sockets available, shutting down
AH00015: Unable to open logs
```

## Raw trace
[trace](trace)

## Rendered trace
FIXME
