## Description
Attempting to start httpd without the right permissions.

## Command
```
$ whoami
nsultana
$ hostname
dbclust1.cis.upenn.edu
$ pwd
/home/DBGROUP/nsultana/httpd-2.4.25_traced
$ sudo ./httpd
[sudo] password for nsultana:
(98)Address already in use: AH00072: make_sock: could not bind to address [::]:443
(98)Address already in use: AH00072: make_sock: could not bind to address 0.0.0.0:443
no listening sockets available, shutting down
AH00015: Unable to open logs
```

## Raw trace
[program.trace](program.trace)

## Rendered trace
FIXME
